//
//  KnowledgeCell.h
//  Baby
//
//  Created by kim ly on 5/6/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowledgeCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *ktImageView;

@end
