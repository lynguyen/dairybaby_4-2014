//
//  SQLiteDB.h
//  Baby
//
//  Created by kim ly on 4/7/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Dairy.h"
#import "BabyGrow.h"

@interface SQLiteDB : NSObject
{
    sqlite3 *database;
}

#pragma mark - table INFO

- (void)initInfo;
- (void)insertInfo:(NSInteger)indexDairy indexBaby:(NSInteger)indexBaby;
- (NSMutableArray *)getInfo;
- (void)updateIndexBabyInfo:(NSInteger)indexBaby;
- (void)updateindexDairyInfo:(NSInteger)indexDairy;

#pragma mark - table DAIRY

- (void)initDairy;
- (void)insertDairy:(NSString *)nameDairy avata:(NSInteger)avataDairy title:(NSString *)titleDairy address:(NSString *)addressDairy time:(NSInteger)timeDiary emotion:(NSString *)emotionDairy index:(NSInteger )indexDairy;
- (NSMutableArray *)getAllDairy;
- (NSMutableArray *)getDairyWithID:(NSInteger)idDairy;
- (void)delDairyWithID:(NSInteger)iDDairy;

#pragma mark - table BABY

- (void)initBaby;
- (void)insertBaby:(NSString *)nameBaby avata:(NSString *)avata birth:(NSInteger) birth gender:(NSInteger )gender like:(NSString *)like;
- (NSMutableArray *)getAllBaby;
- (void)updateBabyInfo:(NSInteger)iD name:(NSString *)name avata: (NSString *)fileName gender:(NSInteger )gender birth:(NSInteger)birth like:(NSString *)like;
- (int)getIDBabyWithName:(NSString *)nameBaby;

#pragma mark - table ALBUM

- (void)initAlbum;
- (void)insertAlbum:(NSString *)nameFile type:(NSInteger)type emotion:(NSString *)emotion nameDairy:(NSString *)nameDairy;
- (NSMutableArray *)getAllAlbumWithNameDairy:(NSString *)nameDairy;
- (void)delAlbumWithNameDiary:(NSString *)nameDairy;
- (void)updateEmotionWithNameFileAlbum:(NSString *)nameFile emotion: (NSString *)emotion;
- (NSMutableArray *)getAllAlbum;



#pragma mark - table BABYGROW
- (void)initBabyGrow;
- (void)insertBabyGrow:(NSInteger)iDBaby month:(NSInteger)month height:(NSNumber *)height weight:(NSNumber *)weight;
- (NSMutableArray *)getInfoBabyGrowWithIDBaby:(NSInteger)iDBaby;
- (BOOL)checkMonthOfBabyWithIDAndMonth:(NSInteger)idBaby month:(NSInteger)monthBaby;

#pragma mark - table COMINGEVENT
- (void)initComingEvent;
- (void)insertComingEvent:(NSString *)titleComingEvent hourTime:(NSInteger )hourTimeComingEvent statusReminder:(NSInteger)statusReminderCommingEvent typeReminder:(NSInteger)typeReminder sunday:(NSInteger)sunday monday:(NSInteger)monday tuesday:(NSInteger)tuesday wednesday:(NSInteger)wednesday thursday:(NSInteger)thursday friday:(NSInteger)friday saturday:(NSInteger)saturday ringtone:(NSString *)ringtoneFile;
- (NSMutableArray *)getAllComingEvents;
- (void)delComingEventWithID:(NSInteger)iD;
- (void)updateStatusReminderWithId:(NSInteger)idComingEvent status:(NSInteger)statusReminder;

#pragma mark - table STANDARDBOY;
-(NSMutableArray *)getAllInfoStandartBoyWithMonth:(NSInteger)monthInfo;

#pragma mark - table STANDARDGIRL
-(NSMutableArray *)getallINfoStandartGirlWithMonth:(NSInteger)monthInfo;



#pragma mark - method open - close database
- (void)closeData;
- (void)openData;

#pragma mark - table Knowledge
- (NSMutableArray *)GetAllKnowledgeWithType:(NSInteger)type;



// cac method viet vao de thu fix bug loi
- (void)initGetMyRoiDo;

- (void)insertDairyTemp:(NSString *)nameDairy title:(NSString *)titleDairy  emotion:(NSString *)emotionDairy address:(NSString *)addressDairy;

@end
