//
//  SQLiteDB.m
//  Baby
//
//  Created by kim ly on 4/7/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "SQLiteDB.h"
#import "Info.h"
#import "Dairy.h"
#import "Baby.h"
#import "Album.h"
#import "Knowledge.h"
#import "ComingEvent.h"
#import "StandardGrow.h"

@implementation SQLiteDB

#pragma mark - @ table

-(void)initInfo;
{
    
    // mo duong dan csdl san khong dong. khi nao thoat khoi app thi khi do moi dong database
        // Create a string containing the full path to the sqlite.db inside the documents folder
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"sqlite.db"];
        
        // Check to see if the database file already exists
        bool databaseAlreadyExists = [[NSFileManager defaultManager] fileExistsAtPath:databasePath];
        
        // Open the database and store the handle as a data member
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            // Create the database if it doesn't yet exists in the file system
            if (!databaseAlreadyExists)
            {
                // Create the PERSON table
                const char *sqlStatement = "CREATE TABLE IF NOT EXISTS INFO (ID INTEGER PRIMARY KEY AUTOINCREMENT, INDEXDAIRY INTEGER, INDEXBABY INTEGER)";
                char *error;
                
                    if (sqlite3_exec(database, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
                    {
                        NSLog(@"create table INFO complete");
                    }
                    else
                    {
                        NSLog(@"Error create table INFO: %s", error);
                    }
            
            }
        } //open OK
   
}


-(void)insertInfo:(NSInteger)indexDairy indexBaby:(NSInteger)indexBaby;
{
    // Create insert statement for the person
    int indexBabyConvert = (int)indexBaby;
    int indexDairyConvert = (int)indexDairy;
    NSLog(@"gia tri nhap vao baby and dairy %d, %d",indexBabyConvert, indexDairyConvert);
     NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO INFO (INDEXDAIRY, INDEXBABY) VALUES ('%d',%d)", indexDairy, indexBaby];
    char *error;
       
    // Create insert statement for the address
   
    if ( sqlite3_exec(database, [insertSQL UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Insert Object INFO complete");
    }
    else
    {
        NSLog(@"Error insert object INFO: %s", error);
    }
}

-(NSMutableArray *)getInfo
{
    // Allocate a persons array
    NSMutableArray *listInfo = [[NSMutableArray alloc]init];
    
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM INFO"];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            
            int indexDairy = sqlite3_column_int(statement, 1);
            int indexBaby = sqlite3_column_int(statement, 2);
            
            Info *obj = [[Info alloc] initWithName:indexDairy andIndexBaby:indexBaby];
            [listInfo addObject:obj];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return listInfo;
}

- (void)updateIndexBabyInfo:(NSInteger)indexBaby;
{
    // This is left as an exercise to extend both the Address and Person class with an ID and use this ID for the update
    
    // Create the replace statement for the address
    
    NSString *replaceStatement = [NSString stringWithFormat:@" UPDATE INFO SET INDEXBABY = '%d'", indexBaby];
    
    // Execute the replace
    char *error;
    if (sqlite3_exec(database, [replaceStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"update attributes INDEXBABY of table INFO complete");
    }
    else
    {
        NSLog(@"Error update attributes INDEXBABY of table INFO: %s", error);
    }
    
}

- (void)updateindexDairyInfo:(NSInteger)indexDairy;
{
    // This is left as an exercise to extend both the Address and Person class with an ID and use this ID for the update
    
    // Create the replace statement for the address
    
    NSString *replaceStatement = [NSString stringWithFormat:@" UPDATE INFO SET INDEXDAIRY = '%d'", indexDairy];
    
    // Execute the replace
    char *error;
    if (sqlite3_exec(database, [replaceStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"update attributes INDEXDAIRY of table INFO complete");
    }
    else
    {
        NSLog(@"Error update attributes INDEXDAIRY of table INFO : %s", error);
    }

    
}

#pragma mark - @ table DAIRY

- (void)initDairy;
{
    // Create the DAIRY table
    const char *sqlStatement = "CREATE TABLE IF NOT EXISTS DAIRY (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAMEDAIRY TEXT, TITLE TEXT, AVATA INTEGER, ADDRESS TEXT, TIME INTEGER, EMOTION TEXT, INDEXDAIRY INTEGER)";
    
    char *error;
    
    if (sqlite3_exec(database, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Create table DAIRY complete");
    }
    else
    {
        NSLog(@"Error create table DAIRY: %s", error);
    }

}

- (void)insertDairy:(NSString *)nameDairy avata:(NSInteger)avataDairy title:(NSString *)titleDairy address:(NSString *)addressDairy time:(NSInteger)timeDiary emotion:(NSString *)emotionDairy index:(NSInteger )indexDairy;
{
    NSLog(@"==> call method insert BABY:");
    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO DAIRY (NAMEDAIRY, TITLE, AVATA, ADDRESS, TIME, EMOTION, INDEXDAIRY) VALUES ('%@','%@', %d, '%@', %d, '%@', %d)",nameDairy, titleDairy, avataDairy, addressDairy, timeDiary, emotionDairy, indexDairy ];
    char *error;
    
    // Create insert statement for the address
    
    if ( sqlite3_exec(database, [insertSQL UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Insert Object DAIRY complete");
    }
    else
    {
        NSLog(@"Error insert object DAIRY: %s", error);
    }
    
}



- (NSMutableArray *)getAllDairy;
{
    // Allocate a persons array
    NSMutableArray *listDairy = [[NSMutableArray alloc]init];
    
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM DAIRY ORDER BY TIME DESC"];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int iD = sqlite3_column_int(statement, 0);
            NSString *name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            NSString *title = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            int avataIndex = sqlite3_column_int(statement, 3);
            NSString *address = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            NSString *emotion = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
            int indexDairy = sqlite3_column_int(statement, 7);
            
            NSLog(@"thuc hien lay du lieu time");
            
            NSLog(@"datetime %d", sqlite3_column_int(statement, 5)); //=0
            
            // time
            NSDate *eventDate = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 5)];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
            dateFormatter.dateFormat = @"dd-MM-yyyy";
            NSString  *theDate = [dateFormatter stringFromDate:eventDate];
            NSLog(@"theDate %@", theDate);
            
            NSLog(@"thanh cong lay du lieu thoi gian");
            //
         
            
            Dairy *objDairy = [[Dairy alloc] initWithName:iD name:name title:title avata:avataIndex address:address time:eventDate emotion:emotion index:indexDairy];
            
            // xem lai 1 lan nua:
//            NSLog(@"&&&& ==> xem lai 1 lan nua: ");
//            NSDate *eventDate1 = [objDairy time];
//            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
//            //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
//            dateFormatter1.dateFormat = @"dd-MM-yyyy";
//            NSString  *theDate1 = [dateFormatter stringFromDate:eventDate1];
//            NSLog(@"theDate %@", theDate1);
            
            [listDairy addObject:objDairy];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return listDairy;
}

- (NSMutableArray *)getDairyWithID:(NSInteger)idDairy;
{
    NSMutableArray *list = [[NSMutableArray alloc] init];
    // Create the query statement to get 
    Dairy *objDairy;
    
    NSLog(@"********dang trong method load dairy thoe id: void id = %d",idDairy);
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM DAIRY WHERE ID = '%d'",idDairy];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int iD = sqlite3_column_int(statement, 0);
            NSString *name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            NSString *title = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            int avataIndex = sqlite3_column_int(statement, 3);
            NSString *address = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            NSString *emotion = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
            int indexDairy = sqlite3_column_int(statement, 7);
            
            NSLog(@"thuc hien lay du lieu time");
            
            NSLog(@"datetime %d", sqlite3_column_int(statement, 5)); //=0
            
            // time
            NSDate *eventDate = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 5)];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
            dateFormatter.dateFormat = @"dd-MM-yyyy";
            NSString  *theDate = [dateFormatter stringFromDate:eventDate];
            NSLog(@"theDate %@", theDate);
            
            NSLog(@"thanh cong lay du lieu thoi gian");
            //

            
            
            objDairy = [[Dairy alloc] initWithName:iD name:name title:title avata:avataIndex address:address time:eventDate emotion:emotion index:indexDairy];
            [list addObject:objDairy];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return list;

}


#pragma mark - @ table BABY

- (void)initBaby;
{
    // Create the DAIRY table
    const char *sqlStatement = "CREATE TABLE IF NOT EXISTS BABY (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAMEBABY TEXT, AVATA TEXT, BIRTH INTEGER, GENDER INTEGER, LIKE TEXT)";
    
    char *error;
    
    if (sqlite3_exec(database, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Create table BABY complete");
    }
    else
    {
        NSLog(@"Error create table BABY: %s", error);
    }

}

- (void)insertBaby:(NSString *)nameBaby avata:(NSString *)avata birth:(NSInteger) birth gender:(NSInteger )gender like:(NSString *)like;
{
    NSLog(@" Thuc hien tao moi  Baby");
    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO BABY (NAMEBABY, AVATA, BIRTH, GENDER, LIKE) VALUES ('%@','%@', '%d', %d,'%@')",nameBaby, avata, birth, gender, like];
    char *error;
    
    // Create insert statement for the address
    
    if ( sqlite3_exec(database, [insertSQL UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Insert Object BABY complete");
    }
    else
    {
        NSLog(@"Error insert object BABY: %s", error);
    }
 
}

- (NSMutableArray *)getAllBaby;
{
    // Allocate a persons array
    NSMutableArray *listBaby = [[NSMutableArray alloc]init];
    
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM BABY ORDER BY ID DESC"];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int iD = sqlite3_column_int(statement, 0);
            NSString *name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            NSString *avata = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            int gender = sqlite3_column_int(statement, 4);
            NSString *like = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
            
            //NSLog(@"thuc hien lay du lieu time");
            NSLog(@"thuc hien lay du lieu time");
            
            NSLog(@"datetime %d", sqlite3_column_int(statement, 3)); //=0
            
            // time
            NSDate *eventDate = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 3)];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
            dateFormatter.dateFormat = @"dd-MM-yyyy";
            NSString  *theDate = [dateFormatter stringFromDate:eventDate];
            NSLog(@"theDate %@", theDate);
            
            NSLog(@"thanh cong lay du lieu thoi gian");
            //
            
            Baby *objBaby = [[Baby alloc] initWithName:iD nameBaby:name genderBaby:gender birthdayBaby:eventDate likeBaby:like avataBaby:avata];
            
            
            [listBaby addObject:objBaby];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return listBaby;
    
}

- (void)updateBabyInfo:(NSInteger)iD name:(NSString *)name avata: (NSString *)fileName gender:(NSInteger )gender birth:(NSInteger)birth like:(NSString *)like;
{
    //"CREATE TABLE IF NOT EXISTS BABY  BIRTH INTEGER, GENDER INTEGER, LIKE TEXT)"
    NSString *replaceStatement = [NSString stringWithFormat:@" UPDATE BABY SET NAMEBABY = '%@', AVATA = '%@', BIRTH = '%d',   GENDER = '%d', LIKE = '%@' WHERE NAMEFILE = '%d'",name, fileName, birth, gender, like, iD];
    
    // Execute the replace
    char *error;
    if (sqlite3_exec(database, [replaceStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"update attributes INDEXBABY of table INFO complete");
    }
    else
    {
        NSLog(@"Error update attributes INDEXBABY of table INFO : %s", error);
    }

    
}


- (int)getIDBabyWithName:(NSString *)nameBaby;
{
    int iD = 0;
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM BABY WHERE NAMEBABY = %@",nameBaby];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
             iD = sqlite3_column_int(statement, 0);
                      }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return iD;
}

- (void)delDairyWithID:(NSInteger)iDDairy;
{
    sqlite3_stmt *statement;
    NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM DAIRY WHERE ID = %d",iDDairy];
    
    const char *delete_stmt = [deleteSQL UTF8String];
    
    sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
    // loi o cau lenh nay
    if (sqlite3_step(statement) == SQLITE_DONE)
    {
        NSLog(@"xoa thanh cong data trong dairy co id: %d", iDDairy);
    } else {
        NSAssert(0, @"loi xoa data tu dairy");
    }
    sqlite3_finalize(statement);
}



#pragma mark - @ table ALBUM


- (void)initAlbum;
{
    // Create the DAIRY table
    const char *sqlStatement = "CREATE TABLE IF NOT EXISTS ALBUM (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAMEFILE TEXT, TYPE INTEGER, EMOTION TEXT, NAMEDAIRY TEXT)";
    
    char *error;
    
    if (sqlite3_exec(database, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Create table ALBUM complete");
    }
    else
    {
        NSLog(@"Error create table ALBUM: %s", error);
    }
}

- (void)insertAlbum:(NSString *)nameFile type:(NSInteger)type emotion:(NSString *)emotion nameDairy:(NSString *)nameDairy;
{
    NSLog(@" Thuc hien tao moi  Baby");
    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO ALBUM (NAMEFILE, TYPE, EMOTION, NAMEDAIRY) VALUES ('%@', %d, '%@', '%@')",nameFile, type, emotion, nameDairy];
    char *error;
    
    // Create insert statement for the address
    
    if ( sqlite3_exec(database, [insertSQL UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Insert Object  ALBUM complete");
    }
    else
    {
        NSLog(@"Error insert object ALBUM %s", error);
    }
}

- (NSMutableArray *)getAllAlbumWithNameDairy:(NSString *)nameDairy;
{
    // Allocate a persons array
    NSMutableArray *listAlbum = [[NSMutableArray alloc]init];
    
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM ALBUM WHERE NAMEDAIRY = '%@'", nameDairy];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int iD = sqlite3_column_int(statement, 0);
            
            NSString *nameFile = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            int type = sqlite3_column_int(statement, 2);
            
            NSString *emotion = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            NSString *nameDairy = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            
            Album *objAlbum = [[Album alloc] initWithName:iD nameFile:nameFile emotion:emotion nameDairy:nameDairy typeAlbum:type];
            
            [listAlbum addObject:objAlbum];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return listAlbum;

}
- (void)updateEmotionWithNameFileAlbum:(NSString *)nameFile emotion: (NSString *)emotion;
{
    NSString *replaceStatement = [NSString stringWithFormat:@" UPDATE ALBUM SET EMOTION = '%@' WHERE NAMEFILE = '%@'", nameFile,emotion];
    
    // Execute the replace
    char *error;
    if (sqlite3_exec(database, [replaceStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"update attributes INDEXBABY of table INFO complete");
    }
    else
    {
        NSLog(@"Error update attributes INDEXBABY of table INFO : %s", error);
    }
    
}

- (void)delAlbumWithNameDiary:(NSString *)nameDairy;
{
    sqlite3_stmt *statement;
    NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM ALBUM WHERE NAMEDAIRY = '%@'",nameDairy];
    
    const char *delete_stmt = [deleteSQL UTF8String];
    
    sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
    // loi o cau lenh nay
    if (sqlite3_step(statement) == SQLITE_DONE)
    {
        NSLog(@"xoa thanh cong data trong dairy co ten: %@", nameDairy);
    } else {
        NSAssert(0, @"loi xoa data tu Album");
    }
    sqlite3_finalize(statement);   
}

- (NSMutableArray *)getAllAlbum;
{
    // Allocate a persons array
    NSMutableArray *listAlbum = [[NSMutableArray alloc]init];
    
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM ALBUM "];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int iD = sqlite3_column_int(statement, 0);
            
            NSString *nameFile = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            int type = sqlite3_column_int(statement, 2);
            
            NSString *emotion = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            NSString *nameDairy = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            
            Album *objAlbum = [[Album alloc] initWithName:iD nameFile:nameFile emotion:emotion nameDairy:nameDairy typeAlbum:type];
            
            [listAlbum addObject:objAlbum];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return listAlbum;
}






#pragma mark - table BABYGROW
- (void)initBabyGrow;
{
    // Create the BABYGROWtable
    const char *sqlStatement = "CREATE TABLE IF NOT EXISTS BABYGROW (ID INTEGER PRIMARY KEY AUTOINCREMENT, IDBABY INTEGER, MONTH INTEGER, HEIGHT FLOAT, WEIGHT FLOAT)";
    
    char *error;
    
    if (sqlite3_exec(database, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Create table BABYGROW complete");
    }
    else
    {
        NSLog(@"Error create table BABYGROW: %s", error);
    }

    
}

- (void)insertBabyGrow:(NSInteger)iDBaby month:(NSInteger)month height:(NSNumber *)height weight:(NSNumber *)weight;
{
    NSLog(@" Thuc hien tao moi  thong tin phat trien cua be");
    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO BABYGROW (IDBABY, MONTH, HEIGHT, WEIGHT) VALUES ('%d', '%d','%f', '%f')",iDBaby,month, [height floatValue], [weight floatValue]];
    char *error;
    
    // Create insert statement for the address
    
    if ( sqlite3_exec(database, [insertSQL UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Insert Object  BABYGROW complete");
    }
    else
    {
        NSLog(@"Error insert object BABYGROW %s", error);
    }

    
}

- (NSMutableArray *)getInfoBabyGrowWithIDBaby:(NSInteger)iDBaby;
{
    NSLog(@"==> Call method getInfoBabyGrowWithIDBaby");
    // Allocate a persons array
    NSMutableArray *listAlbum = [[NSMutableArray alloc]init];
    
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM BABYGROW WHERE IDBABY = '%d' ORDER BY MONTH DESC",iDBaby];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
        
            int iD = sqlite3_column_int(statement, 0);
            int month = sqlite3_column_int(statement, 2);
            
            float height = sqlite3_column_double(statement, 3);
            NSNumber *heightNumber = [[NSNumber alloc] initWithFloat:height];
            float weight = sqlite3_column_double(statement, 4);
            NSNumber *weightNumber = [[NSNumber alloc] initWithFloat:weight];            

            BabyGrow *babyGrowTemp = [[BabyGrow alloc] initWithInfo:iD iDBabyInfo:iDBaby month:month height:heightNumber weight:weightNumber];
            [listAlbum addObject:babyGrowTemp];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return listAlbum;

}


- (BOOL)checkMonthOfBabyWithIDAndMonth:(NSInteger)idBaby month:(NSInteger)monthBaby;
{
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM BABYGROW WHERE IDBABY = '%d' AND MONTH = '%d'",idBaby, monthBaby];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
             int month = sqlite3_column_int(statement, 2);
            if (month == monthBaby) {
                return YES;
            }
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return NO;
}

#pragma mark - table COMINGEVENT
- (void)initComingEvent;
{
    NSLog(@"==> call method init schedule table:");
    // Create the DAIRY table
    const char *sqlStatement = "CREATE TABLE IF NOT EXISTS COMINGEVENT (ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT, HOURTIME INTEGER, STATUSREMINDER INTEGER, TYPEREMINDER INTEGER, SUNDAY INTEGER, MONDAY INTEGER, TUESDAY INTEGER, WEDNESDAY INTEGER, THURSDAY INTEGER, FRIDAY INTEGER, SATURDAY INTEGER, RINGTONE TEXT, ALARMREMINDER INTEGER)";
    
    char *error;
    
    if (sqlite3_exec(database, sqlStatement, NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Create table COMINGEVENT complete");
    }
    else
    {
        NSLog(@"Error create table COMINGEVENT: %s", error);
    }
}

- (void)insertComingEvent:(NSString *)titleComingEvent hourTime:(NSInteger )hourTimeComingEvent statusReminder:(NSInteger)statusReminderCommingEvent typeReminder:(NSInteger)typeReminder sunday:(NSInteger)sunday monday:(NSInteger)monday tuesday:(NSInteger)tuesday wednesday:(NSInteger)wednesday thursday:(NSInteger)thursday friday:(NSInteger)friday saturday:(NSInteger)saturday ringtone:(NSString *)ringtoneFile;
{
    NSLog(@"==> call method insert COMINGEVENT:");
    NSInteger createAlarm;
    if (statusReminderCommingEvent == 0) {
        createAlarm = 0;
    }
    else{
        createAlarm = 1;
    }
    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO COMINGEVENT (TITLE, HOURTIME, STATUSREMINDER, TYPEREMINDER, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, RINGTONE, ALARMREMINDER) VALUES ('%@','%d', %d, '%d', '%d', '%d','%d', '%d', '%d', %d, '%d','%@', '%d')",titleComingEvent, hourTimeComingEvent, statusReminderCommingEvent, typeReminder, sunday, monday, tuesday, wednesday, thursday, friday, saturday, ringtoneFile, createAlarm];
    
    char *error;
    
    // Create insert statement for the address
    
    if ( sqlite3_exec(database, [insertSQL UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Insert Object COMINGEVENT complete");
    }
    else
    {
        NSLog(@"Error insert object COMINGEVENT: %s", error);
    }
}

- (NSMutableArray *)getAllComingEvents;
{
    NSLog(@"CALL METHOD GETALL EVENT:");
    // Allocate a persons array
    NSMutableArray *listSchedule = [[NSMutableArray alloc]init];
    
    // Create the query statement to get all persons
    NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM COMINGEVENT"];
    
    // Prepare the query for execution
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [queryStatement UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        NSLog(@"TOI BAT DAU VAO LAY DATA");
        // Iterate over all returned rows
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int iD = sqlite3_column_int(statement, 0);
            NSString *title = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            //
            
            int statusReminder = sqlite3_column_int(statement, 3);
            BOOL statusReminderBool;
            if (statusReminder == 0) {
                statusReminderBool = YES;
            }
            else{
                statusReminderBool = NO;
            }
            
            int typeReminder = sqlite3_column_int(statement, 4);
                        
            int sunday = sqlite3_column_int(statement, 5);
            BOOL sundayBool;
            if (sunday == 0) {
                sundayBool = YES;
            }
            else{
                sundayBool = NO;
            }
            //
            int monday = sqlite3_column_int(statement, 6);
            BOOL mondayBool;
            if (monday == 0) {
                mondayBool = YES;
            }
            else{
                mondayBool = NO;
            }
            //
            int tuesday = sqlite3_column_int(statement, 7);
            BOOL tuesdayBool;
            if (tuesday == 0) {
                tuesdayBool = YES;
            }
            else{
                tuesdayBool = NO;
            }
            //
            int wednesday = sqlite3_column_int(statement, 8);
            BOOL wednesdayBool;
            if (wednesday == 0) {
                wednesdayBool = YES;
            }
            else{
                wednesdayBool = NO;
            }
            //
            int thursday = sqlite3_column_int(statement, 9);
            BOOL thursdayBool;
            if (thursday == 0) {
                thursdayBool = YES;
            }
            else{
                thursdayBool = NO;
            }
            //
            int friday = sqlite3_column_int(statement, 10);
            BOOL fridayBool;
            if (friday == 0) {
                fridayBool = YES;
            }
            else{
                fridayBool = NO;
            }
            //
            int saturday = sqlite3_column_int(statement, 11);
            BOOL saturdayBool;
            if (saturday == 0) {
                saturdayBool = YES;
            }
            else{
                saturdayBool = NO;
            }
            
            // file music
            NSString *ringTone = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 12)];
            int alarm = sqlite3_column_int(statement, 13);
            NSLog(@"DANG DUNG");
            // getTime
            
            // hour time
            NSLog(@"La ra time su kien");
            NSDate *hourEvent= [NSDate dateWithTimeIntervalSince1970:sqlite3_column_int(statement, 2)];
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
            //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
            dateFormatter1.dateFormat = @"MM-DD-YYYY HH:MM";
            NSString  *theDate1 = [dateFormatter1 stringFromDate:hourEvent];
            NSLog(@"theDate %@", theDate1);
            
            
            //
            NSLog(@"TOI CHEN THONG TIN VAO:");
//            ComingEvent *comingEvent = [[ComingEvent alloc] initWithName:iD title:title hourTime:hourEvent statusReminder:[NSNumber numberWithBool:statusReminderBool] typeReminder:typeReminder sunday:[NSNumber numberWithBool:sundayBool] monday:[NSNumber numberWithBool:mondayBool] tuesday:[NSNumber numberWithBool:tuesdayBool] wednesday:[NSNumber numberWithBool:wednesdayBool] thursday:[NSNumber numberWithBool:thursdayBool] friday:[NSNumber numberWithBool:fridayBool] saturday:[NSNumber numberWithBool:saturdayBool] ringtone:ringTone];
            ComingEvent *comingEvent = [[ComingEvent alloc] initWithName:iD title:title hourTime:hourEvent statusReminder:[NSNumber numberWithBool:statusReminderBool] typeReminder:typeReminder sunday:[NSNumber numberWithBool:sundayBool] monday:[NSNumber numberWithBool:mondayBool] tuesday:[NSNumber numberWithBool:tuesdayBool] wednesday:[NSNumber numberWithBool:wednesdayBool] thursday:[NSNumber numberWithBool:thursdayBool] friday:[NSNumber numberWithBool:fridayBool] saturday:[NSNumber numberWithBool:saturdayBool] ringtone:ringTone alarm:alarm];
            
            [listSchedule addObject:comingEvent];
            
        }
        sqlite3_finalize(statement);
    }
    // Return the persons array an mark for autorelease
    return listSchedule;
}

- (void)delComingEventWithID:(NSInteger)iD;
{
    sqlite3_stmt *statement;
    NSString *deleteSQL = [NSString stringWithFormat: @"DELETE FROM COMINGEVENT WHERE ID = '%d'",iD];
    
    const char *delete_stmt = [deleteSQL UTF8String];
    
    sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
    // loi o cau lenh nay
    if (sqlite3_step(statement) == SQLITE_DONE)
    {
        NSLog(@"xoa thanh cong data trong table COMINGEVENT co ten: %d", iD);
    } else {
        NSAssert(0, @"loi xoa data tu COMINGEVENT");
    }
    sqlite3_finalize(statement);
}
- (void)updateStatusReminderWithId:(NSInteger)idComingEvent status:(NSInteger)statusReminder;
{
    NSString *replaceStatement = [NSString stringWithFormat:@" UPDATE COMINGEVENT SET STATUSREMINDER = '%d' WHERE ID = '%d'", statusReminder,idComingEvent];
    
    // Execute the replace
    char *error;
    if (sqlite3_exec(database, [replaceStatement UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"update attributes STATUSREMINDER of table COMINGEVENT complete");
    }
    else
    {
        NSLog(@"Error update attributes STATUSREMINDER of table COMINGEVENT : %s", error);
    }
}

#pragma mark - @ open and close data

- (void)closeData;
{
    sqlite3_close(database);
}


- (void)openData;
{
    // Create a string containing the full path to the sqlite.db inside the documents folder
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:@"sqlite.db"];
    
    // Check to see if the database file already exists
    bool databaseAlreadyExists = [[NSFileManager defaultManager] fileExistsAtPath:databasePath];
    
    // Open the database and store the handle as a data member
    if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        // Create the database if it doesn't yet exists in the file system
        if (!databaseAlreadyExists)
        {
            
        }
    } //open OK

}

#pragma mark - table STANDARDBOY;
-(NSMutableArray *)getAllInfoStandartBoyWithMonth:(NSInteger)monthInfo;
{
    NSMutableArray *listInfoGrow = [[NSMutableArray alloc] init];
    @try {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *dbPath = [[[NSBundle mainBundle] resourcePath ]stringByAppendingPathComponent:@"BABYDAIRY1.sqlite"];
        BOOL success = [fileMgr fileExistsAtPath:dbPath];
        if(!success)
        {
            NSLog(@"Cannot locate database file '%@'.", dbPath);
        }
        if(!(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK))
        {
            NSLog(@"An error has occured.");
        }
        NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM STANDARDBOY WHERE MONTH == '%d' ORDER BY MONTH DESC",monthInfo];
        sqlite3_stmt *sqlStatement;
        if(sqlite3_prepare(database, [queryStatement UTF8String], -1, &sqlStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Problem with prepare statement");
        }
        
        //
        while (sqlite3_step(sqlStatement)==SQLITE_ROW) {
            int iD = sqlite3_column_int(sqlStatement, 0);
            int month = sqlite3_column_int(sqlStatement, 1);
//            float = sqlite3_column_
            NSNumber *lows = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 2)];
            NSNumber *low1 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 3)];
            NSNumber *low2 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 4)];
            NSNumber *standard1 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 5)];
            NSNumber *standard2 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 6)];
            NSNumber *high1 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 7)];
            NSNumber *high2 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 8)];
            NSNumber *highs = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 9)];
            NSNumber *height = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 10)];
            
            StandardGrow *info = [[StandardGrow alloc] initWithID:iD month:month lows:lows low1:low1 low2:low2 standard1:standard1 standard2:standard2 high1Info:high1 hight2Info:high2 hights:highs height:height];
            [listInfoGrow addObject:info];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
    @finally {
        return listInfoGrow;
    }

}

#pragma mark - table STANDARDGIRL
-(NSMutableArray *)getallINfoStandartGirlWithMonth:(NSInteger)monthInfo;
{
    NSMutableArray *listInfoGrow = [[NSMutableArray alloc] init];
    @try {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *dbPath = [[[NSBundle mainBundle] resourcePath ]stringByAppendingPathComponent:@"BABYDAIRY1.sqlite"];
        BOOL success = [fileMgr fileExistsAtPath:dbPath];
        if(!success)
        {
            NSLog(@"Cannot locate database file '%@'.", dbPath);
        }
        if(!(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK))
        {
            NSLog(@"An error has occured.");
        }
        NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM STANDARDBOY WHERE MONTH == '%d' ORDER BY MONTH DESC",monthInfo];
        sqlite3_stmt *sqlStatement;
        if(sqlite3_prepare(database, [queryStatement UTF8String], -1, &sqlStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Problem with prepare statement");
        }
        
        //
        while (sqlite3_step(sqlStatement)==SQLITE_ROW) {
            int iD = sqlite3_column_int(sqlStatement, 0);
            int month = sqlite3_column_int(sqlStatement, 1);
            //            float = sqlite3_column_
            NSNumber *lows = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 2)];
            NSNumber *low1 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 3)];
            NSNumber *low2 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 4)];
            NSNumber *standard1 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 5)];
            NSNumber *standard2 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 6)];
            NSNumber *high1 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 7)];
            NSNumber *high2 = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 8)];
            NSNumber *highs = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 9)];
            NSNumber *height = [NSNumber numberWithFloat:(float)sqlite3_column_double(sqlStatement, 10)];
            
            StandardGrow *info = [[StandardGrow alloc] initWithID:iD month:month lows:lows low1:low1 low2:low2 standard1:standard1 standard2:standard2 high1Info:high1 hight2Info:high2 hights:highs height:height];
            [listInfoGrow addObject:info];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
    @finally {
        return listInfoGrow;
    }
}

#pragma mark - table Knowledge
- (NSMutableArray *)GetAllKnowledgeWithType:(NSInteger)type;
{
    NSMutableArray *listInfo = [[NSMutableArray alloc] init];
    @try {
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *dbPath = [[[NSBundle mainBundle] resourcePath ]stringByAppendingPathComponent:@"BABYDAIRY1.sqlite"];
        BOOL success = [fileMgr fileExistsAtPath:dbPath];
        if(!success)
        {
            NSLog(@"Cannot locate database file '%@'.", dbPath);
        }
        if(!(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK))
        {
            NSLog(@"An error has occured.");
        }
        NSString *queryStatement = [NSString stringWithFormat:@"SELECT * FROM KNOWLEDGE WHERE TYPE = '%d' ",type];
        sqlite3_stmt *sqlStatement;
        if(sqlite3_prepare(database, [queryStatement UTF8String], -1, &sqlStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Problem with prepare statement");
        }
        
        //
        while (sqlite3_step(sqlStatement)==SQLITE_ROW) {
            
            NSString *name = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement,1)];
            NSString *nameFile = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement,2)];
            int minOld = sqlite3_column_int(sqlStatement, 4);
            int maxOld = sqlite3_column_int(sqlStatement, 5);
            
            Knowledge *objInfo = [[Knowledge alloc] initwithName:name nameFile:nameFile type:type minOld:minOld macOld:maxOld];
            
            [listInfo addObject:objInfo];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
    @finally {
        return listInfo;
    }
}


#pragma mark - @ method dung fix bug

- (void)initGetMyRoiDo;
{
    NSString *createSQL = @"CREATE TABLE IF NOT EXISTS PERSONALINFO "
    "(ID INTEGER PRIMARY KEY, AVATAR TEXT, NAME TEXT, GENDER TEXT, BIRTHDAY TEXT, ADDRESS TEXT, EMAIL TEXT, PHONE TEXT, ACCOUNT TEXT, PASSWORD TEXT, REMEMBER TEXT);";
    char *errorMsg;
    if (sqlite3_exec (database, [createSQL UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK) {
        
        NSAssert(0, @"Error creating table: %s", errorMsg);
    }
    else{
        NSLog(@"CHEN THU THANH => GET MY KHONG NOI");
    }
}


- (void)insertDairyTemp:(NSString *)nameDairy title:(NSString *)titleDairy  emotion:(NSString *)emotionDairy address:(NSString *)addressDairy;
{
    NSString *insertSQL = [NSString stringWithFormat: @"INSERT INTO DAIRY (NAMEDAIRY, TITLE, ADDRESS, EMOTION) VALUES ('%@', '%@', '%@', '%@')", nameDairy, titleDairy,  addressDairy, emotionDairy];
    char *error;
    
    // Create insert statement for the address
    
    if ( sqlite3_exec(database, [insertSQL UTF8String], NULL, NULL, &error) == SQLITE_OK)
    {
        NSLog(@"Insert dairy object complete");
    }
    else
    {
        NSLog(@"Error insert dairy object: %s", error);
        
    }
}

@end
