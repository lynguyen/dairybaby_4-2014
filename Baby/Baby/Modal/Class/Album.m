//
//  Album.m
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "Album.h"

@implementation Album
@synthesize iD,nameDairy,emotion,type,nameFile;

-(id)initWithName:(NSInteger)idAlbum nameFile:(NSString *)nameFileAlbum emotion:(NSString *)emotionAlbum nameDairy:(NSString *)nameDairyAlbum typeAlbum:(NSInteger)typeAlbum;
{
    self.iD = idAlbum;
    self.nameFile = nameFileAlbum;
    self.emotion = emotionAlbum;
    self.nameDairy = nameDairyAlbum;
    self.type = typeAlbum;
    
    return self;
}
@end
