//
//  ComingEvent.h
//  Baby
//
//  Created by kim ly on 4/22/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ComingEvent : NSObject
{
    NSInteger iD;
    NSString *title;
    NSDate *hourTime;
    NSNumber *statusReminder;
    NSInteger typeReminder;
    
    NSNumber *sunday;
    NSNumber *monday;
    NSNumber *tuesday;
    NSNumber *wednesday;
    NSNumber *thursday;
    NSNumber *friday;
    NSNumber *saturday;
    NSString *ringtone;
    NSInteger alarm;
}

@property(assign, nonatomic)NSInteger iD;
@property(strong, nonatomic)NSString *title;
@property(strong, nonatomic)NSDate *hourTime;
@property(strong, nonatomic)NSNumber *statusReminder;
@property(assign, nonatomic)NSInteger typeReminder;


@property(strong, nonatomic)NSNumber *sunday;
@property(strong, nonatomic)NSNumber *monday;
@property(strong, nonatomic)NSNumber *tuesday;
@property(strong, nonatomic)NSNumber *wednesday;
@property(strong, nonatomic)NSNumber *thursday;
@property(strong, nonatomic)NSNumber *friday;
@property(strong, nonatomic)NSNumber *saturday;
@property(strong, nonatomic)NSString *ringtone;
@property(assign, nonatomic)NSInteger alarm;

- (id)initWithName:(NSInteger)idComingEvent title:(NSString *)titleComingEvent hourTime:(NSDate *)hourTimeComingEvnet statusReminder:(NSNumber *)statusReminderComingEvent typeReminder:(NSInteger)typeRemindeComingEvent sunday:(NSNumber *)sundayReminder monday:(NSNumber *)mondayReminder tuesday:(NSNumber *)tuesdayReminder wednesday:(NSNumber *)wednesdayReminder thursday:(NSNumber *)thursdayReminder friday:(NSNumber *)fridayReminder saturday:(NSNumber *)saturdayReminder ringtone:(NSString *)ringtoneReminder alarm:(NSInteger)statusAlarm;

@end
