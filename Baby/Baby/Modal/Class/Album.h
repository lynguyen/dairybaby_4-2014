//
//  Album.h
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Album : NSObject{
    NSInteger iD;
    NSString *nameFile;
    NSInteger type;
    NSString *emotion;
    NSString *nameDairy;
    
}
@property(assign, nonatomic)NSInteger iD;
@property(strong, nonatomic)NSString *nameFile;
@property(assign, nonatomic)NSInteger type;
@property(strong, nonatomic)NSString *emotion;
@property(strong, nonatomic)NSString *nameDairy;


-(id)initWithName:(NSInteger)idAlbum nameFile:(NSString *)nameFileAlbum emotion:(NSString *)emotionAlbum nameDairy:(NSString *)nameDairyAlbum typeAlbum:(NSInteger)typeAlbum;

@end
