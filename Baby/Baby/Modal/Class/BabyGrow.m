//
//  BabyGrow.m
//  Baby
//
//  Created by kim ly on 4/14/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "BabyGrow.h"

@implementation BabyGrow
//@synthesize iDBaby, iD, height, weight, month;
@synthesize iD, iDBaby, height, weight, month;

- (id)initWithInfo:(NSInteger)iDInfo iDBabyInfo:(NSInteger)iDBabyInfo month:(NSInteger)monthInfo height:(NSNumber *)heightInfo weight:(NSNumber *)weightInfo;
{
    self.iD = iDBabyInfo;
    self.month = monthInfo;
    self.iDBaby = iDBabyInfo;
    self.height = heightInfo;
    self.weight = weightInfo;
    
    return self;
}

@end
