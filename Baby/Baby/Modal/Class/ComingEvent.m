//
//  ComingEvent.m
//  Baby
//
//  Created by kim ly on 4/22/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "ComingEvent.h"

@implementation ComingEvent
@synthesize iD, title, hourTime, statusReminder, typeReminder, sunday, monday, tuesday, wednesday, thursday, friday, saturday, ringtone, alarm;

- (id)initWithName:(NSInteger)idComingEvent title:(NSString *)titleComingEvent hourTime:(NSDate *)hourTimeComingEvnet statusReminder:(NSNumber *)statusReminderComingEvent typeReminder:(NSInteger)typeRemindeComingEvent sunday:(NSNumber *)sundayReminder monday:(NSNumber *)mondayReminder tuesday:(NSNumber *)tuesdayReminder wednesday:(NSNumber *)wednesdayReminder thursday:(NSNumber *)thursdayReminder friday:(NSNumber *)fridayReminder saturday:(NSNumber *)saturdayReminder ringtone:(NSString *)ringtoneReminder alarm:(NSInteger)statusAlarm;
{
    self.iD = idComingEvent;
    self.title = titleComingEvent;
    self.hourTime = hourTimeComingEvnet;
    self.statusReminder = statusReminderComingEvent;
    self.typeReminder = typeRemindeComingEvent;
   
    
    self.sunday = sundayReminder;
    self.monday = mondayReminder;
    self.tuesday = tuesdayReminder;
    self.wednesday = wednesdayReminder;
    self.thursday = thursdayReminder;
    self.friday = fridayReminder;
    self.saturday = saturdayReminder;
    
    self.ringtone = ringtoneReminder;
    self.alarm = statusAlarm;
    
    return self;
}
@end
