//
//  Baby.h
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Baby : NSObject{
    NSInteger iD;
    NSString *name;// 0 -Nam ; 1 -Nu;
    NSInteger gender;
    NSDate *birthday;
    NSString *like;
    NSString *avata;
}

@property(assign, nonatomic)NSInteger iD;
@property(strong, nonatomic)NSString *name;
@property(assign, nonatomic)NSInteger gender;
@property(strong, nonatomic)NSDate *birthday;
@property(strong, nonatomic)NSString *like;
@property(strong, nonatomic)NSString *avata;

- (id)initWithName:(NSInteger)iDBaby nameBaby:(NSString *)nameBaby genderBaby:(NSInteger)genderBaby birthdayBaby:(NSDate *)birthdayBaby likeBaby:(NSString *)likeBaby avataBaby:(NSString *)avataBaby;

@end
