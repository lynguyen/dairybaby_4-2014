//
//  Dairy.m
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "Dairy.h"

@implementation Dairy
@synthesize iD,name, time,title,emotion, address,avata, index;
- (id)initWithName:(NSInteger)iDDairy name:(NSString *)nameDairy title:(NSString *)titleDairy avata:(NSInteger)avataDairy address:(NSString *)addressDairy time:(NSDate *)timeDairy emotion:(NSString *)emotionDairy index:(NSInteger) indexDairy;
{
    self.iD = iDDairy;
    self.title = titleDairy;
    self.emotion = emotionDairy;
    self.time = timeDairy;
    self.avata = avataDairy;
    self.address = addressDairy;
    self.name = nameDairy;
    self.index = indexDairy;

    return self;
}
@end
