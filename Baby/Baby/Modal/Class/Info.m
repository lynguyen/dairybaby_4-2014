//
//  Info.m
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "Info.h"

@implementation Info
@synthesize iD, indexBaby, indexDairy;

- (id)initWithName:(NSInteger)indDairy andIndexBaby:(NSInteger)indBaby;
{
    self.indexDairy = indDairy;
    self.indexBaby = indBaby;
    return self;
}

@end
