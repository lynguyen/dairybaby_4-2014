//
//  Baby.m
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "Baby.h"

@implementation Baby
@synthesize iD,name,gender,avata,like,birthday;

- (id)initWithName:(NSInteger)iDBaby nameBaby:(NSString *)nameBaby genderBaby:(NSInteger)genderBaby birthdayBaby:(NSDate *)birthdayBaby likeBaby:(NSString *)likeBaby avataBaby:(NSString *)avataBaby;
{
    self.iD = iDBaby;
    self.name = nameBaby;
    self.gender = genderBaby;
    self.birthday = birthdayBaby;
    self.like = likeBaby;
    self.avata = avataBaby;
    
    return self;
}
@end
