//
//  BabyGrow.h
//  Baby
//
//  Created by kim ly on 4/14/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BabyGrow : NSObject
{
    NSInteger iD;
    NSInteger month;
    NSNumber *height;
    NSNumber *weight;
    NSInteger iDBaby;
}

@property(assign, nonatomic)NSInteger iD;
@property(assign, nonatomic)NSInteger month;
@property(strong, nonatomic)NSNumber* height;
@property(strong, nonatomic)NSNumber* weight;
@property(assign, nonatomic)NSInteger iDBaby;

- (id)initWithInfo:(NSInteger)iDInfo iDBabyInfo:(NSInteger)iDBabyInfo month:(NSInteger)monthInfo height:(NSNumber *)heightInfo weight:(NSNumber *)weightInfo;


@end
