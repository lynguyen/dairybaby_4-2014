//
//  Dairy.h
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dairy : NSObject{
    
    NSInteger iD;
    NSString *name;
    NSString *title;
    NSInteger avata;
    NSString *address;
    NSDate *time;
    NSString *emotion;
    NSInteger index;
}

@property(assign, nonatomic)NSInteger iD;
@property(strong, nonatomic)NSString *name;
@property(strong, nonatomic)NSString *title;
@property(assign, nonatomic)NSInteger avata;
@property(strong, nonatomic)NSString *address;
@property(strong, nonatomic)NSDate *time;
@property(strong, nonatomic)NSString *emotion;
@property(assign, nonatomic)NSInteger index;

- (id)initWithName:(NSInteger)iDDairy name:(NSString *)nameDairy title:(NSString *)titleDairy avata:(NSInteger)avataDairy address:(NSString *)addressDairy time:(NSDate *)timeDairy emotion:(NSString *)emotionDairy index:(NSInteger) indexDairy;



@end
