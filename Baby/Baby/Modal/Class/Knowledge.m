//
//  Knowledge.m
//  Baby
//
//  Created by kim ly on 4/18/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "Knowledge.h"

@implementation Knowledge
@synthesize name, nameFile, type, maxOld, minOld;

- (id)initwithName:(NSString *)NameInfo nameFile:(NSString *)nameFileInfo type:(NSInteger)typeInfo minOld:(NSInteger)minOldInfo macOld:(NSInteger)maxOldInfo;
{
    self.name = NameInfo;
    self.nameFile = nameFileInfo;
    self.type = typeInfo;
    self.minOld = minOldInfo;
    self.maxOld = maxOldInfo;
    return self;
}

@end
