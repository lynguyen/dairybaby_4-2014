//
//  StandardGrow.m
//  Baby
//
//  Created by kim ly on 5/5/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "StandardGrow.h"

@implementation StandardGrow
@synthesize iD,month, levelLows, levelLow1, levelLow2, levelStandard1, levelStandard2, levelHigh1, levelHigh2 , levelHighs, height;

- (id)initWithID:(NSInteger)iDInfo month:(NSInteger)monthInfo lows:(NSNumber *)levelLowsInfo low1:(NSNumber *)levelLow1Info low2: (NSNumber *)levelLow2Info standard1:(NSNumber *)levelStandard1Info standard2:(NSNumber *)levelStandard2Infor high1Info:(NSNumber *)levelHigh1Info hight2Info:(NSNumber *)levelHight2Info hights:(NSNumber *)levelHighsInfo height:(NSNumber *)heightInfo;
{
    self.iD = iDInfo;
    self.month = monthInfo;
    self.levelLows = levelLowsInfo;
    self.levelLow1 = levelLow1Info;
    self.levelLow2 = levelLow2Info;
    self.levelStandard1 = levelStandard1Info;
    self.levelStandard2 = levelStandard2Infor;
    self.levelHigh1 = levelHigh1Info;
    self.levelHigh2 = levelHight2Info;
    self.levelHighs = levelHighsInfo;
    self.height = heightInfo;
    return self;
}

@end
