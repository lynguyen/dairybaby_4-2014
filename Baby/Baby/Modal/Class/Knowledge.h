//
//  Knowledge.h
//  Baby
//
//  Created by kim ly on 4/18/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Knowledge : NSObject{
    NSString *name;
    NSString *nameFile;
    NSInteger type;
    NSInteger maxOld;
    NSInteger minOld;
}

@property(strong, nonatomic)NSString *name;
@property(strong, nonatomic)NSString *nameFile;
@property(assign, nonatomic)NSInteger type;
@property(assign, nonatomic)NSInteger maxOld;
@property(assign, nonatomic)NSInteger minOld;

- (id)initwithName:(NSString *)NameInfo nameFile:(NSString *)nameFileInfo type:(NSInteger)typeInfo minOld:(NSInteger)minOldInfo macOld:(NSInteger)maxOldInfo;

@end
