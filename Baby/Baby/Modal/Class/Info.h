//
//  Info.h
//  Baby
//
//  Created by kim ly on 3/31/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Info : NSObject{
    NSInteger iD;
    NSInteger indexDairy;
    NSInteger indexBaby;
}
@property(assign, nonatomic)NSInteger iD;
@property(assign, nonatomic)NSInteger indexDairy;
@property(assign, nonatomic)NSInteger indexBaby;

- (id)initWithName:(NSInteger)indDairy andIndexBaby:(NSInteger)indBaby;
@end
