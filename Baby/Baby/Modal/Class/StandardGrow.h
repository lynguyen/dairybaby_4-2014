//
//  StandardGrow.h
//  Baby
//
//  Created by kim ly on 5/5/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StandardGrow : NSObject
{
    NSInteger iD;
    NSInteger month;
    NSNumber *levelLows;
    NSNumber *levelLow1;
    NSNumber *levelLow2;
    NSNumber *levelStandard1;
    NSNumber *levelStandard2;
    NSNumber *levelHigh1;
    NSNumber *levelHigh2;
    NSNumber *levelHighs;
    NSNumber *height;
}

@property(assign, nonatomic)NSInteger iD;
@property(assign, nonatomic)NSInteger month;
@property(strong, nonatomic)NSNumber *levelLows;
@property(strong, nonatomic)NSNumber *levelLow1;
@property(strong, nonatomic)NSNumber *levelLow2;
@property(strong, nonatomic)NSNumber *levelStandard1;
@property(strong, nonatomic)NSNumber *levelStandard2;
@property(strong, nonatomic)NSNumber *levelHigh1;
@property(strong, nonatomic)NSNumber *levelHigh2;
@property(strong, nonatomic)NSNumber *levelHighs;
@property(strong, nonatomic)NSNumber *height;


- (id)initWithID:(NSInteger)iDInfo month:(NSInteger)monthInfo lows:(NSNumber *)levelLowsInfo low1:(NSNumber *)levelLow1Info low2: (NSNumber *)levelLow2Info standard1:(NSNumber *)levelStandard1Info standard2:(NSNumber *)levelStandard2Infor high1Info:(NSNumber *)levelHigh1Info hight2Info:(NSNumber *)levelHight2Info hights:(NSNumber *)levelHighsInfo height:(NSNumber *)heightInfo;
@end
