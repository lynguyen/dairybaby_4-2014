//
//  DetailViewController.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NhatKyViewController.h"
#import "QuanLyViewController.h"
#import "LapLichViewController.h"
#import "TroGiupViewController.h"
#import "KienThucViewController.h"
#import "TaoMoiNhatKyViewController.h"
#import "TaoMoiSuKienViewController.h"
#import "SQLiteDB.h"

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>{
    UIViewController* topController;
    int indexVC;
    SQLiteDB *database;
    
}
@property (strong, nonatomic) id detailItem;
@property(strong, nonatomic)NhatKyViewController *nhatKy;
@property(strong, nonatomic)QuanLyViewController *quanLy;
@property(strong, nonatomic)TroGiupViewController *help;
@property(strong, nonatomic)LapLichViewController *lapLich;
@property(strong, nonatomic)KienThucViewController *kienThuc;
@property(strong, nonatomic)UINavigationController *naveHelp;
@property(strong, nonatomic)UIBarButtonItem *addNhatKyBarButton;
@property(strong, nonatomic)UIBarButtonItem *addLapLichBarButton;
@property(strong, nonatomic)UIBarButtonItem *updateLapLichBarButon;

-(void)showViewWithId:(int)viewId;



@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
