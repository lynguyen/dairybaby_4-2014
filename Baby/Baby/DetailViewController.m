//
//  DetailViewController.m
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - methods bat su kien click UIButtonBarItem

// method duoc goi khi click vao UIBarButton tao moi nhat ky.
// chuyen sang screen moi cho phep tao moi nhat ky. goi view controller co ten: TaoMoiNhatKyViewController
- (void)newDairy{
    
    TaoMoiNhatKyViewController *newDairy = [self.storyboard instantiateViewControllerWithIdentifier:@"taoMoiNhatKyViewController"];
    UINavigationController *naviDairy = [[UINavigationController alloc] initWithRootViewController:newDairy];
     naviDairy.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:naviDairy animated:YES completion:nil];
    [database closeData];
}


// method duoc goi khi click vao UIBarButton tao moi su kien sap toi ve be.
// chuyen sang screen moi cho phep tao su kien moi. goi view controller co ten: TaoMoiSuKienViewController
- (void)newSchedule{
    
    TaoMoiSuKienViewController *newEvent = [self.storyboard instantiateViewControllerWithIdentifier:@"taoMoiSuKienViewController"];
    UINavigationController *naviEventSchedule = [[UINavigationController alloc] initWithRootViewController:newEvent];
    naviEventSchedule.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:naviEventSchedule animated:YES completion:nil];
}



#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

-(void)showChildViewController:(UIViewController*)content {
    if(topController != content) {
        content.view.frame = [self.view frame]; // 2
        [self.view addSubview:content.view];
        [content didMoveToParentViewController:self];
        topController = content;
    }
}



- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
    }
}


-(void)showViewWithId:(int)viewId{
    
    switch (viewId) {
        case 0:{
            NhatKyViewController * viewController;
            viewController = self.nhatKy;
            [self showChildViewController:viewController];
            
            // check index duoc chon trong chuc nang
            indexVC = 0;
            NSLog(@"index = 0");
            
            self.navigationItem.title = @"Nhật Ký";
            //self.addButton.enabled = YES;
            self.navigationItem.rightBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = self.addNhatKyBarButton;
            [self.masterPopoverController dismissPopoverAnimated:YES];
            break;
            
        }
        case 1:{
            QuanLyViewController *viewController;
            viewController = self.quanLy;
            [self showChildViewController:viewController];
            
            // check index duoc chon trong chuc nang
            indexVC = 1;
            NSLog(@"index = 1");
            self.navigationItem.title = @"Quản Lý";
            
            self.navigationItem.rightBarButtonItem = nil;
            [self.masterPopoverController dismissPopoverAnimated:YES];
            break;
        }
            
        case 2:{
            
            LapLichViewController *viewController;
            viewController = self.lapLich;
            [self showChildViewController:viewController];
            
            // check index duoc chon trong chuc nang
            indexVC = 2;
            NSLog(@"index = 2");
            self.navigationItem.rightBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = self.addLapLichBarButton;
            self.navigationItem.rightBarButtonItem.enabled  = YES;
            self.navigationItem.title = @"Sự kiện ";
            
            [self.masterPopoverController dismissPopoverAnimated:YES];
            break;
        }
        case 3:{
            KienThucViewController *viewController;
            viewController = self.kienThuc;
            [self showChildViewController:viewController];
            
            // check index duoc chon trong chuc nang
            indexVC = 3;
            NSLog(@"indec = 3");
            self.navigationItem.rightBarButtonItem.enabled = NO;
            self.navigationItem.rightBarButtonItem = nil;
            
            self.navigationItem.title = @"Kiến Thức Nuôi Dạy Trẻ";
            [self.masterPopoverController dismissPopoverAnimated:YES];
            break;
        }
        case 4:{
            //TroGiupViewController *viewController;
            UINavigationController *naviHelp1 = self.naveHelp;
            [self showChildViewController:naviHelp1];
            self.navigationItem.rightBarButtonItem = nil;
            
            indexVC = 4;
            self.navigationItem.title = @"Trợ Giúp Sử Dụng";
            //self.navigationItem.rightBarButtonItem = nil;
            //self.addButton.enabled = NO;
            NSLog(@" index = 4");
            [self.masterPopoverController dismissPopoverAnimated:YES];
            break;
        }
            
            
    }
    //[self showChildViewController:viewController];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    database = [[SQLiteDB alloc] init];
    
    [self configureView];
    indexVC =0;
    
   // set mau cho navicontroller
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    
    
    // khoi tao 2 button "add" tuong ung voi 2 view controller Nhat ky, Lap lich
    self.addNhatKyBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newDairy)];
    self.navigationItem.rightBarButtonItem = self.addNhatKyBarButton;
    
    self.addLapLichBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newSchedule)];
                               
    // add cac view controller chuc nang trong detail view controller
    self.nhatKy = [self.storyboard instantiateViewControllerWithIdentifier:@"nhatKyViewController"];
    
    self.quanLy = [self.storyboard instantiateViewControllerWithIdentifier:@"quanLyViewController"];
    self.lapLich = [self.storyboard instantiateViewControllerWithIdentifier:@"lapLichViewController"];
    self.kienThuc = [self.storyboard instantiateViewControllerWithIdentifier:@"kienThucViewController"]; 
   
    self.naveHelp = [self.storyboard instantiateViewControllerWithIdentifier:@"naviHelp"];
    
    self.help = [self.storyboard instantiateViewControllerWithIdentifier:@"troGiupViewController"];
    
    
    
    [self addChildViewController:self.nhatKy];
    [self addChildViewController:self.quanLy];
    [self addChildViewController:self.lapLich];
    [self addChildViewController:self.kienThuc];
    [self addChildViewController:self.help];
    
    //
 
      [self showChildViewController:self.nhatKy];
          // set view default cuar detail view controller la chuc nang nhat ky
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    
    
    //barButtonItem.title = NSLocalizedString(@"Chức Năng", @"Chức Năng");
    //UIBarButtonItem *newBut = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_delDairy.png"] style:UIBarButtonItemStyleBordered target: barButtonItem.target action: barButtonItem.action];
    //[[self navigationItem] setLeftBarButtonItem:newBut];
    barButtonItem.image = [UIImage imageNamed:@"icon_menu.png"];
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation{
    return  YES;
}


@end
