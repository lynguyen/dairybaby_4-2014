//
//  KienThucTableViewCell.h
//  Baby
//
//  Created by kim ly on 4/18/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KienThucTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageShow;
@property (strong, nonatomic) IBOutlet UILabel *nameShow;

@end
