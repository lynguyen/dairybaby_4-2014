//
//  TaoMoiSuKienCollectionViewCell.h
//  Baby
//
//  Created by kim ly on 4/21/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaoMoiSuKienCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameDay;
@property (strong, nonatomic) IBOutlet UIImageView *imageSelectDay;

@end
