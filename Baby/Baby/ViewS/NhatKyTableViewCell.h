//
//  NhatKyTableViewCell.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NhatKyTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *avata;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *numberImageVideo;
@property (strong, nonatomic) IBOutlet UILabel *timeDairyObj;
@property (strong, nonatomic) IBOutlet UITextView *emotion;
@property (strong, nonatomic) IBOutlet UIImageView *imageVideoFirst;
@property (strong, nonatomic) IBOutlet UIImageView *imageVideoSecond;
@property (strong, nonatomic) IBOutlet UIImageView *imageVideoThird;
@property (strong, nonatomic) IBOutlet UILabel *address;

@property (strong, nonatomic) IBOutlet UIImageView *imageVideoFourth;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicatorFirst;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicatorSecond;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicatorThird;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicatorFourth;
- (IBAction)dellDairy:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *delObj;

@property (strong, nonatomic) IBOutlet UIView *viewImage1;
@property (strong, nonatomic) IBOutlet UIView *viewImage2;
@property (strong, nonatomic) IBOutlet UIView *viewImage3;

@property (strong, nonatomic) IBOutlet UIView *viewImage4;
@end
