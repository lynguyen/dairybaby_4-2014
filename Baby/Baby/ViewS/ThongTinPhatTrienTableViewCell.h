//
//  ThongTinPhatTrienTableViewCell.h
//  Baby
//
//  Created by kim ly on 4/4/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThongTinPhatTrienTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *mothInfo;
@property (strong, nonatomic) IBOutlet UILabel *heightInfo;
@property (strong, nonatomic) IBOutlet UILabel *weightInfo;

@end
