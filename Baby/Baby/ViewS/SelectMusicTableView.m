//
//  SelectMusicTableView.m
//  Baby
//
//  Created by kim ly on 4/21/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "SelectMusicTableView.h"

@interface SelectMusicTableView ()

@end

@implementation SelectMusicTableView

- (id)initWithStyle:(UITableViewStyle)style;
{
    if ([super initWithStyle:style] != nil) {
        //self.tableView.backgroundColor = [UIColor colorWithRed:238/255 green:238/255 blue:209/255 alpha:1.0];
        self.tableView.backgroundColor = [UIColor colorWithRed:224.0/255 green:238.0/255 blue:224.0/255 alpha:1.0];
        //Initialize the array
        self.listNameFileMusic = [[NSMutableArray alloc] initWithObjects:@"Bé hứa bé ngoan", @"Bố là tất cả", @"Cả nhà thương nhau", @"Cả Tuần đều ngoan", @"Chú ếch con", @"Chú voi con ở Bản Đôn", @"Con Heo Đất", @"Đưa cơm cho mẹ đi cày", @"Ếch ngồi đáy giếng", @"Em tập lái ôtô",  nil];
        self.listFileMusic = [[NSMutableArray alloc] initWithObjects: @"BeHuaBeNgoan", @"BoLaTatCa", @"CaNhaThuongNhau", @"CaTuanDeuNgoan", @"ChuEchCon",@"ChuVoiConOBanDon", @"ConHeoDat", @"DuaComChoMeDiCay", @"EchNgoiDayGieng", @"EmTapLaiOto",  nil];
        
        
        
        //Make row selections persist.
        self.clearsSelectionOnViewWillAppear = NO;
        
//        //Calculate how tall the view should be by multiplying the individual row height
//        //by the total number of rows.
//        NSInteger rowsCount = [self.listNameFileMusic count];
//        NSInteger singleRowHeight = [self.tableView.delegate tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//        NSInteger totalRowsHeight = rowsCount * singleRowHeight;
//        
//        //Calculate how wide the view should be by finding how wide each string is expected to be
//        CGFloat largestLabelWidth = 0;
//        for (NSString *colorName in self.listNameFileMusic) {
//            largestLabelWidth = 70;
//        }
//        
//        //Add a little padding to the width
//        CGFloat popoverWidth = largestLabelWidth + 200;
//        
//        //Set the property to tell the popover container how big this view will be.
//        self.contentSizeForViewInPopover = CGSizeMake(popoverWidth, totalRowsHeight);
        
    }
    
    return self;

}

- (void)viewDidLoad;
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning;
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{

    // Return the number of rows in the section.
    return [self.listNameFileMusic count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    // Configure the cell...
//    
//    return cell;
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text = [self.listNameFileMusic objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *nameFileMusic = [self.listNameFileMusic objectAtIndex:indexPath.row];
    NSString *fileMusic = [self.listFileMusic objectAtIndex:indexPath.row];

   
    if (_delegate != nil) {
        [_delegate selectedFileMusic:fileMusic];
        [_delegate selectNameFileMusic:nameFileMusic];
    }

}








/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
