//
//  RemindersTimeTableView.h
//  Baby
//
//  Created by kim ly on 4/22/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RemindersTimeTalbeViewDelegate <NSObject>
@required
- (void)selectedRemindersTime:(NSInteger)remindersTime;
- (void)selectedNameRemindersTime:(NSString *)name;
@end

@interface RemindersTimeTableView : UITableViewController
@property (nonatomic, strong) NSMutableArray *listRemindersTime;
@property (nonatomic, weak) id<RemindersTimeTalbeViewDelegate> delegate;
@end
