//
//  SelectMusicTableView.h
//  Baby
//
//  Created by kim ly on 4/21/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectMusicDelegate <NSObject>

@required
- (void)selectedFileMusic:(NSString *)fileMusic;
- (void)selectNameFileMusic:(NSString *)nameFile;
@end

@interface SelectMusicTableView : UITableViewController

@property (nonatomic, strong) NSMutableArray *listNameFileMusic;
@property(strong, nonatomic)NSMutableArray *listFileMusic;
@property (nonatomic, weak) id<SelectMusicDelegate>delegate;

@end
