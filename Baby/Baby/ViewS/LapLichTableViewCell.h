//
//  LapLichTableViewCell.h
//  Baby
//
//  Created by kim ly on 4/21/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LapLichTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleComingEvent;
@property (strong, nonatomic) IBOutlet UILabel *timeComingEvent;
@property (strong, nonatomic) IBOutlet UISwitch *statusReminder;

@end
