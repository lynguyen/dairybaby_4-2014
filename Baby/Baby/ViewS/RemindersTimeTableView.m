//
//  RemindersTimeTableView.m
//  Baby
//
//  Created by kim ly on 4/22/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "RemindersTimeTableView.h"

@interface RemindersTimeTableView ()

@end

@implementation RemindersTimeTableView

- (id)initWithStyle:(UITableViewStyle)style
{
    if ([super initWithStyle:style] != nil) {
        self.tableView.backgroundColor = [UIColor greenColor];
        
//        self.listRemindersTime = [[NSMutableArray alloc] initWithObjects:@"Trước 5 phút", @"Trước 15 phút", @"Trước 30 phút", @"Trước 1 giờ",@"Trước 2 giờ", @"Trước 3 giờ", @"Trước 1 ngày",  nil];
//        
//        //Make row selections persist.
//        self.clearsSelectionOnViewWillAppear = NO;
//        
//        //Calculate how tall the view should be by multiplying the individual row height
//        //by the total number of rows.
//        NSInteger rowsCount = [self.listRemindersTime count];
//        NSInteger singleRowHeight = [self.tableView.delegate tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//        NSInteger totalRowsHeight = rowsCount * singleRowHeight;
//        
//        //Calculate how wide the view should be by finding how wide each string is expected to be
//        CGFloat largestLabelWidth = 0;
//        for (NSString *colorName in self.listRemindersTime) {
//            
//            largestLabelWidth = 70;
//        }
//        
//        //Add a little padding to the width
//        CGFloat popoverWidth = largestLabelWidth + 100;
//        
//        
//        self.contentSizeForViewInPopover = CGSizeMake(popoverWidth, totalRowsHeight);
        
    }
    
    return self;

}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [self.listRemindersTime count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    
//    return cell;
    //
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text = [self.listRemindersTime objectAtIndex:indexPath.row];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //@"Trước 5 phút", @"Trước 15 phút", @"Trước 30 phút", @"Trước 1 giờ",@"Trước 2 giờ", @"Trước 3 giờ", @"Trước 1 ngày",
    NSInteger selectTime;
    
    if(indexPath.row == 1)//15'
    {
//        remindersTime =
        selectTime = 15;
        
    }
    else if (indexPath.row == 2)//30'
    {
        selectTime = 30;
    }
    else if(indexPath.row ==3)//1h
    {
        selectTime = 60;
    }
    else if(indexPath.row == 4)//2h
    {
        selectTime = 2*60;
    }
    else if (indexPath.row == 5)//3h
    {
        selectTime = 3*60;
    }
    else if(indexPath.row ==6) //1 ngay
    {
        selectTime = 24*60;
    }
    
    else{// lua chon va mac dinh truoc 5'
        selectTime = 5;
    }
    
    
    
    
    if (_delegate != nil) {
        [_delegate selectedNameRemindersTime:[self.listRemindersTime objectAtIndex:indexPath.row]];
        [_delegate selectedRemindersTime:selectTime];
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
