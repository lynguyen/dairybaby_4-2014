//
//  KienThucCollectionViewCell.h
//  Baby
//
//  Created by kim ly on 5/7/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KienThucCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *knowledgeImageView;

@end
