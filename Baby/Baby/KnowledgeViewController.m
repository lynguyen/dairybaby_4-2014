//
//  KnowledgeViewController.m
//  Baby
//
//  Created by kim ly on 5/6/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "KnowledgeViewController.h"
#import "KnowledgeCell.h"
#import "Knowledge.h"
@interface KnowledgeViewController ()

@end

@implementation KnowledgeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    database = [[SQLiteDB alloc] init];
    lists = [[NSMutableArray alloc] init];
    lists = [database GetAllKnowledgeWithType:2];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundWeb.png"]];
//    lists = [[NSMutableArray alloc] initWithObjects:, nil];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [lists count];
}
- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"kt";
    KnowledgeCell *cell = nil;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    Knowledge *obj = [[Knowledge alloc] init];
    obj = [lists objectAtIndex:indexPath.row];
    //cell.ktImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", obj.name]];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"kt.png"]];
//    cell.selected = YES;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Toi Dang Lua Chon");
    
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Toi Dang Lua ChonXXXX");
    
    
}

@end
