//
//  MasterViewController.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>{
    int indexFunction;
}

@property (strong, nonatomic) DetailViewController *detailViewController;

// danh sach ten cac chuc nang trong ung dun

@property (nonatomic,retain)NSArray *listFunction;

// danh sach ten icon cua funtions
@property (nonatomic, strong)NSArray *listImageFunction;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
