//
//  KnowledgeViewController.h
//  Baby
//
//  Created by kim ly on 5/6/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLiteDB.h"

@interface KnowledgeViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
{
    NSMutableArray *lists;
    SQLiteDB *database;
    
}

@end
