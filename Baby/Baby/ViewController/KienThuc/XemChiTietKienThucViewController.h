//
//  XemChiTietKienThucViewController.h
//  Baby
//
//  Created by kim ly on 4/18/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface XemChiTietKienThucViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;
@property(strong, nonatomic)NSString *nameFile;
@property(strong, nonatomic)NSString *titleView;

@end
