//
//  KienThucViewController.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Knowledge.h"
#import "SQLiteDB.h"
#import "KienThucCollectionViewCell.h"

@interface KienThucViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    SQLiteDB *database;
    // khoi tao 3 array chua thong tin kient thuc ve tam ly, suc khoe, dinh duong
    NSMutableArray *listPsychologys;    //tam ly
    NSMutableArray *listHealths;    // suc khoe
    NSMutableArray *listNutrients;  //dinh duong
    
    // trang thai segment dang lua chon
    int statusInfo;
}
- (IBAction)mySegment:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tableview;



@end
