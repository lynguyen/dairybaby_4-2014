
//
//  KienThucViewController.m
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "KienThucViewController.h"
#import "KienThucTableViewCell.h"
#import "XemChiTietKienThucViewController.h"


@interface KienThucViewController ()

@end

@implementation KienThucViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"SCREEN KIEN THUC NUOI DAY TRE");
    database = [[SQLiteDB alloc] init];
    
	// Do any additional setup after loading the view.
    listHealths = [[NSMutableArray alloc] init];
    listNutrients = [[NSMutableArray alloc] init];
    listPsychologys = [[NSMutableArray alloc] init];
    
    // lay thong tin tu sqlite ve
    listPsychologys = [database GetAllKnowledgeWithType:1];
    //
    
    
    
    listHealths = [database GetAllKnowledgeWithType:2];
    for (int i = 0; i< [listHealths count]; i++) {
        Knowledge *jj = [[Knowledge alloc] init];
        jj = [listHealths objectAtIndex:i];
        NSLog(@"name: %@ - file: %@", jj.name, jj.nameFile);
    }
    listNutrients = [database GetAllKnowledgeWithType:3];
    
    statusInfo = 0;
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nen.png"]];
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundWeb.png"]];
    //self.tableview.backgroundColor = [UIColor clearColor];
    //
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int listCounts = 0;
    
    // kiem tra trang thai segment dang duoc lua chon
    if (statusInfo ==0) {// suc khoe
        listCounts = [listHealths count];
    }
    else if(statusInfo ==1){//dinh duong
        listCounts = [listNutrients count];
    }
    else {//tam ly
        listCounts = [listPsychologys count];
    }
    
    return listCounts;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"kienThucTableViewCell";
    
    
    KienThucTableViewCell *cellKnowledge = [tableView
                             dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    //cellKnowledge.selectionStyle = UITableViewCellStyleDefault;
    cellKnowledge.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (statusInfo == 0) // suc khoe
    {
        Knowledge *objInfo = [[Knowledge alloc] init];
        objInfo = [listHealths objectAtIndex:indexPath.row];
        cellKnowledge.nameShow.text = objInfo.name;
    }
    else if(statusInfo == 1) // sinh duong
    {
        Knowledge *objInfo = [[Knowledge alloc] init];
        objInfo = [listNutrients objectAtIndex:indexPath.row];
        cellKnowledge.nameShow.text  = objInfo.name;
    }
    else  // tam ly
    {
        Knowledge *objInfo = [[Knowledge alloc] init];
        objInfo = [listPsychologys objectAtIndex:indexPath.row];
        cellKnowledge.nameShow.text  = objInfo.name;
    }
    cellKnowledge.backgroundColor = [UIColor clearColor];
    return cellKnowledge;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    XemChiTietKienThucViewController *showDedai = [self.storyboard instantiateViewControllerWithIdentifier:@"xemChiTietKienThucViewController"];
    
    [self.navigationController pushViewController:showDedai animated:YES];
    
    if (statusInfo == 0) // suc khoe
    {
        showDedai.titleView = @"Sức khoẻ";
        Knowledge *objInfo = [[Knowledge alloc] init];
        objInfo = [listHealths objectAtIndex:indexPath.row];
        showDedai.nameFile = objInfo.nameFile;

    }
    else if(statusInfo == 1) // sinh duong
    {
        showDedai.titleView = @"Dinh dưỡng";
        Knowledge *objInfo = [[Knowledge alloc] init];
        objInfo = [listNutrients objectAtIndex:indexPath.row];
        showDedai.nameFile = objInfo.nameFile;
    }
    else  // tam ly
    {
        showDedai.titleView = @"Tâm lý";
        Knowledge *objInfo = [[Knowledge alloc] init];
        objInfo = [listPsychologys objectAtIndex:indexPath.row];
        showDedai.nameFile = objInfo.nameFile;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (IBAction)mySegment:(id)sender {
    if ([sender selectedSegmentIndex] == 0)
    {
        statusInfo = 0;
    }
    else if ([sender selectedSegmentIndex] ==1)
    {
        statusInfo = 1;
    }
    else
    {
        statusInfo = 2;
    }
    [self.tableview reloadData];
    
}
@end
