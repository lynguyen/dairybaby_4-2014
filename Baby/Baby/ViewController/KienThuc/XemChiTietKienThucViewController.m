//
//  XemChiTietKienThucViewController.m
//  Baby
//
//  Created by kim ly on 4/18/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "XemChiTietKienThucViewController.h"

@interface XemChiTietKienThucViewController ()

@end

@implementation XemChiTietKienThucViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = self.titleView;
    
    NSString *file = [[NSBundle mainBundle]pathForResource:self.nameFile ofType:@"html" inDirectory:nil];
    //    NSString *htmlString = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil];
    [self.myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:file]]];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
