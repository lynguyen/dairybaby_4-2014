//
//  TaoMoiSuKienViewController.h
//  Baby
//
//  Created by kim ly on 3/26/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectMusicTableView.h"
#import "RemindersTimeTableView.h"
#import "SQLiteDB.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

@interface TaoMoiSuKienViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate,
SelectMusicDelegate,RemindersTimeTalbeViewDelegate>
{
    // khai bao database de su dung
    SQLiteDB *database;
    
    // khoi tao mot so gia tri ban dau
    NSInteger typeReminder;   // 2 nhan 0 neu bao theo ngay cu the. nhan 1 neu bao lap lai theo tuan
    NSMutableArray *listReminderfollowWeekly;
    BOOL statusReminder;
    //file music mm.mp3
    NSString *nameFileRingtone;
    NSInteger noticesMinutes;
    
    NSString *addDay;
    NSString *addHour;
}

@property (strong, nonatomic) IBOutlet UITextField *titleEvent;

// picker chon gio dien ra su kien
@property(strong, nonatomic)NSDate *returnHourTime;
@property (strong, nonatomic) IBOutlet UIDatePicker *hourTime;
@property (strong, nonatomic) IBOutlet UIDatePicker *hourTimeEvent;
@property (strong, nonatomic) IBOutlet UIDatePicker *timeEventDatePicker;

@property(strong, nonatomic)NSDate *detailHourTime;

// switch lua chon nhac nho
- (IBAction)statusAlarm:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *viewGroundContainEvent;

// button lua chon nhac nho theo ngay cu the
- (IBAction)alarmWithDate:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *alarmWithDateOutlet;


//button lua chon nhac nho theo tuan, lap lai
- (IBAction)alarmRepeatWeekly:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *alarmRepeatWeeklyOutlet;


//button lua chon gio nhac nho
- (IBAction)noticeTime:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *noticeTimeOutlet;


// button lua chon nhac chuong
- (IBAction)selectRingTone:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *selectRingToneOutlet;

// date picker lua chon ngay nhac nho cu the
@property(strong, nonatomic)NSDate *returnDetailTime;
@property (strong, nonatomic) IBOutlet UIDatePicker *detaiTime;
// dateTime: lay ra ngay gi[
@property(strong, nonatomic)NSDate *dateTime;

// collectionview. lua chon cac ngay nhac nho trong 1 tuan
@property (strong, nonatomic) IBOutlet UICollectionView *myColletionView;
@property(nonatomic, strong)NSMutableArray *listNameDay;

// UIBarButtonItem: luu or huy 
@property(strong, nonatomic)UIBarButtonItem *saveEvent;
@property(strong, nonatomic)UIBarButtonItem *cancelEvent;
@property (strong, nonatomic)EKEventStore *eventStore;

// popover list musics
@property(strong, nonatomic)SelectMusicTableView *selectMusicTable;
@property(strong, nonatomic)UIPopoverController *popoverSelectMusicTable;

// popover list reminders times
@property(strong, nonatomic)RemindersTimeTableView *remindersTimeTable;
@property(strong, nonatomic)UIPopoverController *popoverRemindersTimeTable;


@end
