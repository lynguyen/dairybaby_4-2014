//
//  LapLichViewController.m
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "LapLichViewController.h"
#import "LapLichTableViewCell.h"
@interface LapLichViewController ()

@end

@implementation LapLichViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"SCREEN : LAP LICH");
	// Do any additional setup after loading the view.
    
    database = [[SQLiteDB alloc] init];
    [database openData];
    listComingEvent = [[NSMutableArray alloc] init];
    //
    listComingEvent = [database getAllComingEvents];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - tableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listComingEvent count];
}
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath;
{
    
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifierCellSchedule = @"lapLichTableViewCell";
    
    LapLichTableViewCell *cellSchedule = nil;
    cellSchedule = [tableView dequeueReusableCellWithIdentifier:identifierCellSchedule forIndexPath:indexPath];
//    cellSchedule set data
    ComingEvent *comingEventObj = [[ComingEvent alloc] init];
      
    
        comingEventObj = [listComingEvent objectAtIndex:indexPath.row];
        cellSchedule.titleComingEvent.text = comingEventObj.title;
        
        NSLog(@"&&&& ==> xem lai 1 lan nua: ");
        NSDate *eventDate1 = comingEventObj.hourTime;
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        dateFormatter1.dateFormat = @"dd/MM/yyyy";
        NSString  *theDate1 = [dateFormatter1 stringFromDate:eventDate1];
        NSLog(@"theDate %@", theDate1);
        cellSchedule.timeComingEvent.text = theDate1;
        
        if ([comingEventObj.statusReminder boolValue]) {
            [cellSchedule.statusReminder setOn:YES];
        }
        else{
            [cellSchedule.statusReminder setOn:NO];
        }
    if (comingEventObj.alarm == 0) {
        cellSchedule.statusReminder.hidden = NO;
    }
    else{
        cellSchedule.statusReminder.hidden = YES;
        [cellSchedule.statusReminder setTag:indexPath.row];
        [cellSchedule.statusReminder addTarget:self action:@selector(changeStatusReminder:) forControlEvents:UIControlEventValueChanged];
    }
    
    return cellSchedule;
    
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    ComingEvent *comingEventTemp = [[ComingEvent alloc] init];
    comingEventTemp = [listComingEvent objectAtIndex:indexPath.row];
    [database delComingEventWithID:comingEventTemp.iD];
    [listComingEvent removeObjectAtIndex:indexPath.row];
    [self.myTableView reloadData];
}

#pragma mark - Change status reminder
- (void)changeStatusReminder:(id)sender;
{
    NSLog(@"==> call method changeStatusReminder:");
    NSLog(@"cell goi thay doi trang thai la on status: %d, tag: %d", [sender isOn], [sender tag]);
    // 1 bao nhac nho - 0 ko bao nhac nho
    // tag: ban ghi duoc nhac nho
    
    ComingEvent *comingEventObj = [[ComingEvent alloc] init];
    comingEventObj = [listComingEvent objectAtIndex:[sender tag]];
    if (sender == 0) {
        // tu yes ->no
        [database updateStatusReminderWithId:comingEventObj.iD status:1];
    }
    else{
        // tu no ->
        [database updateStatusReminderWithId:comingEventObj.iD status:0];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"==> call method viewWillAppear:");
    [database openData];
    listComingEvent = nil;
    listComingEvent = [database getAllComingEvents];
    NSLog(@"so luong obj nhac nho la: %d", [listComingEvent count]);
    [self.myTableView reloadData];
}

@end
