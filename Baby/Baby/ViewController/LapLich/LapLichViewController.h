//
//  LapLichViewController.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLiteDB.h"
#import "ComingEvent.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

@interface LapLichViewController : UIViewController <UITableViewDataSource, UITableViewDataSource>
{
    SQLiteDB *database;
    NSMutableArray *listComingEvent;
}
@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@end
