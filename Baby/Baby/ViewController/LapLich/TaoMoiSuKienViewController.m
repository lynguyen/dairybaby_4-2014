//
//  TaoMoiSuKienViewController.m
//  Baby
//
//  Created by kim ly on 3/26/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "TaoMoiSuKienViewController.h"
#import "TaoMoiSuKienCollectionViewCell.h"

@interface TaoMoiSuKienViewController ()

@end

@implementation TaoMoiSuKienViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"SCREEN: TAO MOI SU KIEN SAP TOI");
    
    database = [[SQLiteDB alloc] init];
    [database openData];
    
    //
    typeReminder = 0;
    statusReminder = NO;
    listReminderfollowWeekly = [[NSMutableArray alloc] init];
    for (int i = 0; i < 7; i++) {
        
        BOOL statusReminderDay = NO;
        [listReminderfollowWeekly addObject:[NSNumber numberWithBool:statusReminderDay]];
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:232.0/255 green:247.0/255 blue:251.0/255 alpha:1.0];
    
    self.title = @"Tạo mới sự kiện";
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    
    self.saveEvent = [[UIBarButtonItem alloc] initWithTitle:@"Lưu" style:UIBarButtonItemStyleBordered target:self action:@selector(saveEventNew)];
    
    self.navigationItem.leftBarButtonItem = self.saveEvent;
    
    self.cancelEvent = [[UIBarButtonItem alloc] initWithTitle:@"Huỷ" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelEventNew)];
    self.navigationItem.rightBarButtonItem = self.cancelEvent;
    
    self.listNameDay = [[NSMutableArray alloc] initWithObjects: @"CN",@"T.Hai", @"T.Ba", @"T.Tư", @"T.Năm", @"T.Sáu", @"T.Bảy", nil];
    
  
    
    // radio button cho lua chon kieu nhac nho
    [self.alarmWithDateOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    [self.alarmRepeatWeeklyOutlet setImage:[UIImage imageNamed:@"noCheck.png"] forState:UIControlStateNormal];
    
    [self.alarmWithDateOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateHighlighted];
    [self.alarmWithDateOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    [self.alarmRepeatWeeklyOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateHighlighted];
    [self.alarmRepeatWeeklyOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nen.png"]];
    self.view.backgroundColor  = [UIColor colorWithRed:224.0/255 green:238.0/255 blue:224.0/255 alpha:1.0];
    
    [self.detaiTime addTarget:self action:@selector(changeDateTime) forControlEvents:UIControlEventValueChanged];
    NSDate *dateCurrent = [NSDate date];
    [self.detaiTime setMinimumDate:dateCurrent];

    //[self.hourTimeEvent setDatePickerMode:UIDatePickerModeCountDownTimer];
    
    //self.hourTimeEvent.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_VN"];
    
//    [self.hourTimeEvent addTarget:self action:@selector(changeHourTime) forControlEvents:UIControlEventValueChanged];
    [self.timeEventDatePicker addTarget:self action:@selector(changeHourTime) forControlEvents:UIControlEventValueChanged];

    self.viewGroundContainEvent.layer.cornerRadius = 8;
    self.viewGroundContainEvent.layer.masksToBounds = YES;
    self.viewGroundContainEvent.layer.borderColor = [[UIColor whiteColor] CGColor];
    //self.viewGroundContainEvent.layer.borderWidth = 1;
    
    
    // tao duong link ngan cach trong giao dien
    UIView *viewLineIntoPicker2 = [[UIView alloc] initWithFrame:CGRectMake(30, 140, 590, 1)];
    viewLineIntoPicker2.backgroundColor = [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
    [self.viewGroundContainEvent addSubview:viewLineIntoPicker2];
    
    UIView *viewLineIntoPicker3 = [[UIView alloc] initWithFrame:CGRectMake(30, 200, 590, 1)];
    viewLineIntoPicker3.backgroundColor = [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
    [self.viewGroundContainEvent addSubview:viewLineIntoPicker3];
    
}
#pragma mark - update return time when change UIDatePicker

// xuat time khi thay doi thoi gian trong UIDatePicker
- (NSDate *)changeDateTime;
{
    NSLog(@"==> call method changeDateTime");
    [self toucheHidekeyBoard];
    self.dateTime = self.detaiTime.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-DD-YYYY HH:MM"]; //30-12-2010 12:10
    //cccc, MMMM d, hh:mm aa
    NSString *prettyVersion = [dateFormat stringFromDate:self.dateTime];
    NSLog(@"%@", prettyVersion);
    addDay = prettyVersion;
    NSDate *timeCreateDairy = [dateFormat dateFromString:prettyVersion];
    NSLog(@"Ngay lua chon bao chi tiet %@", timeCreateDairy);
    return timeCreateDairy;
}

- (NSDate *)changeHourTime;
{
    NSLog(@"==> call method changeHourTime");
    [self toucheHidekeyBoard];
//    self.detailHourTime = self.hourTimeEvent.date;
    self.detailHourTime = self.timeEventDatePicker.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"DD-MM-YYYY HH:MM"];   //30-12-2010 12:10
    NSString *prettyVersion = [dateFormat stringFromDate:self.detailHourTime];
    NSLog(@"%@", prettyVersion);
    addHour = prettyVersion;
    NSDate *timeCreateDairy = [dateFormat dateFromString:prettyVersion];
    NSLog(@" gio dien ra kien %@", timeCreateDairy);
    return timeCreateDairy;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - set event click UIBarButton save or cancel
- (void)saveEventNew{
    //
    NSString *titleEvent = self.titleEvent.text;
    //
    NSInteger stausReminderInt;
    if (statusReminder) {
        stausReminderInt = 0;
    }
    else {
        stausReminderInt = 1;
    }
    
    // typeReminder 0-ngay cu the, 1-lap lai theo tuan.
    
    //
    int sunday;
    NSNumber *sundayBool = [listReminderfollowWeekly objectAtIndex:0];
    if ([sundayBool boolValue]) {
        sunday = 0;
    }
    else
        sunday = 1;
    //
    int monday;
    NSNumber *mondayBool = [listReminderfollowWeekly objectAtIndex:0];
    if ([mondayBool boolValue]) {
        monday = 0;
    }
    else
        monday = 1;
    //
    int tuesday;
    NSNumber *tuesdayBool = [listReminderfollowWeekly objectAtIndex:0];
    if ([tuesdayBool boolValue]) {
        tuesday = 0;
    }
    else
        tuesday = 1;
    //
    int wednesday;
    NSNumber *wednesdayBool = [listReminderfollowWeekly objectAtIndex:0];
    if ([wednesdayBool boolValue]) {
        wednesday = 0;
    }
    else
        wednesday = 1;
    //
    int thursday;
    NSNumber *thursdayBool = [listReminderfollowWeekly objectAtIndex:0];
    if ([thursdayBool boolValue]) {
        thursday = 0;
    }
    else
        thursday = 1;
    //
    int friday;
    NSNumber *fridayBool = [listReminderfollowWeekly objectAtIndex:0];
    if ([fridayBool boolValue]) {
        friday = 0;
    }
    else
        friday = 1;
    //
    int saturday;
    NSNumber *saturdayBool = [listReminderfollowWeekly objectAtIndex:0];
    if ([saturdayBool boolValue]) {
        saturday = 0;
    }
    else
        saturday = 1;
    //
//    nameFileRingtone .mp3
    nameFileRingtone = @"EchNgoiDayGieng";
    //  noticesMinutes - so phut bao truoc
    
    
    
    NSDate *changeHourTime = [self changeHourTime];
    NSInteger changeHourTimeInt = [changeHourTime timeIntervalSince1970];
    NSLog(@"???? Chen bang Su kien");
    NSLog(@"Cac thong tin su kien chen vao la: title:%@, time: %d, ten nhac: %@",titleEvent, changeHourTimeInt, nameFileRingtone);
    
    [database insertComingEvent:titleEvent hourTime:changeHourTimeInt statusReminder:stausReminderInt typeReminder:typeReminder sunday:sunday monday:monday tuesday:tuesday wednesday:wednesday thursday:thursday friday:friday saturday:saturday ringtone:nameFileRingtone];
    
    // tao mootj thong bao neu co
    
    if (stausReminderInt == 0) {
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSDate *pickerDate = [self.timeEventDatePicker date];
        NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                                       fromDate:pickerDate];
        
        NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
                                                       fromDate:pickerDate];
        NSDateComponents *dateComps = [[NSDateComponents alloc] init];
        [dateComps setDay:[dateComponents day]];
        [dateComps setMonth:[dateComponents month]];
        [dateComps setYear:[dateComponents year]];
        [dateComps setHour:[timeComponents hour]];
        // Notification will fire in one minute
        [dateComps setMinute:[timeComponents minute]];
        [dateComps setSecond:[timeComponents second]];
        NSDate *itemDate = [calendar dateFromComponents:dateComps];
        // kho tao 1 localNotification
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        
        // set time tuc hien notification
        localNotification.fireDate = itemDate;
        
        // set noi dung thong bao hien thi
        localNotification.alertBody = titleEvent;
        
        // set tieu de thong bao
        localNotification.alertAction = @"View";
        
        // set sound notification
        localNotification.soundName = @"ring1.caf";
        
        
        // set mui gio thuc hien
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        
        // set thanh phan
        localNotification.applicationIconBadgeNumber = 0;
        
        // yeu cau thaong bao duoc send
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
   
    [self dismissViewControllerAnimated:YES completion:nil];
    [database closeData];
}

-(void)createReminder
{
    NSLog(@"==> cal method createReminder:");
    // khoi tao 1 nhac nho
    // tao 1 doi tuong EKReminder
    EKReminder *reminder = [EKReminder
                            reminderWithEventStore:self.eventStore];
    // ten su kien can nhac nho = gia tri nhap vao trong text
    reminder.title = self.titleEvent.text;
    
    // set calendar cho su kien
    reminder.calendar = [_eventStore defaultCalendarForNewReminders];
    
    
    
    // lay ve gia tri can nhac nho
    NSString *timeObj = [NSString stringWithFormat:@"%@-%@",addDay, addHour]; // ghep time cua gioPhut-ngay
    NSLog(@"thoi gian gop lai la: %@",timeObj);
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy-hh-mm-aa"];                               //cccc, MMMM d, hh:mm aa
    
    NSDate *timeCreateDairy = [dateFormat dateFromString:timeObj];
    NSLog(@" gio dien ra kien %@", timeCreateDairy);
    
    //thoi gian bao thuc = thoi gian trong picker
    EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:timeCreateDairy];
    
    
    // add bao thuc cho cai nhac nho nay
    [reminder addAlarm:alarm];
    
    // tra ve loi neu co
    NSError *error = nil;
    
    [_eventStore saveReminder:reminder commit:YES error:&error];
    
    if (error)
        NSLog(@"error = %@", error);
    
}

- (void)cancelEventNew{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [database closeData];
}

#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 7;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifierNewEventCell = @"taoMoiSuKienCollectionViewCell";
    TaoMoiSuKienCollectionViewCell *cellNewEvent = nil;
    cellNewEvent = [collectionView dequeueReusableCellWithReuseIdentifier:identifierNewEventCell forIndexPath:indexPath];
    cellNewEvent.nameDay.text = [self.listNameDay objectAtIndex:indexPath.row];
    
    // thay doi mau nen trang thai chon hay khong chon
    NSNumber *checkStatus = [listReminderfollowWeekly objectAtIndex:indexPath.row];
    if ([checkStatus boolValue]) {
        cellNewEvent.backgroundColor =  [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
        cellNewEvent.imageSelectDay.image = [UIImage imageNamed:@"icon_checkRepeat.png"];
    }
    else
    {
        cellNewEvent.backgroundColor = [UIColor colorWithRed:224.0/255 green:238.0/255 blue:224.0/255 alpha:1.0];
        cellNewEvent.imageSelectDay.image = nil;
    }
    
    return cellNewEvent;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"==> method select row in collectionview:");
    
    NSNumber *checkStatus = [listReminderfollowWeekly objectAtIndex:indexPath.row];
    NSLog(@"Row duoc click la: %d trang thai tra ve %@",indexPath.row, ([checkStatus boolValue] ?@"YES":@"NO"));
    // kiem tra thay doi trang thai
    if ([checkStatus boolValue]) {
        BOOL selectStatus = NO;
        [listReminderfollowWeekly removeObjectAtIndex:indexPath.row];
        [listReminderfollowWeekly insertObject:[NSNumber numberWithBool:selectStatus] atIndex:indexPath.row];
        
        // check status
        NSNumber *newStatus = [listReminderfollowWeekly objectAtIndex:indexPath.row];
        
        NSLog(@"thay do lai trang thai: %d trang thai tra ve %@",indexPath.row, ([newStatus boolValue] ?@"YES":@"NO"));
     
    }
    else{
        BOOL selectStatus = YES;
        [listReminderfollowWeekly removeObjectAtIndex:indexPath.row];
        [listReminderfollowWeekly insertObject:[NSNumber numberWithBool:selectStatus] atIndex:indexPath.row];
        // check status
        NSNumber *newStatus = [listReminderfollowWeekly objectAtIndex:indexPath.row];
        
        NSLog(@"thay do lai trang thai: %d trang thai tra ve %@",indexPath.row, ([newStatus boolValue] ?@"YES":@"NO"));
        
    }
    NSLog(@" goi reload data");
    [self.myColletionView reloadData];
}

#pragma mark - UISwith change status
// switch lua chon nhac nho
- (IBAction)statusAlarm:(id)sender {
    
    if ([sender isOn]) {
        NSLog(@"trang thai: nhac nho");
        statusReminder = YES;
    }
    else{
        NSLog(@"Trang thai khong nhac nho");
        statusReminder = NO;
    }
}

#pragma mark - group radio button: select type reminder
- (IBAction)alarmWithDate:(id)sender {
    [self.alarmWithDateOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    [self.alarmWithDateOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateHighlighted];
    [self.alarmWithDateOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    [self.alarmRepeatWeeklyOutlet setImage:[UIImage imageNamed:@"noCheck.png"] forState:UIControlStateNormal];
    typeReminder = 0;
}

- (IBAction)alarmRepeatWeekly:(id)sender;
{
    [self.alarmWithDateOutlet setImage:[UIImage imageNamed:@"noCheck.png"] forState:UIControlStateNormal];
    
    [self.alarmRepeatWeeklyOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    [self.alarmRepeatWeeklyOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateHighlighted];
    [self.alarmRepeatWeeklyOutlet setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    typeReminder = 1;
}

#pragma mark - select notice time -> show popover

- (IBAction)noticeTime:(id)sender;
{
    if (self.remindersTimeTable == nil) {
        //Create the ColorPickerViewController.
        self.remindersTimeTable = [[RemindersTimeTableView alloc] initWithStyle:UITableViewStylePlain];
        
        //Set this VC as the delegate.
        self.remindersTimeTable.delegate = self;
    }
    
    if (self.popoverRemindersTimeTable == nil) {
        //The color picker popover is not showing. Show it.
        self.popoverRemindersTimeTable = [[UIPopoverController alloc] initWithContentViewController:self.remindersTimeTable];
        //[_colorPickerPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *) sender  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        [self.popoverRemindersTimeTable presentPopoverFromRect:[self noticeTimeOutlet].frame  inView:self.noticeTimeOutlet.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        //        [popoverChoiceBabyController presentPopoverFromRect:[self selectBabyOutlet].frame inView:self.selectBabyOutlet.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        //The color picker popover is showing. Hide it.
        [self.popoverRemindersTimeTable dismissPopoverAnimated:YES];
        self.popoverRemindersTimeTable = nil;
    }

}

#pragma mark - method of RemindersTimeTableView delegete
- (void)selectedRemindersTime:(NSInteger)remindersTime;
{
    noticesMinutes = remindersTime;
}
- (void)selectedNameRemindersTime:(NSString *)name;
{
    [self.noticeTimeOutlet setTitle:name forState:UIControlStateNormal];
    if (self.popoverRemindersTimeTable) {
        [self.popoverRemindersTimeTable dismissPopoverAnimated:YES];
        self.popoverRemindersTimeTable = nil;
    }

}

#pragma mark - select ringtone -> show popover
- (IBAction)selectRingTone:(id)sender {
    if (self.selectMusicTable == nil) {
        //Create the ColorPickerViewController.
        self.selectMusicTable = [[SelectMusicTableView alloc] initWithStyle:UITableViewStylePlain];
        
        //Set this VC as the delegate.
        self.selectMusicTable.delegate = self;
    }
    
    if (self.popoverSelectMusicTable == nil) {
        //The color picker popover is not showing. Show it.
        self.popoverSelectMusicTable = [[UIPopoverController alloc] initWithContentViewController:self.selectMusicTable];
        //[_colorPickerPopover presentPopoverFromBarButtonItem:(UIBarButtonItem *) sender  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        [self.popoverSelectMusicTable presentPopoverFromRect:[self selectRingToneOutlet].frame  inView:self.selectRingToneOutlet.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        //        [popoverChoiceBabyController presentPopoverFromRect:[self selectBabyOutlet].frame inView:self.selectBabyOutlet.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        //The color picker popover is showing. Hide it.
        [self.popoverSelectMusicTable dismissPopoverAnimated:YES];
        self.popoverSelectMusicTable = nil;
    }

}




#pragma mark - method an keyboard khi click vung trong

//- (void)toucheOnView:(UIGestureRecognizer *)gesture;
//{
//    if (statusKeyboard) {
//        [self.titleEvent resignFirstResponder];
//    }
//}

- (void)toucheHidekeyBoard;
{
    
    [self.titleEvent resignFirstResponder];
}

#pragma mark -  delegate popover select ringtone
- (void)selectedFileMusic:(NSString *)fileMusic;
{
    NSLog(@"==> call method selectedFileMucsic delegate:");
    nameFileRingtone = fileMusic;
    NSLog(@"ten file nhac da chon la: %@", nameFileRingtone);
}
- (void)selectNameFileMusic:(NSString *)nameFile;
{
    // hien thi ten ban nhac da lua chon
    [self.selectRingToneOutlet setTitle:nameFile forState:UIControlStateNormal];

    // huy popover
    if (self.popoverSelectMusicTable) {
        [self.popoverSelectMusicTable dismissPopoverAnimated:YES];
        self.popoverSelectMusicTable = nil;
    }

}

@end
