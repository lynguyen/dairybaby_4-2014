//
//  QuanLyViewController.m
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "QuanLyViewController.h"
#import "ThayDoiThongTinVeBeViewController.h"
#import "CapNhatThongTinPhatTrienViewController.h"
#import "ThongTinPhatTrienTableViewCell.h"
#import "StandardGrow.h"

@interface QuanLyViewController ()

@end

@implementation QuanLyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad

{
    [super viewDidLoad];
    
    indexBabySelect = -1;

    listNameBaby = [[NSMutableArray alloc] init];
    listObjecBaby = [[NSMutableArray alloc] init];
    listInfoGrowBaby = [[NSMutableArray alloc] init];
    
    database = [[SQLiteDB alloc] init];
    [database openData];
    
    
    listObjecBaby = [database getAllBaby];
    for (int countDown = 0; countDown < [listObjecBaby count]; countDown ++) {
        Baby *babyTemp = [[Baby alloc] init];
        babyTemp = [listObjecBaby objectAtIndex:countDown];
        if ([babyTemp.name isEqualToString:@"Mặc định"]) {
            [listObjecBaby removeObjectAtIndex:countDown];
        }
    }
         
    for (int i = 0; i < [listObjecBaby count]; i++) {
        Baby *babyTempObj = [[Baby alloc] init];
        babyTempObj = [listObjecBaby objectAtIndex:i];
        [listNameBaby addObject:babyTempObj.name];
    }
    
    NSLog(@"so luong be yeu hien tai la: %d", [listNameBaby count]);
	
    // Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:243.0f/255 green:242.0f/255 blue:238.0f/255 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nen.png"]];
    
    self.toolbarGrow.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    
    // set back ground cho fundtion menu
    self.viewFunction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"viewFuntion.png"]];
    // bo goc cho view info, shadow
    self.avataInfo.layer.masksToBounds = YES;
    self.avataInfo.layer.cornerRadius = 20;
    //self.avataInfo.layer.shadowRadius = 5;
    //self.avataInfo.layer.shadowOpacity = 0.5;
    //self.avataInfo.layer.shadowOffset = CGSizeMake(-15,20);
    
    // setmac dinh cho view hien thi dau tien;
    self.viewBabyGrow.hidden = YES;
    self.viewBabyInfo.hidden = NO;
    self.toolbarGrow.hidden = YES;
    self.toolbarInfo.hidden = NO;
    
    // add collor for view info
    self.viewBackGroundShowInfoBaby.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"view_groundInfoBaby.png"]];
    
    // add line in in to view show info detai about a baby
    UIView *viewLineIntoPicker = [[UIView alloc] initWithFrame:CGRectMake(35, 63, 570, 1)];
    viewLineIntoPicker.backgroundColor = [UIColor blueColor];
    [self.viewBackGroundShowInfoBaby addSubview:viewLineIntoPicker];
    
    UIView *viewLineIntoPicker1 = [[UIView alloc] initWithFrame:CGRectMake(35, 100, 570, 1)];
    viewLineIntoPicker1.backgroundColor = [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
    [self.viewBackGroundShowInfoBaby addSubview:viewLineIntoPicker1];
    
    UIView *viewLineIntoPicker2 = [[UIView alloc] initWithFrame:CGRectMake(35, 133, 570, 1)];
    viewLineIntoPicker2.backgroundColor = [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
    [self.viewBackGroundShowInfoBaby addSubview:viewLineIntoPicker2];
    
   
    //self.myTable.backgroundColor = [UIColor clearColor];
    
    // cap nhat thong tin lan dau tien run profile
    if ([listObjecBaby count] != 0) {
        indexBabySelect  = 0;
        Baby *babyObj = [[Baby alloc] init];
        babyObj = [listObjecBaby objectAtIndex:0];
        self.nameInfo.text = babyObj.name;
        
        self.likeOfBaby.text = babyObj.like;
        if (babyObj.gender == 0) {
            self.genderInfo.text = @"Nam";
        }
        else{
            self.genderInfo.text = @"Nu";
        }
        NSDate *eventDate = babyObj.birthday;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        dateFormatter.dateFormat = @"dd-MM-yyyy";
        NSString  *theDate = [dateFormatter stringFromDate:eventDate];
        NSLog(@"theDate %@", theDate);
        self.birthInfo.text = theDate;
        self.avataInfo.image = [self loadImage:babyObj.avata];
        
         listInfoGrowBaby = [database getInfoBabyGrowWithIDBaby:babyObj.iD];
    }

    self.listBabyOutlet.layer.masksToBounds = YES;
    self.listBabyOutlet.layer.cornerRadius = 4;

//    self.newBabyInfoOutlet.layer.masksToBounds = YES;
//    self.newBabyInfoOutlet.layer.cornerRadius = 4;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - @ bat su kien khi click bao cac button bar trong toolbar

- (IBAction)changeInfo:(id)sender {
    ThayDoiThongTinVeBeViewController *changeInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"thayDoiThongTinVeBeViewController"];
    UINavigationController *naviChangeInfo = [[UINavigationController alloc] initWithRootViewController:changeInfo];
    [self presentViewController:naviChangeInfo animated:YES completion:nil];
    changeInfo.stateWorking = 1;
    
    Baby *babyScreen =[[Baby alloc] init];
    babyScreen = [listObjecBaby objectAtIndex:indexBabySelect];
    changeInfo.getBabyInfo = babyScreen;
    // indexbabyselect
    
}

- (IBAction)newBabyInfo:(id)sender {
    
    ThayDoiThongTinVeBeViewController *newBaby = [self.storyboard instantiateViewControllerWithIdentifier:@"thayDoiThongTinVeBeViewController"];
    UINavigationController *naviNewBaby = [[UINavigationController alloc] initWithRootViewController:newBaby];
    naviNewBaby.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:naviNewBaby animated:YES completion:nil];
    newBaby.stateWorking = 0; // tao moi khong do thong tin i vao view ThayDoiThongTinBeYeu
    newBaby.indexBabyInfo = -1;
//    newBaby.getBabyInfo
    //getBabyInfo
    [database closeData];
}


- (IBAction)updateInfoGrow:(id)sender {
    //    naviUpdateInfoBaby.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //    naviUpdateInfoBaby.modalTransitionStyle = UIModalPresentationCurrentContext;
    //    naviUpdateInfoBaby.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    if ([listObjecBaby count] == 0) {
        UIAlertView *alertUpdateInfoGrow = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Không có bé yêu nào!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertUpdateInfoGrow show];
    }
    else{
        CapNhatThongTinPhatTrienViewController *updateInfoBaby = [self.storyboard instantiateViewControllerWithIdentifier:@"capNhatThongTinPhatTrienViewController"];
        
        UINavigationController *naviUpdateInfoBaby = [[UINavigationController alloc] initWithRootViewController:updateInfoBaby];
        [self presentViewController:naviUpdateInfoBaby animated:YES completion:nil];
        
        Baby *babyScreen =[[Baby alloc] init];
        babyScreen = [listObjecBaby objectAtIndex:indexBabySelect];
        updateInfoBaby.iDBaby = babyScreen.iD;
    }

}

- (IBAction)commentInfoGrow:(id)sender {
    if ([listObjecBaby count] == 0) {
        UIAlertView *alertUpdateInfoGrow = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Không có bé yêu nào!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertUpdateInfoGrow show];
    }
    else
    {
        if ([listInfoGrowBaby count] == 0) {
            UIAlertView *alertUpdateInfoGrow = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Không có thông tin phát triển, bạn cần phải cập nhât!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertUpdateInfoGrow show];
        }
        else if(([listInfoGrowBaby count] == 1) && (noteMonth==10)){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@" " message:@"Chúc mừng bạn đã có thêm một thành viên mới trong gia đình" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        else{
            int levelHeight; // 0 xau - 1 tot
            int levelWeight; // 0 suy dinh duong - 1 gan suy dinh duong - 2 bt - 3 gan map - 4 beo
            // kiem tra be trai hay be gai
            Baby *babySelectComment = [[Baby alloc] init];
            babySelectComment = [listObjecBaby objectAtIndex:indexBabySelect];
            // neu la be trai
            if (babySelectComment.gender == 0) {
                //listInfoGrowBaby
                BabyGrow *babyGrowObj = [[BabyGrow alloc] init];
                babyGrowObj = [listInfoGrowBaby objectAtIndex:0];
                NSMutableArray *listStandardBoy = [[NSMutableArray alloc] init];
                listStandardBoy = [database getAllInfoStandartBoyWithMonth:babyGrowObj.month];
                
                StandardGrow *standard= [listStandardBoy objectAtIndex:0];
                
                if ([babyGrowObj.height floatValue] < [standard.height floatValue]) {
                    levelHeight = 0;
                }
                else levelHeight = 1;
                
                if ([babyGrowObj.weight floatValue] < [standard.levelLows floatValue]) {
                    levelWeight = 0;
                }
                else if ([babyGrowObj.weight floatValue] >= [standard.levelLow1 floatValue] && [babyGrowObj.weight floatValue] <= [standard.levelLow2 floatValue]){
                    levelWeight = 1;
                }
                else if ([babyGrowObj.weight floatValue] >= [standard.levelStandard1 floatValue] && [babyGrowObj.weight floatValue] <= [standard.levelStandard2 floatValue]){
                    levelWeight = 2;
                }
                else if ([babyGrowObj.weight floatValue] >= [standard.levelHigh1 floatValue] && [babyGrowObj.weight floatValue] <= [standard.levelHigh2 floatValue]){
                    levelWeight = 3;
                }
                else{
                    levelWeight = 4;
                }
                
                
            }
            // neu la be gai
            else{
                BabyGrow *babyGrowObj = [[BabyGrow alloc] init];
                babyGrowObj = [listInfoGrowBaby objectAtIndex:0];
                NSMutableArray *listStandardBoy = [[NSMutableArray alloc] init];
                listStandardBoy = [database getallINfoStandartGirlWithMonth:babyGrowObj.month];
                
                StandardGrow *standard= [listStandardBoy objectAtIndex:0];
                
                if ([babyGrowObj.height floatValue] < [standard.height floatValue]) {
                    levelHeight = 0;
                }
                else levelHeight = 1;
                
                if ([babyGrowObj.weight floatValue] < [standard.levelLows floatValue]) {
                    levelWeight = 0;
                }
                else if ([babyGrowObj.weight floatValue] >= [standard.levelLow1 floatValue] && [babyGrowObj.weight floatValue] <= [standard.levelLow2 floatValue]){
                    levelWeight = 1;
                }
                else if ([babyGrowObj.weight floatValue] >= [standard.levelStandard1 floatValue] && [babyGrowObj.weight floatValue] <= [standard.levelStandard2 floatValue]){
                    levelWeight = 2;
                }
                else if ([babyGrowObj.weight floatValue] >= [standard.levelHigh1 floatValue] && [babyGrowObj.weight floatValue] <= [standard.levelHigh2 floatValue]){
                    levelWeight = 3;
                }
                else{
                    levelWeight = 4;
                }

                
            }
            
            NSString *comment;
            if (levelHeight == 0 && levelWeight <=1) {
                comment = @"Bé yêu của bạn đang phát triển chậm, bạn cần chú ý quan tâm hơn nữa";
            }
            else if (levelHeight == 1 && levelWeight >=2) {
                comment = @"Bạn không phải lo lắng, bé yêu của bạn phát triển rất tốt";
            }
            else if (levelWeight >=3) {
                comment = @"Bé có biểu hiện thừa cân, bạn cần xem lại chế độ dinh dưỡng cho bé";
            }
            else{
                comment = @"Bé đang phát triển bình thường";
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Đánh giá tình hình phát triển:" message:comment delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
            [alert show];
            
        }
    }
        
}



#pragma mark - @ bat su kien thay doi segment

- (IBAction)mySegment:(id)sender {
    if ([sender selectedSegmentIndex] == 0) {
        NSLog(@"segment index = 0");
        self.viewBabyInfo.hidden = NO;
        self.viewBabyGrow.hidden = YES;
        self.toolbarInfo.hidden = NO;
        self.toolbarGrow.hidden = YES;
    }
    else
    {
        NSLog(@"segment index = 1");
        self.toolbarInfo.hidden = YES;
        self.toolbarGrow.hidden = NO;
        self.viewBabyInfo.hidden = YES;
        self.viewBabyGrow.hidden = NO;
    }
}

- (void)showInfoGrow{
    listInfoGrowBaby = [[NSMutableArray alloc] init];
    BabyGrow *babyShowGrow = [[BabyGrow alloc] init];
    babyShowGrow = [listObjecBaby objectAtIndex:indexBabySelect];
    int iDBaby = babyShowGrow.iD;
    
    listInfoGrowBaby = [database getInfoBabyGrowWithIDBaby:iDBaby];
    [self.myTable reloadData];
    
}
#pragma mark - @ UItableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listInfoGrowBaby count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ThongTinPhatTrienTableViewCell *cellListBaby = nil;
    static NSString *identifierListBaby = @"thongTinPhatTrienTableViewCell";
    cellListBaby = [tableView dequeueReusableCellWithIdentifier:identifierListBaby forIndexPath:indexPath];
//    cellListBaby.hidden = NO;
    BabyGrow *babyGrow = [[BabyGrow alloc] init];
    
    babyGrow = [listInfoGrowBaby objectAtIndex:indexPath.row];
    
    if (babyGrow.month == 0) {
        cellListBaby.mothInfo.text = @"Chào đời";
        noteMonth = 10;
    }
    else{
        cellListBaby.mothInfo.text = [NSString stringWithFormat:@"%d tháng", babyGrow.month];
    }
    cellListBaby.weightInfo.text = [NSString stringWithFormat:@"%.1f kg", [babyGrow.weight floatValue]];
    cellListBaby.heightInfo.text = [NSString stringWithFormat:@"%.1f cm", [babyGrow.height floatValue]];
    
    return cellListBaby;
}



#pragma mark - @ su kien click button lua chon be

- (IBAction)listBaby:(id)sender {
    // su dung 1 UIPicker de hien thi danh sach be yeu.
    
    
    myPickerBaby = [[UIPickerView alloc] init];
    toolBarBary = [[UIToolbar alloc] init];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        UIViewController *popOverContentBaby= [[UIViewController alloc] init];
        
        UIView *popOverView = [[UIView alloc]
                               initWithFrame:CGRectMake(0, 0, [myPickerBaby frame].size.width - 110, [myPickerBaby frame].size.height )];//height+ 44
        
        myPickerBaby = [[UIPickerView alloc]
                        initWithFrame:CGRectMake([popOverView frame].origin.x - 60, [popOverView frame].origin.y, 0 , 0)];//y + 44
        
        toolBarBary = [[UIToolbar alloc]
                       initWithFrame:CGRectMake([popOverView frame].origin.x, [popOverView frame].origin.y , [popOverView frame].size.width+10, 30)];//44
        toolBarBary.translucent = YES;
        toolBarBary.barTintColor = [UIColor clearColor];
        
        toolBarBary.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
        
        
        [myPickerBaby setDataSource:self];
        
        [myPickerBaby setDelegate:self];
        [myPickerBaby setShowsSelectionIndicator:YES];
        [toolBarBary setBarStyle:UIBarStyleDefault];
        
        UIBarButtonItem *fleXibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
       UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"Lựa chọn" style:UIBarButtonItemStyleBordered target:self action:@selector(donePressed)];
//        UIImage *selectBaby = [UIImage imageNamed:@"icon_tickBaby.png"];
//        
//        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithImage:[selectBaby imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(donePressed)];
        
        //UIImage *noSelectBaby = [UIImage imageNamed:@"icon_noTickBaby.png"];
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"Huỷ" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPressed)];
//        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithImage:[noSelectBaby imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(donePressed)];

        
        [toolBarBary setItems:[NSArray arrayWithObjects: done, fleXibleSpace, cancel, nil]];
        
        
        [popOverView addSubview:myPickerBaby];
        
        [popOverView addSubview:toolBarBary];
        [popOverContentBaby setView:popOverView];
        
        popoverChoiceBabyController = [[UIPopoverController alloc] initWithContentViewController:popOverContentBaby];
        popoverChoiceBabyController.popoverContentSize  = CGSizeMake([myPickerBaby frame].size.width - 110, [popOverView frame].size.height);
        
        [popoverChoiceBabyController presentPopoverFromRect:[self listBabyOutlet].frame inView:self.listBabyOutlet.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        
    }
    
}


#pragma mark - @implement UIPicker delegate choice baby

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    return [listNameBaby count];
}

// hien thi danh sach be yeu trong listBaby
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    return [listNameBaby objectAtIndex:row];
}

// bat su kien khi click 1 be trong danh sach
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    //[[self nameBaby]setText:[self.listBaby objectAtIndex:row]];
    indexBaby = row;  // toi co dung no
    NSLog(@"Be duoc lua cho o vi tri index %i",row);
    
}


// su kien khi click button done
- (void)donePressed;
{
    NSLog(@"===> CAL METHOD RELOAD DATA AFFTER SELECTED");
    //[[self nameBaby]setText:[listNameBaby objectAtIndex:indexBaby]];
    
    indexBabySelect = indexBaby;
    NSLog(@" --> select baby choice %d", indexBabySelect);
    Baby *babySelectShow = [[Baby alloc] init];
    babySelectShow = [listObjecBaby objectAtIndex:indexBabySelect];
    self.nameInfo.text = babySelectShow.name;
    
    self.likeOfBaby.text = babySelectShow.like;
    if (babySelectShow.gender == 0) {
            self.genderInfo.text = @"Nam";
    }
    else{
            self.genderInfo.text = @"Nu";
   }
    NSDate *eventDate = babySelectShow.birthday;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
    dateFormatter.dateFormat = @"dd-MM-yyyy";
    NSString  *theDate = [dateFormatter stringFromDate:eventDate];
    NSLog(@"theDate %@", theDate);
    self.birthInfo.text = theDate;
    NSLog(@"Reload ten avata: %@", babySelectShow.avata);
    //load image avata
    self.avataInfo.image = [self loadImage:babySelectShow.avata];

    
    // load thong tin cho tableview
    listInfoGrowBaby = [[NSMutableArray alloc] init];
//    Baby *babyObj = [[Baby alloc] init];
//    babyObj = [listObjecBaby objectAtIndex:indexBabySelect];
    listInfoGrowBaby = [database getInfoBabyGrowWithIDBaby:babySelectShow.iD];
    [self.myTable reloadData];
    
    [popoverChoiceBabyController dismissPopoverAnimated:YES];
    
}

- (void)cancelPressed;
{
    //[[self nameBaby]setText:[listNameBaby objectAtIndex:indexBaby]];
    [popoverChoiceBabyController dismissPopoverAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    if ([popoverChoiceBabyController isPopoverVisible]) {
        [popoverChoiceBabyController dismissPopoverAnimated:YES];
    }
}


#pragma mark - @ UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (buttonIndex == 0) {
//        // code for ok button
//        NSInteger heightBaby = [[alertView textFieldAtIndex:0].text integerValue];
//        NSInteger weightBaby = [[alertView textFieldAtIndex:1].text integerValue];
//        
//        // luu vao data
//        [database insertBabyGrow:1 height:heightBaby weight:weightBaby];
//        
//    }
//    if (buttonIndex == 1) {
//        // code for cancel button
//        // khong lam j
//    }
}
#pragma mark - @ reload data
- (void)viewWillAppear:(BOOL)animated{
    [database openData];
    [super viewWillAppear:YES];
    
    NSLog(@"==> goi viewWillApppear cho data profile");
    listNameBaby = [[NSMutableArray alloc] init];
    listObjecBaby = nil;
    
     //NSLog(@" ==>ten be yeu hien tai la chua add la: %@", listNameBaby);
    [database openData];
    
    listObjecBaby = [database getAllBaby];
    NSLog(@"==> so doi tuong: %d", [listObjecBaby count]);
    for (int countDown = 0; countDown < [listObjecBaby count]; countDown ++) {
        Baby *babyTemp = [[Baby alloc] init];
        babyTemp = [listObjecBaby objectAtIndex:countDown];
        if ([babyTemp.name isEqualToString:@"Mặc định"]) {
            [listObjecBaby removeObjectAtIndex:countDown];
        }
    }
    //[listObjecBaby removeObjectAtIndex:0];
    
    NSLog(@"==> so doi tuong sau khi remove: %d", [listObjecBaby count]);
    
    for (int i = 0; i < [listObjecBaby count]; i++) {
        Baby *babyTempObj = [[Baby alloc] init];
        babyTempObj = [listObjecBaby objectAtIndex:i];
        [listNameBaby addObject:babyTempObj.name];
    }
    // Reload lai thong tin be yeu
    if ([listObjecBaby count] != 0) {
        indexBabySelect = 0; //[listObjecBaby count] - 1
        Baby *babyObj = [[Baby alloc] init];
        babyObj = [listObjecBaby objectAtIndex:indexBabySelect];
        self.nameInfo.text = babyObj.name;
        self.likeOfBaby.text = babyObj.like;
        if (babyObj.gender == 0) {
            self.genderInfo.text = @"Nam";
        }
        else{
            self.genderInfo.text = @"Nu";
        }
        NSDate *eventDate = babyObj.birthday;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        dateFormatter.dateFormat = @"dd-MM-yyyy";
        NSString  *theDate = [dateFormatter stringFromDate:eventDate];
        NSLog(@"theDate %@", theDate);
        self.birthInfo.text = theDate;
        self.avataInfo.image = [self loadImage:babyObj.avata];
        
        listInfoGrowBaby = nil;
        listInfoGrowBaby = [[NSMutableArray alloc] init];
        listInfoGrowBaby = [database getInfoBabyGrowWithIDBaby:babyObj.iD];
        [self.myTable reloadData];
    }
    
}

#pragma mark - work with file system.

// save luu file
- (void)saveImage:(UIImage *)imageTemp nameFile:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(imageTemp); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    NSLog(@"image saved");
    
}

//xoa 1 file image co ten laf fileName

- (void)removeImage:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"image removed");
    
}



// load 1 file tu he thong len, voi doi so la ten nameFile, tra ve i UIImage.

- (UIImage*)loadImage:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}

@end
