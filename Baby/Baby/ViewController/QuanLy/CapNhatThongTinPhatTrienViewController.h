//
//  CapNhatThongTinPhatTrienViewController.h
//  Baby
//
//  Created by kim ly on 4/15/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLiteDB.h"

/*
 screen bo tri 3 textField cho phep nhap cac thong tin: "thang", "chieu cao" , "can nang".
 1 BarButtonItem "quay lai"
 1 button "cap nhat".
 cap nhat se khong thanh cong neu cac truong hop nay xay ra
 - khong nhap du 3 thong tin trong textField
 - nhap trung thang da co
 - nhap thang lon hon do tuoi cua be
 */

@interface CapNhatThongTinPhatTrienViewController : UIViewController{
    SQLiteDB *database;
}
@property (strong, nonatomic) IBOutlet UITextField *mothInfo;
@property (strong, nonatomic) IBOutlet UITextField *heightInfo;
@property (strong, nonatomic) IBOutlet UITextField *weightInfo;
@property (strong, nonatomic) IBOutlet UIView *viewBackgroundContent;

@property(strong, nonatomic)UIBarButtonItem *backProfile;

@property(nonatomic, assign)NSInteger iDBaby;
- (IBAction)saveInfo:(id)sender;

@end
