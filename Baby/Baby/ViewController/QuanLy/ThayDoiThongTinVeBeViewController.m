//
//  ThayDoiThongTinVeBeViewController.m
//  Baby
//
//  Created by kim ly on 4/4/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "ThayDoiThongTinVeBeViewController.h"

@interface ThayDoiThongTinVeBeViewController ()

@property (nonatomic) float originalScrollerOffsetY;

-(void)keyboardWasShown:(NSNotification *)notification;
-(void)keyboardWillHide:(NSNotification *)notification;

@end

@implementation ThayDoiThongTinVeBeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"SCREEN CHANGE INFO BABY:");
    
    database  = [[SQLiteDB alloc] init];
    [database openData];
    babyChangeTemp = [[Baby alloc] init];// khoi tao 1 doi tuong tam.
    
    UIScrollView *scroll = [[UIScrollView alloc] init];
    [scroll addSubview:self.view];
    
    self.title = @"Thông tin bé yêu";
    NSLog(@"thong tin duoc day sang khi click button la: %d", self.stateWorking);
    if (self.stateWorking == 0) {
        
        self.title = @"Tạo mới thông tin bé yêu";
        self.imageFolder = [UIImage imageNamed:@"icon_newBaby.png"];// dung anh mac dinh
        
        // lay indexBaby trong table INFO de dat ten cho file avatababy moi.
        NSMutableArray *getDataInfo = [[NSMutableArray alloc] init];
        getDataInfo = [database getInfo];
        getInfoObject = [getDataInfo objectAtIndex:0];
        indexBaby = getInfoObject.indexBaby;
    }
    else{
        self.title = @"Thay đổi thông tin bé yêu";
        
        // lay tat ca thong tin cua baby can thay doi
        self.nameBaby.text = self.getBabyInfo.name;
        self.like.text = self.getBabyInfo.like;
        [self.birthDay setDate:self.getBabyInfo.birthday];
        
        if (self.getBabyInfo.gender==0) {
            [self.genderOutlet setSelectedSegmentIndex:0];
        }
        else{
            [self.genderOutlet setSelectedSegmentIndex:1];
        }
        // load anh cua baby ra
        self.imageBaby.image = [self loadImage:self.getBabyInfo.avata];
        self.imageFolder = [self loadImage:self.getBabyInfo.avata];
        
    }
    
    
    
    
    
    
    
    
    
    
    
    //self.view.backgroundColor = [UIColor whiteColor];
      self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundView.png"]];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    
    
    // khoi tao 2 button bar de bat su kien luu or huy thay doi thong tin
    
    self.saveInfo = [[UIBarButtonItem alloc] initWithTitle:@"Lưu" style:UIBarButtonItemStyleBordered target:self action:@selector(saveInfoBaby)];
    self.cancelInfo = [[UIBarButtonItem alloc] initWithTitle:@"Huỷ" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelInfoBaby)];
    self.navigationItem.rightBarButtonItem = self.cancelInfo;
    self.navigationItem.leftBarButtonItem = self.saveInfo;
    
    // ve 1 so duong link ngan cach cac phan thong tin
    
    
    UIView *viewLineIntoAvata = [[UIView alloc] initWithFrame:CGRectMake(35, 365, 705, 1)];
    viewLineIntoAvata.backgroundColor = [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
    [self.viewMain addSubview:viewLineIntoAvata];
    
    UIView *viewLineIntoPicker = [[UIView alloc] initWithFrame:CGRectMake(35, 630, 705, 1)];
    viewLineIntoPicker.backgroundColor = [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
    [self.viewMain addSubview:viewLineIntoPicker];
    
    UIView *viewLineIntoSwitch = [[UIView alloc] initWithFrame:CGRectMake(35, 690, 705, 1)];
    viewLineIntoSwitch.backgroundColor = [UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0];
    [self.viewMain addSubview:viewLineIntoSwitch];
    
    
    // switch
    //[self.gender setTransform:CGAffineTransformMakeScale(4.0, 1.0)];
    // boder cho textview
    self.viewGroundLike.layer.borderWidth = 2;
    self.viewGroundLike.layer.borderColor = [[UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0] CGColor];
    
    self.viewGroundLike.layer.masksToBounds = YES;
    self.viewGroundLike.layer.cornerRadius = 5;
    
    // maximum for datePicker
    NSDate *dateCurrent = [NSDate date];
    [self.birthDay setMaximumDate:dateCurrent];
    
    //
    [self.birthDay addTarget:self action:@selector(changeDateTime) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    // an keyboad khi click vao vung trong.
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(toucheOnView:)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - set hide keyboard

-(void)keyboardWasShown:(NSNotification *)notification{
    NSLog(@"Keyboard is shown.");
    
    if ([self.nameBaby isFirstResponder]) {
        return;
    }
    
    float viewWidth = self.view.frame.size.width;
//    float viewHeight = self.view.frame.size.height;
    float viewHeight = 400;
    
    // Get the size of the keyboard from the userInfo dictionary.
    NSDictionary *keyboardInfo = [notification userInfo];
    CGSize keyboardSize = [[keyboardInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    float keyboardHeight = keyboardSize.height;
        
    
    CGRect viewableAreaFrame = CGRectMake(0.0, 0.0, viewWidth, viewHeight - keyboardHeight);
    
    
    CGRect txtDemoFrame = self.viewMain.frame;
    
    if (!CGRectContainsRect(viewableAreaFrame, txtDemoFrame)) {
        // We need to calculate the new Y offset point.
        float scrollPointY = viewHeight - keyboardHeight;
        _originalScrollerOffsetY = self.scrollMain.contentOffset.y;
        
        // Finally, scroll.
        [self.scrollMain setContentOffset:CGPointMake(0.0, scrollPointY) animated:YES];
    }
}


-(void)keyboardWillHide:(NSNotification *)notification{
    NSLog(@"Keyboard is closing.");
    
    // Move the scroll view back into its original position.
    [self.scrollMain setContentOffset:CGPointMake(0.0, -64) animated:YES];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
   
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.like = textView;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    self.like = nil;
    
}
#pragma mark - set event click UIBarButtonItem
- (void)saveInfoBaby;
{
    NSLog(@"==>call method save info baby");
    
    // thong tin day ve:
    NSString *name = self.nameBaby.text; // lay thong tin tu textField name
    
    NSString *likeBaby = self.like.text; // lay thong tin tu texview;
    
    NSDate *changeDateTime = [self changeDateTime];
    NSInteger createTimeInterval = [changeDateTime timeIntervalSince1970];
    
    //neu tao moi
    if (self.stateWorking == 0)
    {
        int indexBabyNew = indexBaby + 1;
        NSString *nameFile = [NSString stringWithFormat:@"baby_%d",indexBabyNew];
        [self saveImage:self.imageFolder nameFile:nameFile];
        
        [database insertBaby:name avata:nameFile birth:createTimeInterval gender:gender like:likeBaby];
        [database updateIndexBabyInfo:indexBabyNew];
        
    }
    // neu thay doi thong tin baby
    else{
        // cap nhat lai avata
        [self removeImage:self.getBabyInfo.avata];
        [self saveImage:self.imageFolder nameFile:self.getBabyInfo.avata];
        [database updateBabyInfo:self.getBabyInfo.iD name:self.nameBaby.text avata:self.getBabyInfo.avata gender:gender birth:createTimeInterval like:likeBaby];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [database closeData];
}

- (void)cancelInfoBaby;
{
     NSLog(@"ban da lua chon huy thao tac");
     [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - update return time when change UIDatePicker

// xuat time khi thay doi thoi gian trong UIDatePicker
- (NSDate *)changeDateTime;
{
    [self toucheHidekeyBoard];
    self.dateTime = self.birthDay.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];                               //cccc, MMMM d, hh:mm aa
    NSString *prettyVersion = [dateFormat stringFromDate:self.dateTime];
    NSLog(@"%@", prettyVersion);
    NSDate *timeCreateDairy = [dateFormat dateFromString:prettyVersion];
    NSLog(@"ngay sinh luu lai o dang string %@", timeCreateDairy);
    return timeCreateDairy;
}

#pragma mark - set event change gender baby
// xac nhan goi tinh khi click segment
- (IBAction)gender:(id)sender {
    if ([sender selectedSegmentIndex] == 0){
        //Nam
        gender = 0;
    }
    else{
        // nu
        gender = 1;
        
    }
}

#pragma mark - method an keyboard khi click vung trong

- (void)toucheOnView:(UIGestureRecognizer *)gesture;
{
    [self.nameBaby resignFirstResponder];
    [self.like resignFirstResponder];
}
- (void)toucheHidekeyBoard;
{
//    [self.emotions resignFirstResponder];
//    [self.titleDairy resignFirstResponder];
//    [self.addressDairy resignFirstResponder];
}


#pragma mark - set event change avata


- (IBAction)camera:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Thiết bị của bạn không hỗ trợ camera" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (IBAction)folder:(id)sender {
    
    BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;

    
    picker.sourceType = hasCamera ? UIImagePickerControllerSourceTypeCamera :    UIImagePickerControllerSourceTypePhotoLibrary;
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:picker];
    CGRect popoverRect = [self.view convertRect:[self.folderOutlet frame]
                                       fromView:[self.folderOutlet superview]];
    
    popoverRect.size.width = MIN(popoverRect.size.width, 100) ;
    popoverRect.origin.x = popoverRect.origin.x;
    
    [popoverController
     presentPopoverFromRect:popoverRect
     inView:self.view
     permittedArrowDirections:UIPopoverArrowDirectionAny
     animated:YES];

}
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage* imageLoadFolder = [info objectForKey:UIImagePickerControllerOriginalImage];
    //img.image= image;
    self.imageBaby.image = imageLoadFolder;
    self.imageFolder  = nil;
    self.imageFolder = imageLoadFolder;
    
    if (popoverController != nil) {
        [popoverController dismissPopoverAnimated:YES];
        popoverController=nil;
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    if (popoverController != nil) {
        [popoverController dismissPopoverAnimated:YES];
        popoverController=nil;
    }
    
}


- (void)pickerDone:(id)sender
{
    if (popoverController != nil) {
        [popoverController dismissPopoverAnimated:YES];
        popoverController=nil;
    }
}

#pragma mark - viewDidDisappear
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self.nameBaby resignFirstResponder];
    [self.like resignFirstResponder];
}

#pragma mark - work with file system.

// save luu file
- (void)saveImage:(UIImage *)imageTemp nameFile:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(imageTemp); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    NSLog(@"image saved");
    
}

//xoa 1 file image co ten laf fileName

- (void)removeImage:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"image removed");
    
}



// load 1 file tu he thong len, voi doi so la ten nameFile, tra ve i UIImage.

- (UIImage*)loadImage:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}


@end
