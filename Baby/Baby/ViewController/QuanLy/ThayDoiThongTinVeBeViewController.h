//
//  ThayDoiThongTinVeBeViewController.h
//  Baby
//
//  Created by kim ly on 4/4/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "SQLiteDB.h"
#import "Baby.h"
#import "Info.h"

/*
 screen nay chua 1 textField cho phep thay doi ten baby, 1 textView de thay thoi so thich cua be,
 1 datePicker cho phep thay doi ngay thang nam sinh - gioi han maximun la ngay hien tai,
 1 button cho phep chon avata tu camerarol
 1 button cho phep hon avata = cach dung camera tao moi
 1 segment thay doi goi tinh baby
 2 BarButtonItem de "luu" & "huy" - "luu" thanh cong neu ten duoc nhap 
 */


@interface ThayDoiThongTinVeBeViewController : UIViewController
<UIPopoverControllerDelegate, UIImagePickerControllerDelegate,
UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate>
{
    SQLiteDB *database;
    Info *getInfoObject;
    Baby *babyChangeTemp;
    int indexBaby;
    
    
    // edit info
    int gender;
    
    
    UIPopoverController *popoverController;
}

// khai bao 2 view long len content va avata, de sau nay set lai layout cho chung

@property (strong, nonatomic) IBOutlet UIView *viewAvata;

//
@property (strong, nonatomic) IBOutlet UIImageView *imageBaby;
@property (strong, nonatomic) IBOutlet UITextField *nameBaby;

- (IBAction)camera:(id)sender;

- (IBAction)folder:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *folderOutlet;
@property(strong, nonatomic)UIImage *imageFolder;

@property (strong, nonatomic) IBOutlet UIDatePicker *birthDay;
// dateTime: lay ra ngay gi[
@property(strong, nonatomic)NSDate *dateTime;


- (IBAction)gender:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *genderOutlet;


@property (strong, nonatomic) IBOutlet UITextView *like;
@property (strong, nonatomic) IBOutlet UIView *viewMain;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollMain;
@property (strong, nonatomic) IBOutlet UIView *viewGroundLike;

// khai bao 1 button tren navicontroller de bat su kien luu or huy thay doi
@property(strong, nonatomic)UIBarButtonItem *saveInfo;
@property(strong, nonatomic)UIBarButtonItem *cancelInfo;

// set thong tin dua vao lan dau
@property(assign, nonatomic)NSInteger stateWorking;   // 0 -tao moi >< 1 - edit
@property(assign, nonatomic)NSInteger indexBabyInfo;  // cap nhap id cua be duoc thay doi.
@property(strong, nonatomic)Baby *getBabyInfo;

@end
