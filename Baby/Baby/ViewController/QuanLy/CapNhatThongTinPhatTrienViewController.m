//
//  CapNhatThongTinPhatTrienViewController.m
//  Baby
//
//  Created by kim ly on 4/15/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "CapNhatThongTinPhatTrienViewController.h"

@interface CapNhatThongTinPhatTrienViewController ()

@end

@implementation CapNhatThongTinPhatTrienViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // open data
    database = [[SQLiteDB alloc] init];
    [database openData];
	
    // thiet lap ban dau
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nen.png"]];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundView.png"]];
    self.title = @"Cập nhật thông tin phát tiển";
	
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    
    self.backProfile = [[UIBarButtonItem alloc] initWithTitle:@"Huỷ" style:UIBarButtonItemStyleBordered target:self action:@selector(backScreen)];
    
    self.navigationItem.leftBarButtonItem = self.backProfile;
    
    
    // an ban phim khi click vao khoang trong
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(toucheOnView:)];
    [self.view addGestureRecognizer:gestureRecognizer];
    self.viewBackgroundContent.layer.cornerRadius = 8;
    self.viewBackgroundContent.layer.masksToBounds = YES;
    self.viewBackgroundContent.layer.borderColor = [[UIColor whiteColor] CGColor];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - event click vung trong
- (void)toucheOnView:(UIGestureRecognizer *)gesture;
{
    [self.mothInfo resignFirstResponder];
    [self.heightInfo resignFirstResponder];
    [self.weightInfo resignFirstResponder];
}

#pragma mark - cancell

- (void)backScreen;
{
    
    [database closeData];
    NSLog(@"ban da lua chon huy thao tac");
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - event save info
- (IBAction)saveInfo:(id)sender {
    // luu tru thong tin phat trien cua baby theo idBaby
    int month = [self.mothInfo.text intValue];
    
    float height = [self.heightInfo.text floatValue];
    NSNumber *heightNumber = [[NSNumber alloc] initWithFloat:height];
    
    float weight = [self.weightInfo.text floatValue];
    NSNumber *weightNumber = [[NSNumber alloc] initWithFloat:weight];
    
    if (([self.mothInfo.text length] == 0) || ([self.heightInfo.text length] == 0) || ([self.weightInfo.text length] == 0)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo:" message:@"Bạn cần nhập thông tin đầy đủ" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else{
        int monthCheck = [self.mothInfo.text intValue];
        BOOL stateUpdate = [database checkMonthOfBabyWithIDAndMonth:self.iDBaby month:monthCheck];
        if (stateUpdate) {// da co
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo:" message:@"Thông tin của bé về tháng này đã có, bạn không thể thêm vào" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];

        }
        else{// chua co
            [database insertBabyGrow:self.iDBaby month:month height:heightNumber weight:weightNumber];
            [database closeData];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }
    
    
    // dong data
    
}
@end
