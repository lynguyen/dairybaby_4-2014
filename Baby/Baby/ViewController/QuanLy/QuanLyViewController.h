//
//  QuanLyViewController.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLiteDB.h"

@interface QuanLyViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,
UIPickerViewDelegate, UIPickerViewDataSource,
UIAlertViewDelegate>
{
    SQLiteDB *database;
    // bat su kien chon 1 be trong danh sach
    UIPopoverController *popoverChoiceBabyController;
    UIToolbar *toolBarBary;
    UIPickerView *myPickerBaby;
    
    
    // chung chung
    int indexBaby;
    int indexBabySelect;
    int noteMonth;
    NSMutableArray *listNameBaby; // list baby string add bao picker
    NSMutableArray *listObjecBaby;
    NSMutableArray *listInfoGrowBaby;
    // array: getListBaby = danh sach ten baby dang lua chon
    
}

// khai bao view chua 1 button lua chon thong tin ve 1 be yeu,
//1 segment cho phep thay doi chuc nang xem thong tin ca nhan va thong tin phat trien cua be

@property (strong, nonatomic) IBOutlet UIView *viewFunction;

@property (strong, nonatomic) IBOutlet UIButton *listBabyOutlet;
- (IBAction)listBaby:(id)sender;

- (IBAction)mySegment:(id)sender;


// khai bao 1 view va 1 UIToolbar

@property (strong, nonatomic) IBOutlet UIView *viewBabyInfo;
@property (strong, nonatomic) IBOutlet UILabel *nameInfo;
@property (strong, nonatomic) IBOutlet UILabel *birthInfo;
@property (strong, nonatomic) IBOutlet UILabel *genderInfo;
@property (strong, nonatomic) IBOutlet UITextView *likeOfBaby;
@property (strong, nonatomic) IBOutlet UIImageView *avataInfo;

@property (strong, nonatomic) IBOutlet UIToolbar *toolbarInfo;
- (IBAction)changeInfo:(id)sender;
- (IBAction)newBabyInfo:(id)sender;



// khai bao 1 view va 1 UIToolbar su dung cho chu nang hien thi thong tin be dang lon

@property (strong, nonatomic) IBOutlet UIView *viewBabyGrow;
@property (strong, nonatomic) IBOutlet UITableView *myTable;

@property (strong, nonatomic) IBOutlet UIToolbar *toolbarGrow;
- (IBAction)updateInfoGrow:(id)sender;
- (IBAction)commentInfoGrow:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *viewBackGroundShowInfoBaby;









@end
