//
//  TaoMoiNhatKyViewController.h
//  Baby
//
//  Created by kim ly on 3/26/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaoMoiNhatKyCollectionViewCell.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AssetsPickerController.h"
#import "SQLiteDB.h"
#import "Info.h"
#import "Album.h"
#import "Dairy.h"

typedef void(^SaveImageCompletion)(NSError* error);

@interface TaoMoiNhatKyViewController : UIViewController
<
UICollectionViewDataSource, UICollectionViewDelegate,
UITextViewDelegate, UITextFieldDelegate,
UIPickerViewDelegate, UIPickerViewDataSource,
UINavigationControllerDelegate,
UIPopoverControllerDelegate,
UIImagePickerControllerDelegate,
AssetsPickerControllerDelegate,
UIAlertViewDelegate
>
{
    SQLiteDB *database;
    
    int indexBaby; // id cua baby chinh trong su kien, no duoc chon picker.
    int newIndexDairy;// indexBaby da co + 1; cap nhat vao in for sau khi luu
    int indexAlbum; // index cua file hien tai da tao toi
    NSString * nameDairy;// name cua dairy nay
    Album *newAlbum;// list data new baby
    NSMutableArray *listAlbum;
    NSMutableArray *getListBaby;
    NSMutableArray *listNameBaby; // list baby string add bao picker
    // popoer su dung de hien thi chen anh tu folder
    
    // popoer su dung de hien thi  camera cho phep chup anh
    UIPopoverController *popoverCamera;
    
    // gia dinh do cell
    int countCell;
    
    // trang thai goi keyboard
    BOOL statusKeyboard;
    
}


// khai bao cac UI element su dung
@property (strong, nonatomic) IBOutlet UITextField *addressDairy;


@property (strong, nonatomic) IBOutlet UITextField *titleDairy;

@property (strong, nonatomic) IBOutlet UIDatePicker *timeDairy;

@property (strong, nonatomic) IBOutlet UITextView *emotions;

@property (strong, nonatomic) IBOutlet UICollectionView *myListImageVideo;


@property (strong, nonatomic) IBOutlet UIView *viewBackgroundEmotion;



@property(strong, nonatomic)UIBarButtonItem *saveDairy;
@property(strong, nonatomic)UIBarButtonItem *cancelDairy;


// khai bao cac button tuong ung: chon su kien ve 1 be, dung camera, lua chon anh tu cameraroll
- (IBAction)selectBaby:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *selectBabyOutlet;

// dateTime: lay ra ngay gi[
@property(strong, nonatomic)NSDate *dateTime;

- (IBAction)camera:(id)sender;

- (IBAction)folder:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *folderOutletButton;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollMain;
@property (strong, nonatomic) IBOutlet UIView *viewMain;

// khai bao ALAssetsLibrary de luu tru imge select tu Camera roll


@end
