
//
//  TaoMoiNhatKyViewController.m
//  Baby
//
//  Created by kim ly on 3/26/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "TaoMoiNhatKyViewController.h"
#import "Info.h"
#import "Dairy.h"
#import "Album.h"
#import "Baby.h"
#import "ChenCamXucViewController.h"
@interface TaoMoiNhatKyViewController () 
{
    
    // choice baby
    UIPopoverController *popoverChoiceBabyController;
    UIToolbar *toolBarBary;
    UIPickerView *myPickerBaby;
    
    
    
}
// scroll view
@property (nonatomic) float originalScrollerOffsetY;

-(void)keyboardWasShown:(NSNotification *)notification;
-(void)keyboardWillHide:(NSNotification *)notification;

// choice photos from library
@property (nonatomic, copy) NSArray *assetsFolder;    // dung mang assets de chua du lieu da chon
@property (nonatomic, strong) NSDateFormatter *dateFormatter; //
@property (nonatomic, strong) UIPopoverController *popoverSelectPhotoFromFolder; // dung no cho ipad


@end

@implementation TaoMoiNhatKyViewController
//@synthesize library;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    countCell = 0;
	// Do any additional setup after loading the view.
    
    //UIScrollView * scrollMain = [UIScrollView alloc];
    //[scrollMain addSubview:self.view];
    // khoi tao mang chua file image, video trong dairy moi
    listAlbum = [[NSMutableArray alloc] init];
    newAlbum = [[Album alloc] init];
    
    // mo database
    database = [[SQLiteDB alloc] init];
    [database openData];
    
    NSMutableArray *getInfo = [[NSMutableArray alloc] init];
    getInfo = [database getInfo];
    Info *objInfo = [[Info alloc] init];
    objInfo = [getInfo objectAtIndex:0];
    newIndexDairy = objInfo.indexDairy + 1;
    nameDairy = [NSString stringWithFormat:@"dairy%d",newIndexDairy];
    NSLog(@"name dairy chuan bi tao moi la %@", nameDairy);
    indexAlbum = 0;
    
    newAlbum = [[Album alloc] init];// mang chua danh sach cac image, video dang tao moi
    
    getListBaby = [[NSMutableArray alloc] init];
    getListBaby = [database getAllBaby];
    
    listNameBaby = [[NSMutableArray alloc] init]; // danh sach ten cac baby
    
    for (int bb = 0; bb < [getListBaby count]; bb ++) {
        Baby *objBaby = [[Baby alloc] init];
        objBaby = [getListBaby objectAtIndex:bb];
        [listNameBaby addObject:objBaby.name];
    }
    
    // thi thi danh sach ba by trong new dairy;
    NSLog(@"Danh sach baby trong tao moi:");
    NSLog(@"%@",listNameBaby);
    
    // default be yeu: default
    indexBaby = 0;
    NSLog(@"Mac dinh ten baby ban dau laf %@", [listNameBaby objectAtIndex:0]);
   // [self.nameBaby setText:[listNameBaby objectAtIndex:0]];

    
    // set title
    self.title = @"Tạo mới nhật ký";//Tạo mới nhật ký
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundWeb.png"]];
    self.view.backgroundColor = [UIColor whiteColor];
    // lay tam csdl listbaby
    
    
    [self.timeDairy addTarget:self action:@selector(changeDateTime) forControlEvents:UIControlEventValueChanged];
    
    NSDate *dateCurrent = [NSDate date];
    [self.timeDairy setMaximumDate:dateCurrent];
    
    // set collor for UINavicontroller
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    
    
    // khoi tao 2 UIBarButtonItem tren navi controller
    self.saveDairy = [[UIBarButtonItem alloc] initWithTitle:@"Lưu" style:UIBarButtonItemStyleBordered target:self action:@selector(saveDairyBaby)];
    
    self.navigationItem.leftBarButtonItem = self.saveDairy;
    
    self.cancelDairy = [[UIBarButtonItem alloc] initWithTitle:@"Huỷ" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelDairyBaby)];
    self.navigationItem.rightBarButtonItem = self.cancelDairy;
    
    // chinh sua layout cuar mot so UI Elements
   // self.emotions.layer.backgroundColor = [[UIColor clearColor] CGColor];
    self.viewBackgroundEmotion.layer.borderWidth = 2;
    self.viewBackgroundEmotion.layer.borderColor = [[UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0] CGColor];
    self.viewBackgroundEmotion.layer.masksToBounds = YES;
    self.viewBackgroundEmotion.layer.cornerRadius = 5;
    
    self.timeDairy.layer.masksToBounds = YES;
    self.timeDairy.layer.cornerRadius = 10;
    self.timeDairy.backgroundColor = [UIColor whiteColor];
    
    
    statusKeyboard = NO;
    // an keyboad khi click vao vung trong.
    //UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
    //                                                                                    action:@selector(toucheOnView:)];
    //[self.view addGestureRecognizer:gestureRecognizer];
   
//  //  [self.scrollMain setContentOffset:CGPointMake(0, -300) animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSLog(@"----%@", NSStringFromCGPoint(self.scrollMain.contentOffset));
}

#pragma mark - Private Methods

-(void)keyboardWasShown:(NSNotification *)notification{
    NSLog(@"Keyboard is shown.");
    
    if (1) {
        
    }
    NSLog(@"toi dang show keyboard len");
    statusKeyboard = YES;
    if ([self.titleDairy isFirstResponder] || [self.addressDairy isFirstResponder]) return;
    
    float viewWidth = self.view.frame.size.width;
   // float viewHeight = self.view.frame.size.height;
    float viewHeight = 450;
    
    // Get the size of the keyboard from the userInfo dictionary.
    NSDictionary *keyboardInfo = [notification userInfo];
    CGSize keyboardSize = [[keyboardInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    float keyboardHeight = keyboardSize.height;
    CGRect viewableAreaFrame = CGRectMake(0.0, 0.0, viewWidth, viewHeight - keyboardHeight);
    
    
    CGRect txtDemoFrame = self.viewMain.frame;
    
    if (!CGRectContainsRect(viewableAreaFrame, txtDemoFrame)) {
        // We need to calculate the new Y offset point.
        float scrollPointY = viewHeight - keyboardHeight;
        _originalScrollerOffsetY = scrollPointY;//self.scrollMain.contentOffset.y;
        
        // Finally, scroll.
        [self.scrollMain setContentOffset:CGPointMake(0.0, scrollPointY) animated:YES];
    }
}


-(void)keyboardWillHide:(NSNotification *)notification{
    NSLog(@"Keyboard is closing.");
    NSLog(@"----%@", NSStringFromCGPoint(self.scrollMain.contentOffset));
    // Move the scroll view back into its original position.
    [self.scrollMain setContentOffset:CGPointMake(0.0, -64) animated:YES];
    NSLog(@"toi vua an key board");
    statusKeyboard = NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //self.textfiel = textField;
    NSLog(@"toi dang click vao textfield");
    if (textField == self.titleDairy) {
        
    }
   
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    //self.textfiel = nil;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.emotions = textView;
    NSLog(@"toi dang click vao textView");
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    self.emotions = nil;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Event lick UIBarButtonItem luu, huy

- (void)saveDairyBaby;
{
    /// Hien thi thong tin da nhap vao
    
    NSLog(@"== > sava new dairy:");
    NSString *title = [[NSString alloc] initWithString:self.titleDairy.text];
    NSLog(@"%@",title);
    NSString *address = [[NSString alloc] initWithString:self.addressDairy.text];
    NSLog(@"%@", address);
    
    NSString *emotion = [[NSString alloc] initWithString:self.emotions.text];
    NSLog(@"%@", emotion);
    NSLog(@"loi tu doan nay");
    
   
    
    Baby *babySave = [getListBaby objectAtIndex:indexBaby];
    int indexBabySave = babySave.iD;
    NSLog(@"ID cua  baby duoc chon al: %d", indexBabySave);
    
    if (title.length == 0 && address.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn cần nhập đầy đủ tên sự kiện và địa điểm" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    else{
        NSDate *changeDateTime = [self changeDateTime];
        NSInteger createTimeInterval = [changeDateTime timeIntervalSince1970];
        
        [database insertDairy:nameDairy avata:indexBabySave title:title address:address time:createTimeInterval emotion:emotion index:1];
        
        NSLog(@"==> da hooan thanh ++>");
        
        [database updateindexDairyInfo:newIndexDairy];
        [database closeData];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    
}

// luu tat cac photo, video duoi dang file

- (void)saveaDataIntoFile{
    
    
}


- (void)cancelDairyBaby;
{
    
    [database delAlbumWithNameDiary:nameDairy];
    [database closeData];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIColletionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if ([listAlbum count] >=3) {
        countCell  = 0;
        return [listAlbum count];
        
    }
    else if ([listAlbum count] ==1){
        countCell = 2;
        return 3;
        
    }
    else if ([listAlbum count] ==2){
        countCell = 1;
        return 3;
    }
    else {
        countCell = 3;
        return 3;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *cellNewDairyIdentifier = @"taoMoiCollectionViewCell";
    
    TaoMoiNhatKyCollectionViewCell *newDairyCell = nil;
    newDairyCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellNewDairyIdentifier forIndexPath:indexPath];
    newDairyCell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon_backgroundImageMain.png"]]];
    
    newDairyCell.imageVideo.layer.backgroundColor = [[UIColor clearColor] CGColor];
    newDairyCell.imageVideo.layer.masksToBounds  = YES;
    newDairyCell.imageVideo.layer.cornerRadius = 7;
    newDairyCell.selected = YES;            //accessoryType
    
    // tam set gia tri bang anh duoc lay ve tu photolibrary trong assets
//    if (indexPath.row < [listAlbum count] ) {
//        Album *albumCell = [listAlbum objectAtIndex:indexPath.row];
//        
//        UIImage *imageCell = [self loadImage:albumCell.nameFile];
//        newDairyCell.imageVideo.image = imageCell;
//    }
    
    if (countCell == 3) {
        // trong 3 o
        
    }
    else if(countCell== 2){
        // trong 2 o
        if (indexPath.row == 0) {
            Album *albumCell = [listAlbum objectAtIndex:indexPath.row];
            
            UIImage *imageCell = [self loadImage:albumCell.nameFile];
            newDairyCell.imageVideo.image = imageCell;
        }
    }
    else if(countCell == 1){
        if (indexPath.row != 2) {
            Album *albumCell = [listAlbum objectAtIndex:indexPath.row];
            
            UIImage *imageCell = [self loadImage:albumCell.nameFile];
            newDairyCell.imageVideo.image = imageCell;
        }
    }
    else{
        Album *albumCell = [listAlbum objectAtIndex:indexPath.row];
        
        UIImage *imageCell = [self loadImage:albumCell.nameFile];
        newDairyCell.imageVideo.image = imageCell;
    }
    
    
    return newDairyCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //chenCamXucViewController
//    ChenCamXucViewController *insertEmotion = [self.storyboard instantiateViewControllerWithIdentifier:@"chenCamXucViewController"];
//    UINavigationController *naviInsertEmotion = [[UINavigationController alloc] initWithRootViewController:insertEmotion];
//    insertEmotion.indexAlbum = indexPath.row;
//    insertEmotion.nameDairy = nameDairy;
//    [self presentViewController:naviInsertEmotion animated:YES completion:nil];
    
/*    if ((countCell == 1) && (indexPath.row <2)) {
        //chenCamXucViewController
        ChenCamXucViewController *insertEmotion = [self.storyboard instantiateViewControllerWithIdentifier:@"chenCamXucViewController"];
        UINavigationController *naviInsertEmotion = [[UINavigationController alloc] initWithRootViewController:insertEmotion];
        insertEmotion.indexAlbum = indexPath.row;
        insertEmotion.nameDairy = nameDairy;
        [self presentViewController:naviInsertEmotion animated:YES completion:nil];

    }
    else if ((countCell == 2) && (indexPath.row == 0))
    {
        //chenCamXucViewController
        ChenCamXucViewController *insertEmotion = [self.storyboard instantiateViewControllerWithIdentifier:@"chenCamXucViewController"];
        UINavigationController *naviInsertEmotion = [[UINavigationController alloc] initWithRootViewController:insertEmotion];
        insertEmotion.indexAlbum = indexPath.row;
        insertEmotion.nameDairy = nameDairy;
        [self presentViewController:naviInsertEmotion animated:YES completion:nil];

    }
    else if (countCell == 3)
    {
        // khong lam j vi thuc te khong co image nao de chuyen view controll
    }
    else{
        //chenCamXucViewController
        ChenCamXucViewController *insertEmotion = [self.storyboard instantiateViewControllerWithIdentifier:@"chenCamXucViewController"];
        UINavigationController *naviInsertEmotion = [[UINavigationController alloc] initWithRootViewController:insertEmotion];
        insertEmotion.indexAlbum = indexPath.row;
        insertEmotion.nameDairy = nameDairy;
        [self presentViewController:naviInsertEmotion animated:YES completion:nil];
    }
 */
    if (indexPath.row < [listAlbum count]) {
        //chenCamXucViewController
        ChenCamXucViewController *insertEmotion = [self.storyboard instantiateViewControllerWithIdentifier:@"chenCamXucViewController"];
        UINavigationController *naviInsertEmotion = [[UINavigationController alloc] initWithRootViewController:insertEmotion];
        insertEmotion.indexAlbum = indexPath.row;
        insertEmotion.nameDairy = nameDairy;
        [self presentViewController:naviInsertEmotion animated:YES completion:nil];
    }
    
}


#pragma mark - Event lick button

- (IBAction)selectBaby:(id)sender {
    
    
    [self toucheHidekeyBoard];
    // su dung 1 UIPicker de hien thi danh sach be yeu.
    
    myPickerBaby = [[UIPickerView alloc] init];
    toolBarBary = [[UIToolbar alloc] init];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        UIViewController *popOverContentBaby= [[UIViewController alloc] init];
        
        UIView *popOverView = [[UIView alloc]
                               initWithFrame:CGRectMake(0, 0, [myPickerBaby frame].size.width - 110, [myPickerBaby frame].size.height )];//height+ 44
        
        myPickerBaby = [[UIPickerView alloc]
                        initWithFrame:CGRectMake([popOverView frame].origin.x  -60, [popOverView frame].origin.y, 0 , 0)];//y + 44
        
        toolBarBary = [[UIToolbar alloc]
                       initWithFrame:CGRectMake([popOverView frame].origin.x, [popOverView frame].origin.y, [popOverView frame].size.width, 30)];//44
        
        toolBarBary.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
        
        [myPickerBaby setDataSource:self];
        
        [myPickerBaby setDelegate:self];
        [myPickerBaby setShowsSelectionIndicator:YES];
        [toolBarBary setBarStyle:UIBarStyleDefault];
        
        UIBarButtonItem *fleXibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"Lựa chọn" style:UIBarButtonItemStyleBordered target:self action:@selector(donePressed)];
        
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"Huỷ" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelPressed)];
        
        
        [toolBarBary setItems:[NSArray arrayWithObjects: done, fleXibleSpace, cancel, nil]];
        
        [popOverView addSubview:myPickerBaby];
        
        [popOverView addSubview:toolBarBary];
        [popOverContentBaby setView:popOverView];
        
        popoverChoiceBabyController = [[UIPopoverController alloc] initWithContentViewController:popOverContentBaby];
        popoverChoiceBabyController.popoverContentSize  = CGSizeMake([myPickerBaby frame].size.width - 110, [popOverView frame].size.height);
        
        [popoverChoiceBabyController presentPopoverFromRect:[self selectBabyOutlet].frame inView:self.selectBabyOutlet.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        
    }
    
}
- (IBAction)camera:(id)sender {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Thiết bị của bạn không hỗ trợ camera" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //[alert show];
}

- (IBAction)folder:(id)sender {
    
    [self toucheHidekeyBoard ];
    //if (!self.assetsFolder){
        self.assetsFolder = [[NSMutableArray alloc] init];  // cap phat va khoi tao array assets o day
       
   // }
    
    // khoi tao mot doi tuong xu ly chon anh
    AssetsPickerController *picker = [[AssetsPickerController alloc] init];
    picker.assetsFilter         = [ALAssetsFilter allAssets];
    picker.showsCancelButton    = (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad);
    picker.delegate             = self;
    
    
    picker.selectedAssets       = [NSMutableArray arrayWithArray:self.assetsFolder];
    
    // iPad
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        // su dung popover de hien thi
        self.popoverSelectPhotoFromFolder = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        self.popoverSelectPhotoFromFolder.delegate = self;
        
        self.popoverSelectPhotoFromFolder.popoverContentSize = CGSizeMake(400.0f, 550.0f);
        
        //NSLog(@"den day thanh cong");
        [self.popoverSelectPhotoFromFolder presentPopoverFromRect:[self folderOutletButton].frame inView:self.folderOutletButton.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        //NSLog(@"den day khong thanh cong");
    }
    else
    {
        [self presentViewController:picker animated:YES completion:nil];
    }
  
    ////////////////////////////////////////////////////
    
    
    /** hien thi camera mac dinh
    
        UIImagePickerController *pickerSellectPhotos = [[UIImagePickerController alloc] init];
    
        // set sourceType and mediaTypes
        pickerSellectPhotos.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        pickerSellectPhotos.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
        // khi tao popover
        popoverSelectfolder = [[UIPopoverController alloc] initWithContentViewController:pickerSellectPhotos];
        popoverSelectfolder.popoverContentSize = CGSizeMake(720.0f, 668.0f);
    
    
         //hien thi popover
        [popoverSelectfolder presentPopoverFromRect:self.button.frame inView:self.button.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
     
        [popoverSelectfolder presentPopoverFromRect:[self folderOutletButton].frame inView:self.folderOutletButton.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
        */
    
    

}


#pragma mark - Popover Controller Delegate

// goi method popoverController Delegate dung tam huy 1 popover

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.popoverSelectPhotoFromFolder = nil;
}


#pragma mark-  @implement method of UIImagePickerController delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    NSLog(@"huy su dung UIImagePickerViewController");
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    
    NSLog(@"%@", info);
    
    

    
    if (popoverChoiceBabyController != nil) {
        [popoverChoiceBabyController dismissPopoverAnimated:YES];
        popoverChoiceBabyController = nil;
    }
    
   [picker dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark - Assets Picker Delegate for event Action Choice photo from libarary


// method nay dung de huy popover
//lay data assets ve
- (void)assetsPickerController:(AssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    if (self.popoverSelectPhotoFromFolder != nil)
        [self.popoverSelectPhotoFromFolder dismissPopoverAnimated:YES];
    else
        [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    self.assetsFolder = [NSMutableArray arrayWithArray:assets];
    
    
    if (self.assetsFolder == nil) {
        
    }
    else {
        // copy cac danh da chon vao trog listAlbum
        
        int countFile = [self.assetsFolder count];
        NSLog(@"so anh da day ve sau khi chon la: %d", countFile);
        
        for (int img = 0; img < countFile; img++) {
            ALAsset *asset = [self.assetsFolder objectAtIndex:img];
            //UIImage *imageSaveTemp = [UIImage imageWithCGImage:asset.thumbnail];
            NSString *nameFile = [NSString stringWithFormat:@"%@_%d", nameDairy, indexAlbum];
            
            // luu du lieu image vao system
            
            //[self saveImage:[UIImage imageWithCGImage:asset.thumbnail] nameFile:nameFile];
            [self saveImage:[UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]] nameFile:nameFile];
            
            Album *albumSaveSystemTemp = [[Album alloc] init];
            
            albumSaveSystemTemp.nameFile = nameFile;
            albumSaveSystemTemp.nameDairy = nameDairy;
            albumSaveSystemTemp.type = 1;       // mac dinh 1-image , 2-video
            albumSaveSystemTemp.emotion = @"";
            
            [database insertAlbum:albumSaveSystemTemp.nameFile type:albumSaveSystemTemp.type emotion:albumSaveSystemTemp.emotion nameDairy:nameDairy];
            
            [listAlbum addObject:albumSaveSystemTemp];
            NSLog(@"ten cua album vua moi luu: %@",nameFile);
            NSLog(@"ban moi chen vao 1 album - namefile:%@ - nameType: %d - nameDairy:%@ - emotion:%@",albumSaveSystemTemp.nameFile, albumSaveSystemTemp.type, albumSaveSystemTemp.nameDairy, albumSaveSystemTemp.emotion);
            indexAlbum += 1;
        }
        
        
        [self.myListImageVideo reloadData];
    }
    
    // sau khi lay duoc data tu popover => xuat hieen alert de chen cam xuc, bo qua neu khong muon
    //.......... 
    
    //[self.tableView reloadData]; // note: reload data trong table
}


// xu ly cho truong hop du lieu tra ve la video
- (BOOL)assetsPickerController:(AssetsPickerController *)picker shouldEnableAssetForSelection:(ALAsset *)asset
{
    // Enable video clips if they are at least 5s
    if ([[asset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo])
    {
        NSTimeInterval duration = [[asset valueForProperty:ALAssetPropertyDuration] doubleValue];
        return lround(duration) >= 5;
    }
    else
    {
        return YES;
    }
}


// xu ly loi neu co

- (BOOL)assetsPickerController:(AssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
    if (picker.selectedAssets.count >= 10)//>20
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Thông báo"
                                   message:@"Bạn không được chọn quá 10"
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
    }
    
    if (!asset.defaultRepresentation)
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Thông báo"
                                   message:@"Your asset has not yet been downloaded to your device"
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
    }
    
    return (picker.selectedAssets.count < 10 && asset.defaultRepresentation != nil);
}
#pragma mark -  event insert text for photo select from library
- (void)insertEmotionDairyEventSelectFolder{
    
    
    
    
}
#pragma mark - implement method of UIAlert delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
}




#pragma mark - update return time when change UIDatePicker

// xuat time khi thay doi thoi gian trong UIDatePicker
- (NSDate *)changeDateTime;
{
    [self toucheHidekeyBoard];
    self.dateTime = self.timeDairy.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];                               //cccc, MMMM d, hh:mm aa
    NSString *prettyVersion = [dateFormat stringFromDate:self.dateTime];
    NSLog(@"%@", prettyVersion);
    NSDate *timeCreateDairy = [dateFormat dateFromString:prettyVersion];
     NSLog(@"thoi gian luu lai o dang string %@", timeCreateDairy);
    return timeCreateDairy;
}



#pragma mark - @implement UIPicker delegate choice baby

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    return [listNameBaby count];
}

// hien thi danh sach be yeu trong listBaby
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    return [listNameBaby objectAtIndex:row];
}

// bat su kien khi click 1 be trong danh sach
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    //[[self nameBaby]setText:[self.listBaby objectAtIndex:row]];
    indexBaby = row;
    NSLog(@"Be duoc lua cho o vi tri index %i",row);
    
}


// su kien khi click button done
- (void)donePressed;
{
    //[[self nameBaby]setText:[listNameBaby objectAtIndex:indexBaby]];
    NSLog(@" toi da nhan nut dong y thay doi");
    NSString *changeBaby = [NSString stringWithFormat:@"%@",[listNameBaby objectAtIndex:indexBaby]];
    [self.selectBabyOutlet setTitle:changeBaby forState:UIControlStateNormal];
    [popoverChoiceBabyController dismissPopoverAnimated:YES];
    
}

- (void)cancelPressed;
{
    //[[self nameBaby]setText:[listNameBaby objectAtIndex:indexBaby]];
    [popoverChoiceBabyController dismissPopoverAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    if ([popoverChoiceBabyController isPopoverVisible]) {
        [popoverChoiceBabyController dismissPopoverAnimated:YES];
    }
    [self.emotions resignFirstResponder];
    [self.titleDairy resignFirstResponder];
    [self.addressDairy resignFirstResponder];
}





#pragma mark -UITextField delegate

//UTextFieldDelegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - UITextView delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
//    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//    }
    return YES;
}

#pragma mark - method an keyboard khi click vung trong

- (void)toucheOnView:(UIGestureRecognizer *)gesture;
{
    if (statusKeyboard) {
        [self.emotions resignFirstResponder];
        [self.titleDairy resignFirstResponder];
        [self.addressDairy resignFirstResponder];
    }
    
}
- (void)toucheHidekeyBoard;
{
    [self.emotions resignFirstResponder];
    [self.titleDairy resignFirstResponder];
    [self.addressDairy resignFirstResponder];
}

#pragma mark - work with file system.

// save luu file
- (void)saveImage:(UIImage *)imageTemp nameFile:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(imageTemp); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    NSLog(@"image saved");
    
}

//xoa 1 file image co ten laf fileName

- (void)removeImage:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"image removed");
    
}



// load 1 file tu he thong len, voi doi so la ten nameFile, tra ve i UIImage.

- (UIImage*)loadImage:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}


@end
