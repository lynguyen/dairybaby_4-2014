//
//  NhatKyViewController.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NhatKyTableViewCell.h"
#import "Info.h"
#import "Baby.h"
#import "Dairy.h"
#import "Album.h"
#import "SQLiteDB.h"

@interface NhatKyViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,
UISearchBarDelegate,
UIAlertViewDelegate>
{
    SQLiteDB *database;
    NSMutableArray *getListInfo;
    NSMutableArray *getListDairy;
    NSMutableArray *getListDairyCopy;
    NSMutableArray *getListAlbum;
    NSMutableArray *getListBaby;
    int indexDairy;
    
    // trang thai ban phim
    BOOL statusKeyboard;
}

// khai bao 2 UI element su dung
@property (strong, nonatomic) IBOutlet UISearchBar *mySearchBar;
@property (strong, nonatomic) IBOutlet UITableView *myTableViewNhatKy;

// trang thai su dung UISearchBar
@property BOOL isFilter;
@end
