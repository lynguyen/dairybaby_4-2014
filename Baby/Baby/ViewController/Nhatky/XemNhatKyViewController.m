//
//  XemNhatKyViewController.m
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "XemNhatKyViewController.h"
#import "XemNhatKyCollectionViewCell.h"
#import "XemChiTietViewController.h"
#import "Baby.h"
@interface XemNhatKyViewController ()

@end

@implementation XemNhatKyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)backDairy;
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    databse = [[SQLiteDB alloc] init];
    [databse openData];
    dairyObj = [[Dairy alloc] init];
    
   
    NSMutableArray *getDataDairy = [[NSMutableArray alloc] init];
    //NSMutableArray *info = [[NSMutableArray alloc] init];
    
    
    NSLog(@"goi mehtod: ===> get dairy with id");
   
    getDataDairy = [databse getDairyWithID:self.indexDairy];
    dairyObj = [getDataDairy objectAtIndex:0];
    ///NSLog(@"thong tin cua id dairy ben View xem la; %d", self.indexDairy);
    //NSLog(@"thong tin ve dairy nay: id: %d - name: %@ - emotion: %@ - address: %@" ,dairyObj.iD, dairyObj.name, dairyObj.emotion, dairyObj.address);
    nameDiaryChoice = dairyObj.name;
	// Do any additional setup after loading the view.
    //NSLog(@"id cua dairy vua moi duoc dua vao: %d",self.indexDairy);
    
    // view image avata
    
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nen.png"]];
    // set collor for UINavicontroller
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    self.title = @"Xem nhật ký";
    
    // khoi tao 2 UIBarButtonItem tren navi controller
//    self.backMainDairy = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"tron.PNG"] style:UIButtonTypeCustom target:self action:@selector(backDairy)];
//    self.navigationItem.leftBarButtonItem = self.backMainDairy;
    
    //hien thi anh cho avata
    UIImage *avataShow = [self loadImage:self.nameFileAvata];
    self.avata.image = avataShow;
    // border cho avata
    self.avata.layer.masksToBounds = NO;
    self.avata.layer.cornerRadius = 8;
    self.avata.layer.shadowRadius = 5;
    self.avata.layer.shadowOpacity = 0.5;
    self.avata.layer.shadowOffset = CGSizeMake(-15,20);
    
    self.viewEmotion.layer.cornerRadius = 8;
    self.viewEmotion.layer.masksToBounds = YES;
    self.viewEmotion.layer.borderColor = [[UIColor colorWithRed:193.0/255 green:205.0/255 blue:193.0/255 alpha:1.0] CGColor];

    self.viewEmotion.layer.borderWidth = 1;
    
    self.titleDairy.font = [UIFont boldSystemFontOfSize:30.0f];
    self.titleDairy.font = [UIFont italicSystemFontOfSize:30.0f];
    
    self.emotion.font = [UIFont fontWithName:@"ArialMT" size:18];
    
    // get album image, video
    getListAlbumDairy = [[NSMutableArray alloc] init];
    getListAlbumDairy = [databse getAllAlbumWithNameDairy:dairyObj.name];
    
    //NSLog(@"so anh trong 1 album la; %d",[getListAlbumDairy count]);

    // set da ta ban dau cho cac  label
    self.titleDairy.text = dairyObj.title;
    self.addressdairy.text = dairyObj.address;
    self.emotion.text = dairyObj.emotion;
    
    NSLog(@"&&&& ==> xem lai 1 lan nua: ");
    NSDate *eventDate1 = dairyObj.time;
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
    dateFormatter1.dateFormat = @"dd-MM-yyyy";
    NSString  *theDate1 = [dateFormatter1 stringFromDate:eventDate1];
    NSLog(@"theDate %@", theDate1);
    
    self.timeDairy.text = theDate1;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"---> so image trong collectionviewCell la: %d",[getListAlbumDairy count]);
    return [getListAlbumDairy count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellXemNhatKyIdentifier = @"xemNhatKyCollectionViewCell";
   // XemNhatKyCollectionViewCell *cellXemNhatKy;
    XemNhatKyCollectionViewCell *cellNhatKyObj = nil;
    cellNhatKyObj = [collectionView dequeueReusableCellWithReuseIdentifier:cellXemNhatKyIdentifier forIndexPath:indexPath];
    
    
    // chen anh vao album
    Album *albumShow = [[Album alloc] init];
    albumShow = [getListAlbumDairy objectAtIndex:indexPath.row];
    
    UIImage *imageShowTemp = [self loadImage:albumShow.nameFile];
    cellNhatKyObj.imageshow.image = imageShowTemp;
    
    return cellNhatKyObj;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //Xem chi tiet view controller
    XemChiTietViewController *showDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"xemChiTietViewController"];
//    UINavigationController *naviShowDetail = [[UINavigationController alloc] initWithRootViewController:showDetailViewController];
    showDetailViewController.dairyID = self.indexDairy;
    showDetailViewController.indexChoice = indexPath.row;
    showDetailViewController.nameDairy = nameDiaryChoice;
    showDetailViewController.litThuNak = getListAlbumDairy;
    [self.navigationController pushViewController:showDetailViewController animated:YES];
    [databse closeData];
}


#pragma mark - work with file system.

// save luu file
- (void)saveImage:(UIImage *)imageTemp nameFile:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(imageTemp); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    NSLog(@"image saved");
    
}

//xoa 1 file image co ten laf fileName

- (void)removeImage:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"image removed");
    
}



// load 1 file tu he thong len, voi doi so la ten nameFile, tra ve i UIImage.

- (UIImage*)loadImage:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}

- (IBAction)shareDairy:(id)sender {
    NSLog(@"Lua chon chia se ca album");
}

- (IBAction)editEmotion:(id)sender {
 NSLog(@"Lua chon thay doi cam xuc su kien ");
}

- (IBAction)deleteDairy:(id)sender {
    NSLog(@"lua chon xoa album");
    [databse delDairyWithID:self.indexDairy];
    [databse closeData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
