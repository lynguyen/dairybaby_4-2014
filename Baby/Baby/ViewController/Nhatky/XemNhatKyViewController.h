//
//  XemNhatKyViewController.h
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLiteDB.h"
#import "Dairy.h"
#import "Album.h"
#import "XemNhatKyCollectionViewCell.h"

@interface XemNhatKyViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

{
    SQLiteDB *databse;
    Dairy *dairyObj;
    NSMutableArray *getListAlbumDairy;
    NSString *nameDiaryChoice;
}

// khai bao 1 so view su dung cho thay doi lay out sau nay
@property (assign, nonatomic)NSInteger indexDairy;
@property (strong, nonatomic)NSString *nameFileAvata;

@property (strong, nonatomic) IBOutlet UIView *viewMain;
@property (strong, nonatomic) IBOutlet UIView *viewAvata;

// khai bao mot so UI de hien thi thong tin ve nhat ky
@property (strong, nonatomic) IBOutlet UIImageView *avata;
@property (strong, nonatomic) IBOutlet UILabel *titleDairy;
@property (strong, nonatomic) IBOutlet UILabel *addressdairy;
@property (strong, nonatomic) IBOutlet UILabel *timeDairy;
@property (strong, nonatomic) IBOutlet UIView *viewEmotion;
@property (strong, nonatomic) IBOutlet UITextView *emotion;

// khai bao UIViewColletion dung de load image, video trong nhat ky
@property (strong, nonatomic) IBOutlet UIView *viewCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionView *myColletionView;
@property (strong, nonatomic) IBOutlet UIToolbar *myToolbar;

// khai bao 3 button bar trong toolbar de bat cac su kien: xoa, chia se, thay doi cam xuc nhat ky
- (IBAction)shareDairy:(id)sender;
- (IBAction)editEmotion:(id)sender;
- (IBAction)deleteDairy:(id)sender;


// 1 uibar button back
@property(strong, nonatomic)UIBarButtonItem *backMainDairy;
@end
