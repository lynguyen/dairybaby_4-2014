//
//  NhatKyViewController.m
//  Baby
//
//  Created by kim ly on 3/25/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "NhatKyViewController.h"
#import "SQLiteDB.h"
#import "XemNhatKyViewController.h"
#import "StandardGrow.h"
#import "BabyGrow.h"

@interface NhatKyViewController ()

@end

@implementation NhatKyViewController




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"SCREEN: NHAT KY:");
    
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundWeb.png"]];
	// khai bao bien indexDairy: lay indexPath cell da click button dell dairy
    indexDairy = 0;
    
    // an cac dong ke giua cac table view cell
    [self.myTableViewNhatKy setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    // set su kien an ban phim ngoai vung trong
//    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                        action:@selector(toucheOnView:)];
//    [self.view addGestureRecognizer:gestureRecognizer];
    statusKeyboard = NO;
    
    // mo database de lam viec
    database = [[SQLiteDB alloc] init];
    
    NSMutableArray *thune = [[NSMutableArray alloc] init];
    thune = [database getAllInfoStandartBoyWithMonth:12];
    for (int jj = 0; jj < [thune count]; jj++) {
        StandardGrow *obb = [[StandardGrow alloc] init];
        obb = [thune objectAtIndex:jj];
        NSLog(@"lay thu %.1f - %.1f ", [obb.levelStandard1 floatValue], [obb.height floatValue]);
    }
    
    getListInfo = [[NSMutableArray alloc] init];
    
    // khoi tao table INFO neu day la lan dau run app
    [database initInfo];
    getListInfo = [database getInfo];
    if ([getListInfo count] == 0 ) {
        [database insertInfo:1 indexBaby:1];

    }
    // hien thi thong tin trong table INFO
    getListInfo = [database getInfo];
    Info *obj = [[Info alloc] init];
    obj = [getListInfo objectAtIndex:0];
    NSLog(@"gia tri hien tai cua INFO indexDiary: %d, indexBaby: %d", obj.indexDairy, obj.indexBaby);
   
    
    // khoi tao table DAIRY neu day la lan dau run app
    //NSLog(@"Thuc hien tao moi table DAIRY neu chua co");
    [database initDairy];
    getListDairy = [[NSMutableArray alloc] init];
    
   // hien thi thong tin trong table DAIRY
    getListDairy = [database getAllDairy];
    NSLog(@"hien thi tat ca cac dairy hien dang co:");
    for (int k = 0; k < [getListDairy count]; k++) {
        Dairy *objDairy = [[Dairy alloc] init];
        objDairy = [getListDairy objectAtIndex:k];
        NSLog(@"id diary: %d - name: %@ - title: %@ - address: %@ - avata: %d - emotion: %@ - index %d", objDairy.iD,  objDairy.name, objDairy.title, objDairy.address, objDairy.avata, objDairy.emotion, objDairy.index);
    }
    
    
    // khoi tao table BABY neu day la lan dau run app
    [database initBaby];
    
   NSLog(@"hien thi tat ca cac baby hien dang co:");
    getListBaby = [[NSMutableArray alloc] init];
    getListBaby = [database getAllBaby];
    
    // khoi tao 1 ban ghi mac dinh
    if ([getListBaby count] == 0) {
        
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSDate *date = [dateFormatter dateFromString:@"2014-04-07"];
        NSInteger createBirth = [date timeIntervalSince1970];
        [self saveImage:[UIImage imageNamed:@"icon_newBaby.png"] nameFile:@"mama"];
        
        [database insertBaby:@"Mặc định" avata:@"mama" birth:createBirth gender:1 like:@" "];
    }
    
    NSLog(@"load tat ca cac baby ra man hinh:");
    getListBaby = [database getAllBaby];
    for (int bb = 0; bb < [getListBaby count]; bb++) {
        Baby *getObjBaby = [[Baby alloc] init];
        getObjBaby = [getListBaby objectAtIndex:bb];
        NSLog(@"id: %d - name : %@ - avata: %@ - birth: %@ - gender: %d - like: %@", getObjBaby.iD, getObjBaby.name, getObjBaby.avata, getObjBaby.birthday, getObjBaby.gender, getObjBaby.like);
    }
   
    // kho tao table BABYGROW
    [database initBabyGrow];
    
    // khoi tao table Album neu chua co
    [database initAlbum];
    
    // khoi tao table ComingEvent chua co
    NSLog(@"=====> CHEN THU FLOAT");
    [database initComingEvent];
    
} //end didloadview

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (self.isFilter == YES) {
        return [getListDairyCopy count];
    }
    else  {
        return [getListDairy count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *identifierNhatKyCell = @"nhatKyTaleViewCell";
    
    NhatKyTableViewCell *cellNhatKy = nil;
    cellNhatKy = [tableView dequeueReusableCellWithIdentifier:identifierNhatKyCell forIndexPath:indexPath];
    
    /// khong tuong tac voi textview
    cellNhatKy.emotion.userInteractionEnabled = NO;
    
    // bo gocs cho cac imageview
    cellNhatKy.avata.layer.backgroundColor = [[UIColor clearColor] CGColor];
    cellNhatKy.avata.layer.masksToBounds = YES;
    cellNhatKy.avata.layer.borderWidth = 0.5f;
    cellNhatKy.avata.layer.borderColor = [[UIColor whiteColor] CGColor];
    cellNhatKy.avata.layer.cornerRadius = 3;
    
    cellNhatKy.imageVideoFirst.layer.backgroundColor = [[UIColor clearColor] CGColor];
    cellNhatKy.imageVideoFirst.layer.masksToBounds = YES;
    cellNhatKy.imageVideoFirst.layer.cornerRadius = 4;
   
    cellNhatKy.imageVideoSecond.layer.backgroundColor = [[UIColor clearColor] CGColor];
    cellNhatKy.imageVideoSecond.layer.masksToBounds = YES;
    cellNhatKy.imageVideoSecond.layer.cornerRadius = 4;
    
    cellNhatKy.imageVideoThird.layer.backgroundColor = [[UIColor clearColor] CGColor];
    cellNhatKy.imageVideoThird.layer.masksToBounds = YES;
    cellNhatKy.imageVideoThird.layer.cornerRadius = 4;
    
    cellNhatKy.imageVideoFourth.layer.backgroundColor = [[UIColor clearColor] CGColor];
    cellNhatKy.imageVideoFourth.layer.masksToBounds = YES;
    cellNhatKy.imageVideoFourth.layer.cornerRadius = 4;
    
    // background images
    
    // set draw line
    UIView *viewLine = [[UIView alloc] initWithFrame:CGRectMake(10, 277, 750, 1)];
    viewLine.backgroundColor = [UIColor brownColor];
    [cellNhatKy.contentView addSubview:viewLine];
    
    
    UIButton *buttonCell = [[UIButton alloc] initWithFrame:CGRectMake(685, 77, 60, 30)];
    [cellNhatKy.contentView addSubview:buttonCell];
    [buttonCell setImage:[UIImage imageNamed:@"icon_delDairy.png"] forState:UIControlStateNormal];
    [buttonCell setTitle:@"Xoá" forState:UIControlStateNormal];
    [buttonCell setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [buttonCell setTitleColor:[UIColor brownColor] forState:UIControlStateSelected];
    buttonCell.backgroundColor = [UIColor colorWithRed:237.0/255 green:237.0/255 blue:237.0/255 alpha:1.0f];
    //buttonCell.layer.backgroundColor = [[UIColor clearColor] CGColor];
    buttonCell.layer.masksToBounds = YES;
    buttonCell.layer.cornerRadius = 4;
    
    // bat dau load data cho cell
    
    // kiem tra trang thai do du lieu:
    //  if dung -> dang su dung search bar. nguoc lai -> khong su dung search bar
    
    if (self.isFilter == YES) {
        // lay 1 doi tuong Dairy tuong ung voi index
        Dairy *objDairyIndex = [getListDairyCopy objectAtIndex:indexPath.row];
        
        getListAlbum = [[NSMutableArray alloc] init];
        getListAlbum = [database getAllAlbumWithNameDairy:objDairyIndex.name];
        
        // hien thi tat ca cac image, video co trong dairy nay:
        NSLog(@"do tat ca cac thong tin trong album ra:");
        for (int infoAlbum = 0; infoAlbum < [getListAlbum count]; infoAlbum ++) {
            Album *tempAlbum = [[Album alloc] init];
            tempAlbum = [getListAlbum objectAtIndex:infoAlbum];
            NSLog(@"id: %d - namefile: %@ - name diary: %@ - type: %d",tempAlbum.iD, tempAlbum.nameFile, tempAlbum.nameDairy, tempAlbum.type);
        }

        NSLog(@"&&&& ==> xem lai 1 lan nua: ");
        NSDate *eventDate1 = objDairyIndex.time;
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        dateFormatter1.dateFormat = @"dd-MM-yyyy";
        NSString  *theDate1 = [dateFormatter1 stringFromDate:eventDate1];
        NSLog(@"theDate %@", theDate1);
        
        

        cellNhatKy.timeDairyObj.text = theDate1;
        cellNhatKy.title.text = objDairyIndex.title;
        cellNhatKy.emotion.text = objDairyIndex.emotion;
        cellNhatKy.address.text = objDairyIndex.address;
        cellNhatKy.numberImageVideo.text = [NSString stringWithFormat:@"%d ảnh",[getListAlbum count]];
        // set avata baby
        Baby *babyTempAvata  = [[Baby alloc] init];
        for (int i = 0;  i < [getListBaby count]; i++) {
            babyTempAvata = [getListBaby objectAtIndex:i];
            if (babyTempAvata.iD == objDairyIndex.avata) {
                cellNhatKy.avata.image = [self loadImage:babyTempAvata.avata];
            }
        }
        
        // do du lieu image cho avata
        
        // do du lieu cho image, video dai dien.
        if ([getListAlbum count]>= 1) {
            Album *album1 = [[Album alloc] init];
            album1 = [getListAlbum objectAtIndex:0];
            
            NSString *nameimage = album1.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoFirst.image = imageLoad;
        }
        if ([getListAlbum count]>= 2) {
            Album *album2 = [[Album alloc] init];
            album2 = [getListAlbum objectAtIndex:1];
            NSString *nameimage = album2.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoSecond.image = imageLoad;
        }
        if ([getListAlbum count]>= 3) {
            Album *album3 = [[Album alloc] init];
            album3 = [getListAlbum objectAtIndex:2];
            
            NSString *nameimage = album3.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoThird.image = imageLoad;
        }
        if ([getListAlbum count]>= 4) {
            Album *album4 = [[Album alloc] init];
            album4 = [getListAlbum objectAtIndex:3];
            
            NSString *nameimage = album4.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoFourth.image = imageLoad;
        }
        //////////
        // thu luu va load data
        //UIImage *chan = [[UIImage alloc] init];
       // [self saveImage:[UIImage imageNamed:@"be7.PNG"] nameFile:@"thunak"];
       // cellNhatKy.avata.image = [self loadImage:@"thunak"];
        
        
        
        int indexObject = indexPath.row;
        buttonCell.tag = indexObject;
        
        [buttonCell addTarget:self action:@selector(delDairyQuick:) forControlEvents:UIControlEventTouchUpInside];
        

    }
    
    // trang thai khi khog su dung seach bar
    else  {
        // thuc hien gan  gia tri ch tung thuoc tinh trong custo cell
        Dairy *objDairyIndex = [getListDairy objectAtIndex:indexPath.row];
        
        getListAlbum = [[NSMutableArray alloc] init];
        getListAlbum = [database getAllAlbumWithNameDairy:objDairyIndex.name];
        
        NSLog(@"do tat ca cac thong tin trong album ra:");
        for (int infoAlbum = 0; infoAlbum < [getListAlbum count]; infoAlbum ++) {
            Album *tempAlbum = [[Album alloc] init];
            tempAlbum = [getListAlbum objectAtIndex:infoAlbum];
            NSLog(@"id: %d - namefile: %@ - name diary: %@ - type: %d",tempAlbum.iD, tempAlbum.nameFile, tempAlbum.nameDairy, tempAlbum.type);
        }
        
        NSLog(@"&&&& ==> xem lai 1 lan nua: ");
        NSDate *eventDate1 = objDairyIndex.time;
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        //dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        dateFormatter1.dateFormat = @"dd-MM-yyyy";
        NSString  *theDate1 = [dateFormatter1 stringFromDate:eventDate1];
        NSLog(@"theDate %@", theDate1);
        
        
        cellNhatKy.timeDairyObj.text = theDate1;
        
        cellNhatKy.title.text = objDairyIndex.title;
        cellNhatKy.emotion.text = objDairyIndex.emotion;
        cellNhatKy.address.text = objDairyIndex.address;
        cellNhatKy.numberImageVideo.text = [NSString stringWithFormat:@"%d ảnh",[getListAlbum count]];//
        
        // set avata baby
        Baby *babyTempAvata  = [[Baby alloc] init];
        for (int i = 0;  i < [getListBaby count]; i++) {
            babyTempAvata = [getListBaby objectAtIndex:i];
            if (babyTempAvata.iD == objDairyIndex.avata) {
                cellNhatKy.avata.image = [self loadImage:babyTempAvata.avata];
            }
        }
        
        // do du lieu cho image, video dai dien.
        if ([getListAlbum count]>= 1) {
            Album *album1 = [[Album alloc] init];
            album1 = [getListAlbum objectAtIndex:0];
            
            NSString *nameimage = album1.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoFirst.image = imageLoad;
        }
        if ([getListAlbum count]>= 2) {
            Album *album2 = [[Album alloc] init];
            album2 = [getListAlbum objectAtIndex:1];
            NSString *nameimage = album2.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoSecond.image = imageLoad;
        }
        if ([getListAlbum count]>= 3) {
            Album *album3 = [[Album alloc] init];
            album3 = [getListAlbum objectAtIndex:2];
            
            NSString *nameimage = album3.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoThird.image = imageLoad;
        }
        if ([getListAlbum count]>= 4) {
            Album *album4 = [[Album alloc] init];
            album4 = [getListAlbum objectAtIndex:3];
            
            NSString *nameimage = album4.nameFile;
            UIImage *imageLoad = [self loadImage:nameimage];
            cellNhatKy.imageVideoFourth.image = imageLoad;
        }
        //////////
        // thu luu va load data
        //UIImage *chan = [[UIImage alloc] init];
        [self saveImage:[UIImage imageNamed:@"be7.PNG"] nameFile:@"thunak"];
//        if (indexPath.row == 0) {
//            cellNhatKy.avata.image = [self loadImage:@"thunak"];
//        }
        
        int indexObject = indexPath.row;
        buttonCell.tag = indexObject;
        
        [buttonCell addTarget:self action:@selector(delDairyQuick:) forControlEvents:UIControlEventTouchUpInside];
        

    }

    
    // stop run indicator
    [cellNhatKy.indicatorFirst stopAnimating];
    [cellNhatKy.indicatorSecond stopAnimating];
    [cellNhatKy.indicatorThird stopAnimating];
    [cellNhatKy.indicatorFourth stopAnimating];
    //
    cellNhatKy.accessoryType = UITableViewCellAccessoryNone;
    cellNhatKy.selectionStyle = UITableViewCellAccessoryNone;
    return cellNhatKy;
}

// xu ly su kien khi click vao 1 custom UITableview cell

//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{

//    NSLog(@"Toi vua click vao cell co thu tu la: %d",indexPath.row);
//    XemNhatKyViewController *wathDairy = [self.storyboard instantiateViewControllerWithIdentifier:@"xemNhatKyViewController"];
//    UINavigationController *naviWathDairy = [[UINavigationController alloc] initWithRootViewController:wathDairy];
//    [self presentViewController:naviWathDairy animated:YES completion:nil];

    
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        [database closeData];
}

- (CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 1){
        return 100;
    }
    else
        return 280;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
   if([segue.identifier isEqualToString:@"backNhatKyViewController"]){
       //NhatKyViewController *cellNext = (NhatKyViewController *)sender;
       
       //NhatKyTableViewCell *celNext = (NhatKyTableViewCell *)sender;
       NSIndexPath *indexPath = [self.myTableViewNhatKy indexPathForCell:sender];
       //*indexPath
       NSLog(@"thu tu cua cell duoc chuyen qua laf: %d", indexPath.row);
       
       XemNhatKyViewController *nextViewController = (XemNhatKyViewController *)[segue destinationViewController];
       // chuyen gia tri cuar dairy da chon cho Xem nhat ky
       if (self.isFilter == YES) {
           Dairy *dairyNext = [[Dairy alloc] init];
           dairyNext = [getListDairyCopy objectAtIndex:indexPath.row];
           nextViewController.indexDairy = dairyNext.iD;
           
           Baby *babyObj = [[Baby alloc] init];
           for (int i = 0;  i < [getListBaby count]; i++) {
               babyObj = [getListBaby objectAtIndex:i];
               if (dairyNext.avata == babyObj.iD) {
                   nextViewController.nameFileAvata = babyObj.avata;
               }
           }
           
       }
       else  {
           Dairy *dairyNext = [[Dairy alloc] init];
           dairyNext = [getListDairy objectAtIndex:indexPath.row];
           nextViewController.indexDairy = dairyNext.iD;
           
           Baby *babyObj = [[Baby alloc] init];
           for (int i = 0;  i < [getListBaby count]; i++) {
               babyObj = [getListBaby objectAtIndex:i];
               if (dairyNext.avata == babyObj.iD) {
                   nextViewController.nameFileAvata = babyObj.avata;
               }
           }
       }

   }
}

#pragma mark- method load lai data

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    getListDairy = nil;
    [database openData];
    getListDairy = [database getAllDairy];
    self.isFilter = NO;
    //[self viewDidLoad];
    [self.myTableViewNhatKy reloadData];
}


- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self.mySearchBar resignFirstResponder];
}
#pragma mark- method xoa 1 dairy:

- (void)delDairyQuick:(UIButton *)sender;
{
    [self.mySearchBar resignFirstResponder];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn thực sự muốn xoá sự kiện này?" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Bỏ qua", nil];
    [alert show];
    NSLog(@"gia tri hien tai cua tag button: %d",sender.tag );
    indexDairy = sender.tag;
    NSLog(@"gia tri hien tai cua index diary: %d",indexDairy );
}

#pragma mark- method UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"gia tri hien tai cua index trong alert delegate: %d",indexDairy);
    if (buttonIndex == 0) {
        NSLog(@"Lua chon xoa su kien");
        
        if (self.isFilter == YES) {// dung search bar-> xoa 1 dairy trong seachbar
            // xoa thanh cong
            Dairy *delDairy = [[Dairy alloc] init];
            delDairy = [getListDairyCopy objectAtIndex:indexDairy];// xoa dairy co id
            [database delDairyWithID:delDairy.iD];           // xoa tat ca anh video co name dairy
            [database delAlbumWithNameDiary:delDairy.name];  // xoa tat ca anh video co name dairy
            
            // load lai data cho table view:
            getListDairy = nil;
            getListDairy = [database getAllDairy];
            self.mySearchBar.text = @"";
            self.isFilter = NO;
            [self.myTableViewNhatKy reloadData];
        }
        
        else  { // khong dung search bar
            // xoa thanh cong
            Dairy *delDairy = [[Dairy alloc] init];
            delDairy = [getListDairy objectAtIndex:indexDairy];
            [database delDairyWithID:delDairy.iD];
            [database delAlbumWithNameDiary:delDairy.name];
            
            // load lai data cho table view:
            getListDairy = nil;
            getListDairy = [database getAllDairy];
            
            [self.myTableViewNhatKy reloadData];
        }
        
    }
    if (buttonIndex == 1) {
        NSLog(@"Lua chon bo qua su kien");
    }
}


#pragma mark - implement UISearchBar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([searchText length] == 0) {
        self.isFilter = NO;
        
    }
    else{
        
        // thay doi trang thai flag
        self.isFilter = YES;
        
        getListDairyCopy = [[NSMutableArray alloc] init];
        
        
        for (Dairy *dairyFind in getListDairy) {
            
            NSRange titleDairyRange = [dairyFind.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (titleDairyRange.location != NSNotFound) {
                
                [getListDairyCopy addObject:dairyFind];
            }
            
            // neu tim kiem theo nhieu du kien thi chen them NSrange
            //NSRange addressDairyRange = [dairyFind.address rangeOfString:searchText options:NSCaseInsensitiveSearch];
            //if (titleDairyRange.location != NSNotFound || addressDairyRange.location != NSNotFound){..}
        }
        
    }
    
    [self.myTableViewNhatKy reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.mySearchBar resignFirstResponder];
}

// an keyboard khi touch ngoai uisearch ba
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [[event allTouches] anyObject];
    // kiem tra neu text dang su dung va vi tri touch # text thi an....    
      if ([self.mySearchBar isFirstResponder] &&
        [touch view] != self.mySearchBar && [touch view] != self.myTableViewNhatKy ) {
        [self.mySearchBar resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
    
}



#pragma mark -touch hide keyboard

- (void)toucheOnView:(UIGestureRecognizer *)gesture;
{
    
    [self.mySearchBar resignFirstResponder];
}



#pragma mark - work with file system.
- (void)saveImage:(UIImage *)imageTemp nameFile:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(imageTemp); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    NSLog(@"image saved");
    
}

//removing an image

- (void)removeImage:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"image removed");
    
}



//loading an image



- (UIImage*)loadImage:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}


@end
