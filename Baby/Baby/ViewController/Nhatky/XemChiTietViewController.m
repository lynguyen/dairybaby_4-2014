//
//  XemChiTietViewController.m
//  Baby
//
//  Created by kim ly on 4/15/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "XemChiTietViewController.h"
#import "Info.h"

@interface XemChiTietViewController (){
    
}
@property (nonatomic) float originalScrollerOffsetY;

-(void)keyboardWasShown:(NSNotification *)notification;
-(void)keyboardWillHide:(NSNotification *)notification;

@end

@implementation XemChiTietViewController
-(void)keyboardWasShown:(NSNotification *)notification{
    NSLog(@"Keyboard is shown.");
    
    if (1) {
        
    }
    
    float viewWidth = self.view.frame.size.width;
    float viewHeight = self.view.frame.size.height;
    
    // Get the size of the keyboard from the userInfo dictionary.
    NSDictionary *keyboardInfo = [notification userInfo];
    CGSize keyboardSize = [[keyboardInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    float keyboardHeight = keyboardSize.height;
    
    if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
        keyboardHeight = keyboardSize.width;
        
        // Also swap the view width and height.
        float temp = viewWidth;
        viewWidth = viewHeight;
        viewHeight = temp;
    }
    
    
    CGRect viewableAreaFrame = CGRectMake(0.0, 0.0, viewWidth, viewHeight - keyboardHeight);
    
    
    CGRect txtDemoFrame = self.viewMain.frame;
    
    if (!CGRectContainsRect(viewableAreaFrame, txtDemoFrame)) {
        // We need to calculate the new Y offset point.
        float scrollPointY = viewHeight - keyboardHeight;
        _originalScrollerOffsetY = self.scrollMain.contentOffset.y;
        
        // Finally, scroll.
        [self.scrollMain setContentOffset:CGPointMake(0.0, scrollPointY) animated:YES];
    }
}


-(void)keyboardWillHide:(NSNotification *)notification{
    NSLog(@"Keyboard is closing.");
    
    // the scroll view back into its original position.
    [self.scrollMain setContentOffset:CGPointMake(0.0, _originalScrollerOffsetY) animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    self.emotion = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    self.emotion = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSLog(@"===> thong tin duoc dua sang ben xem chi tiet, idname : %d, index duoc chon: %d", self.dairyID, self.indexChoice);
    database = [[SQLiteDB alloc] init];
    [database openData];
    self.title = @"Khoảnh khắc";
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"nen.png"]];

    indexAuto = self.indexChoice;
    dairyInfo = [[Dairy alloc] init];
    getListAlbum = [[NSMutableArray alloc] init];
    getListDairy = [[NSMutableArray alloc] init];
    
    // lay ten dairy co id
    getListDairy = [database getDairyWithID:self.dairyID];
    dairyInfo = [getListDairy objectAtIndex:0];
    //NSString *nameDairy = dairyInfo.name;
    NSLog(@"ten dairy can lay anh ra la %@,", self.nameDairy);
    // lay tat ca anh, video thong qua id cuar dairy
    getListAlbum = [database getAllAlbumWithNameDairy:self.nameDairy];
    NSLog(@"so album anh co trong dairy nay la; %d,", [getListAlbum count]);
    
    
    // THU DA
    NSMutableArray * lietThu = [[NSMutableArray alloc] init];
    lietThu = [database getInfo];
    NSLog(@"th co so hang = %d", [self.litThuNak count]);
    UIImage *imageTemp = [[UIImage alloc] init];
    
    Album *albumThu = [self.litThuNak objectAtIndex:indexAuto];
    imageTemp = [self loadImage:albumThu.nameFile];
    self.imageShow.image = imageTemp;
    self.emotion.text = albumThu.emotion;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// touch ngang quay lai
- (IBAction)touchLeft:(id)sender {
    if (indexAuto >0) {
        indexAuto -= 1;
        Album *albumThu1 = [self.litThuNak objectAtIndex:indexAuto];
        
        UIImage *imageTemp = [[UIImage alloc] init];
        
        imageTemp = [self loadImage:albumThu1.nameFile];
        self.imageShow.image = imageTemp;
        self.emotion.text = albumThu1.emotion;

    }
}

// touch ngang de xoa
- (IBAction)touchRight:(id)sender {
    if (indexAuto < ([self.litThuNak count] - 1)) {
        indexAuto += 1;
        Album *albumThu1 = [self.litThuNak objectAtIndex:indexAuto];
        
        UIImage *imageTemp = [[UIImage alloc] init];
        
        imageTemp = [self loadImage:albumThu1.nameFile];
        self.imageShow.image = imageTemp;
        self.emotion.text = albumThu1.emotion;
    }
    
}
- (IBAction)delImageVideo:(id)sender {
}

#pragma mark


#pragma mark - work with file system.
- (void)saveImage:(UIImage *)imageTemp nameFile:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(imageTemp); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    NSLog(@"image saved");
    
}

//removing an image

- (void)removeImage:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"image removed");
    
}



//loading an image



- (UIImage*)loadImage:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}

@end
