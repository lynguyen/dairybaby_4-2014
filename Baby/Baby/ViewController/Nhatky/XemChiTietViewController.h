//
//  XemChiTietViewController.h
//  Baby
//
//  Created by kim ly on 4/15/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Baby.h"
#import "Dairy.h"
#import "Album.h"
#import "SQLiteDB.h"

@interface XemChiTietViewController : UIViewController<UIScrollViewDelegate, UITextViewDelegate>
{
    SQLiteDB *database;
    
    // get info diary and albums
    NSMutableArray *getListDairy;
    NSMutableArray *getListAlbum;
    int indexAuto;
    Dairy *dairyInfo;
    
}
@property(strong, nonatomic)NSString *nameDairy;
@property(strong, nonatomic)NSString *nameFileAvata;
@property(assign, nonatomic)NSInteger dairyID;
@property(assign, nonatomic)NSInteger indexChoice;
@property(strong, nonatomic)NSMutableArray *litThuNak;

- (IBAction)touchLeft:(id)sender;
- (IBAction)touchRight:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *emotion;
@property (strong, nonatomic) IBOutlet UIImageView *imageShow;

- (IBAction)delImageVideo:(id)sender;

//view using for scroll view
@property (strong, nonatomic) IBOutlet UIView *viewMain;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollMain;

@end
