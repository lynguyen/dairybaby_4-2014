//
//  ChenCamXucViewController.h
//  Baby
//
//  Created by kim ly on 4/9/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLiteDB.h"
#import "Dairy.h"
#import "Album.h"
#import "Info.h"
#import <Social/Social.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface ChenCamXucViewController : UIViewController<UITextViewDelegate, UIScrollViewDelegate>
{
    SQLiteDB *database;
    Album *albumTemp;
    NSMutableArray *albumArray;
    int indexAuto;
    UIImage *imageTemp;
}

@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UIView *viewGroundImage;
@property (strong, nonatomic) IBOutlet UIView *viewGroundEmotion;
@property (strong, nonatomic) IBOutlet UITextView *emotion;

- (IBAction)shareSocial:(id)sender;

- (IBAction)share:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollMain;
@property (strong, nonatomic) IBOutlet UIView *viewMain;


- (IBAction)touchLeft:(id)sender;
- (IBAction)touchRight:(id)sender;

@property(assign, nonatomic)NSInteger indexAlbum;
@property(strong, nonatomic)NSString *nameDairy;
@property(strong, nonatomic)UIBarButtonItem *saveChange;
@property(strong, nonatomic)UIBarButtonItem *cancelChange;
@end
