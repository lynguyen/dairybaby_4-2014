//
//  ChenCamXucViewController.m
//  Baby
//
//  Created by kim ly on 4/9/14.
//  Copyright (c) 2014 kim ly. All rights reserved.
//

#import "ChenCamXucViewController.h"

@interface ChenCamXucViewController ()
// scroll view.
@property (nonatomic) float originalScrollerOffsetY;

-(void)keyboardWasShown:(NSNotification *)notification;
-(void)keyboardWillHide:(NSNotification *)notification;

@end

@implementation ChenCamXucViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"=> SCREEN: CHEN CAM XUC:");
    NSLog(@"id cua Album duoc dua qua chen cam xuc la; %d",self.indexAlbum);
    indexAuto = self.indexAlbum;
    database  = [[SQLiteDB alloc] init];
    [database openData];
    albumArray = [[NSMutableArray alloc] init];
    albumArray = [database getAllAlbumWithNameDairy:self.nameDairy];
    albumTemp = [[Album alloc] init];
    albumTemp = [albumArray objectAtIndex:indexAuto];
    
    imageTemp = [[UIImage alloc]init];
    imageTemp = [self loadImage:albumTemp.nameFile];
    self.image.image = imageTemp;
       
	// Do any additional setup after loading the view.
    // set collor for UINavicontroller
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:166.0/255 green:238.0/255 blue:253.0/255 alpha:1.0f];
    
    
    // khoi tao 2 UIBarButtonItem tren navi controller
    self.saveChange = [[UIBarButtonItem alloc] initWithTitle:@"Lưu" style:UIBarButtonItemStyleBordered target:self action:@selector(saveChangeEmotion)];
    
    self.navigationItem.leftBarButtonItem = self.saveChange;
    self.cancelChange = [[UIBarButtonItem alloc] initWithTitle:@"Huỷ" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelChangeEmotion)];
    
    self.navigationItem.rightBarButtonItem = self.cancelChange;
    
    //
    self.viewGroundEmotion.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    
    self.viewGroundEmotion.layer.cornerRadius = 8;
    self.viewGroundEmotion.layer.masksToBounds = YES;
    self.viewGroundEmotion.layer.borderColor = [[UIColor blueColor] CGColor];
    self.viewGroundEmotion.layer.borderWidth = 1;
    
    self.viewGroundImage.layer.cornerRadius = 8;
    self.viewGroundImage.layer.masksToBounds = YES;
    self.viewGroundImage.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.viewGroundImage.layer.borderWidth = 1;
    
    self.title = @"Chèn cảm xúc";
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(toucheOnView:)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    // Add two notifications for the keyboard. One when the keyboard is shown and one when it's about to hide.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
     //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundWeb.png"]];
    self.emotion.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - event save, cancel

- (void)saveChangeEmotion;
{
    for (int i = 0; i < ([albumArray count] - 1) ; i++) {
        Album *saveEmotionAlbum = [[Album alloc] init];
        saveEmotionAlbum = [albumArray objectAtIndex:i];
        
        [database updateEmotionWithNameFileAlbum:saveEmotionAlbum.nameFile emotion:saveEmotionAlbum.emotion];
    }
    [database closeData];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancelChangeEmotion;
{
    [database closeData];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - method hide keyboard when click on view
- (void)toucheOnView:(UIGestureRecognizer *)gesture;
{
    
    [self.emotion resignFirstResponder];
}

#pragma mark - Private Methods

-(void)keyboardWasShown:(NSNotification *)notification;
{
    NSLog(@"Keyboard is shown.");
    
    if (1) {
        
    }
    
    float viewWidth = self.view.frame.size.width;
//    float viewHeight = self.view.frame.size.height;
    float viewHeight = 420;
    
    // Get the size of the keyboard from the userInfo dictionary.
    NSDictionary *keyboardInfo = [notification userInfo];
    CGSize keyboardSize = [[keyboardInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    float keyboardHeight = keyboardSize.height;
    
//    if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
//        keyboardHeight = keyboardSize.width;
//        
//        // Also swap the view width and height.
//        float temp = viewWidth;
//        viewWidth = viewHeight;
//        viewHeight = temp;
//    }
    
    
    CGRect viewableAreaFrame = CGRectMake(0.0, 0.0, viewWidth, viewHeight - keyboardHeight);
    
    
    CGRect txtDemoFrame = self.viewMain.frame;
    
    if (!CGRectContainsRect(viewableAreaFrame, txtDemoFrame)) {
        // We need to calculate the new Y offset point.
        float scrollPointY = viewHeight - keyboardHeight;
        _originalScrollerOffsetY = self.scrollMain.contentOffset.y;
        
        // Finally, scroll.
        [self.scrollMain setContentOffset:CGPointMake(0.0, scrollPointY) animated:YES];
    }
}


-(void)keyboardWillHide:(NSNotification *)notification{
    NSLog(@"Keyboard is closing.");
    
    // Move the scroll view back into its original position.
    [self.scrollMain setContentOffset:CGPointMake(0.0, _originalScrollerOffsetY) animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.emotion = textView;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    self.emotion = nil;
    
}





- (IBAction)touchLeft:(id)sender;
{
    NSLog(@"  ==> toi vua touch trai ");
    if (indexAuto < ([albumArray count] - 1) ) {
        Album *albumChange = [[Album alloc] init];
        albumChange.iD = albumTemp.iD;
        albumChange.nameFile = albumTemp.nameFile;
        albumChange.nameDairy = albumTemp.nameDairy;
        albumChange.type = albumTemp.type;
        albumChange.emotion = self.emotion.text;  // cap nhat cam xuc trong textview
        
        
        // thay doi gia tri emotion tuong ung voi anh
        
        [albumArray removeObjectAtIndex:indexAuto];
        [albumArray insertObject:albumChange atIndex:indexAuto];
        
        indexAuto += 1;
        albumTemp = [albumArray objectAtIndex:indexAuto];        
        imageTemp = [self loadImage:albumTemp.nameFile];
        self.image.image = imageTemp;
        self.emotion.text = albumTemp.emotion;
    }
    
}

                    
- (IBAction)touchRight:(id)sender;
{
    NSLog(@"  ==> toi vua touch phai  ");
    if (indexAuto > 0) {
        Album *albumChange = [[Album alloc] init];
        albumChange.iD = albumTemp.iD;
        albumChange.nameFile = albumTemp.nameFile;
        albumChange.nameDairy = albumTemp.nameDairy;
        albumChange.type = albumTemp.type;
        albumChange.emotion = self.emotion.text;  // cap nhat cam xuc trong textview        
        
        // thay doi gia tri emotion tuong ung voi anh
        
        [albumArray removeObjectAtIndex:indexAuto];
        [albumArray insertObject:albumChange atIndex:indexAuto];
        
        indexAuto -= 1;
        albumTemp = [albumArray objectAtIndex:indexAuto];
        imageTemp = [self loadImage:albumTemp.nameFile];
        self.image.image = imageTemp;
        self.emotion.text = albumTemp.emotion;
    }
}

                        
                        
// save luu file
//- (void)saveImage:(UIImage *)imageTemp nameFile:(NSString*)imageName {
//    
//    NSData *imageData = UIImagePNGRepresentation(imageTemp); //convert image into .png format.
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    
//    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
//    
//    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
//    
//    NSLog(@"image saved");
//    
//}

//xoa 1 file image co ten laf fileName
                        
- (void)removeImage:(NSString*)fileName {
                            
    NSFileManager *fileManager = [NSFileManager defaultManager];
                            
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            
    NSString *documentsDirectory = [paths objectAtIndex:0];
                            
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
                            
    [fileManager removeItemAtPath: fullPath error:NULL];
                            
    NSLog(@"image removed");
}
                        
                        
                        
// load 1 file tu he thong len, voi doi so la ten nameFile, tra ve i UIImage.
                        
- (UIImage*)loadImage:(NSString*)imageName {
                            
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            
    NSString *documentsDirectory = [paths objectAtIndex:0];
                            
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
                            
    return [UIImage imageWithContentsOfFile:fullPath];
                            
}
                        
                    
                        
- (IBAction)share:(id)sender;
{
    NSLog(@"==>cal method share:");
    NSArray *activityItems;
    
//    if (_postImage.image != nil) {
        activityItems = @[self.emotion.text, self.image.image];
//    } else {
//        activityItems = @[_postText.text];
//    }
    
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
}
- (IBAction)shareSocial:(id)sender {
    
     NSLog(@"==>cal method share n==>");
}
@end
